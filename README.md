# ~/ABRÜPT/LUCIEN RAPHMAJ/CONTRE-NUIT/*

La [page de ce livre](https://abrupt.cc/lucien-raphmaj/contre-nuit/) sur le réseau.

## Sur le livre

Contre-nuit n’est pas la simple contre-histoire de la nuit. Car l’histoire de son effacement, de sa disparition de notre temps et de notre espace, le retrait de la nuit étoilée et de toutes ses ramifications en nous ne nous suffit pas. Après le constat de notre désidération vient le moment d’agir en faveur d’une contre-nuit.

Mythologies cannibales des nuits passées, désastre des nuits présentes, amitologie des nuits futures, la contre-nuit est ce point de nuit noire dans nos nuits blanches traversant l’existence, la poésie, la philosophie, la cosmo-politique, et l’intime de tout dehors.

La contre-nuit se veut ainsi contre-feu, contre-ciel ; autre nuit, autre ciel — à la verticale des étoiles, à l’horizon du monde. La contre-nuit veut te faire sentir son battement de cœur de pulsar, sa musique drone, nuit contre-nuit, nuit après nuit.

## Sur l'auteur

On pourrait dire Lucien Blandinovitch Raphmaj, pour lui donner un air de Victor Frankenstein enfanté par sa créature, mais il a déjà une vie de latérateur. Écrivant des textes comme des traversées des arts et des savoirs, il pratique avec SMITH ces transformations contemporaines de l’imaginaire par les mots.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
