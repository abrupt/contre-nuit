      \emph{L'image originale de la couverture est de Didier Descouens. Cette photographie\\de \emph{saturnia pyri} (ô grand spectre de nos nuits) se trouve sous licence Creative Commons\\
  Attribution --- Partage dans les Mêmes Conditions 4.0 International (CC~BY-SA~4.0).\\L'image qui en dérive et l'ensemble de la couverture de cet ouvrage\\sont également placées sous cette même licence CC~BY-SA~4.0.}

```{=tex}
\huge
```
```{=tex}
\parbox{\textwidth}{Contre-histoire de la nuit du point de vue de sa disparition (présent), de cet envers de la nuit qui n'est pas le jour (passé), mais ce paradoxe de la nuit disparue dans la nuit, nuit toujours à réinventer comme accueil du vertige, de l'informe, du dehors, de la vie, de la pensée --- contre l'image, le mot de nuit, contre-cosmologie du désastre, refus et promesse résistantes, contre-chants de la contre-nuit (avenir, neutre).\parfillskip=0pt}
```
```{=tex}
\normalsize
```
# Premier Cercle / Premier Ciel[^1-contre-nuit_1] / Seuils^x^ {#premier-cercle-premier-ciel1-seuilsx}

```{=tex}
\def\addquotenancy
{%
  \begin{tabular}[b]{p{3.5cm}}
    \begin{nscenter}
    « La désidération engendre le désir »\\
    Jean-Luc Nancy
    \end{nscenter}
  \end{tabular}%
}
```
```{=tex}
\begin{pullquote}{object=\addquotenancy}
\itshape
```
« Le désastre est celui du sens : désamarré des astres, les astres eux-mêmes désamorcés de la voûte, de son cloutage ou de sa ponctuation scintillante de vérité(s), le sens s'échappe pour faire sens a-cosmique, le sens se fait constellation sans nom et sans fonction, dépourvue de toute astrologie, dispersant aussi les repères de la navigation, les envoyant aux confins.

Alors --- et l'événement de toute cette époque, l'événement *occidental* par excellence --- prend fin la *considération*, c'est-à-dire l'observation et l'observance de l'ordre sidéral, et d'un ordre à ce point ordonné qu'il fallait même en rétablir la vérité contre les apparences de mouvement aberrant données par certains astres. Cela s'appelait "sauver les phénomènes", et les astres en question furent nommés *planètes* ("errants"). Désormais, le monde entier est *planétaire* : errant de part en part. Mais "errance" est un mot trop étroit, car il suppose une rectitude à quoi mesurer l'écart ou la divagation de l'errant. Le *planétaire*, le désastre planétaire, est encore autre chose qu'une errance, autre chose qui serait à sauver contre son apparence : il épuise l'être dans son phénomène, et son phénomène s'épuise dans l'inapparence des espaces intersidéraux, un occident universel, sans directions, sans points cardinaux. Ni errance, ni erreur, l'univers court sur son erre. C'est tout. C'est comme si tout le sens nous était proposé à travers une monstrueuse physique de l'inertie, où un même mobile se propagerait dans tous les sens à la fois. »

```{=tex}
\end{pullquote}
```
## Nuit ouverte, livre ouvert, amorces

Que ce livre te lise[^1-contre-nuit_2].

```{=tex}
\vspace{1\baselineskip}
```
Que ce livre t'exorcise[^1-contre-nuit_3].

Exorcise tes nuits blanches, tes nuits sales, tes nuits absentes[^1-contre-nuit_4].

De toutes ces nuits trop pâles, rapides, évanouies[^1-contre-nuit_5].

Que ce livre[^1-contre-nuit_6]

Exorcise les pourquoi, les comment[^1-contre-nuit_7]

Te laisse aussi avec cette nuit proliférante[^1-contre-nuit_8]

Faisant un livre ouvert autant que la nuit[^1-contre-nuit_9]

Un livre de magie réelle et d'expérience vécue[^1-contre-nuit_10].

```{=tex}
\begin{centre}
```
*J'ai dit **la nuit** et déjà on glisse ailleurs. Comme si **la nuit** ne désignait jamais la nuit, mais était ce point d'absence, de pur glissement, échappant sans cesse dans le discours, comme si la nuit étant toujours autre qu'elle-même, désignant une négativité aveugle (désastre), non consciente d'elle-même, ayant oublié que la nuit n'est pas que l'absence de lumière, ni qu'une image prise entre les mâchoires de la fête et du moribond[^1-contre-nuit_11].*

```{=tex}
\vspace{1\baselineskip}
```
*Qu'opposer à cette image hors-monde, im-monde[^1-contre-nuit_12] ?*

```{=tex}
\vspace{1\baselineskip}
```
*Une expérience. Un geste, un sentiment, une pensée, une façon de relier l'ensemble de l'expérience[^1-contre-nuit_13].*

```{=tex}
\vspace{1\baselineskip}
```
*C'est ainsi la présence de la nuit que j'appellerai de manière paradoxale contre-nuit. Contre-nuit comme résistance à l'image omniprésente, à l'omni-image à laquelle la nuit est réduite maintenant --- le fond d'écran de nos sociétés hypnotiques.*

```{=tex}
\end{centre}
```
Que ce livre te lise. Que ce livre t'exorcise[^1-contre-nuit_14].

Te fasse sentir l'expérience manquée de la nuit[^1-contre-nuit_15],

la nuit unilatérale, la nuit brisée en mille morceaux[^1-contre-nuit_16]

le monde la nuit[^1-contre-nuit_17]

```{=tex}
\begin{centre}
```
*L'espace s'est détruit et la nuit aussi[^1-contre-nuit_18].*

```{=tex}
\vspace{1\baselineskip}
```
*Tu devras reconnaître avec moi, si tu ne l'as pas déjà fait, la profondeur de cette catastrophe qu'est de vivre sans nuit, sans dehors, sans expérience du nocturne que spectaculaire :*

```{=tex}
\vspace{1\baselineskip}
```

`\uline{Tout ce qui était directement vécu s'est éloigné dans une représentation.}`{=tex}

```{=tex}
\vspace{1\baselineskip}
```

*La prophétie du Spectacle faite par Debord, sans cesse répétée et sans cesse déployée, qui atteint ainsi des dimensions cosmiques sur une "planète malade" : c'est la nuit vécue qui s'est maintenant éloignée dans sa représentation[^1-contre-nuit_19].*

```{=tex}
\end{centre}
```
Que ce livre te lise.

```{=tex}
\vspace{1\baselineskip}
```
Toi, moi, nos nuits, la nuit, toutes les nuits, pluriel infini.

```{=tex}
\vspace{1\baselineskip}
```
Que ce livre te dérobe[^1-contre-nuit_20] à l'analyse de l'histoire de la nuit, à sa bibliographie savante[^1-contre-nuit_21]. Qu'il te fasse vivre plutôt la biographie de la nuit, et une autobiographie du point de vue de la nuit[^1-contre-nuit_22], de toutes les nuits vivantes, mortes, aimantes, férales, magnétiques, toutes les nuits encore, toutes les nuits qui manquent, dans toutes les dimensions qui ne se réduisent pas à une pure absence.

Contre-nuit dans la nuit.

Présence de la nuit.

Et de la nuit dans la nuit[^1-contre-nuit_23].

```{=tex}
\vspace{1\baselineskip}
```
Que ces mots, que ce temps, que ces contre-chants soient un appel à une résistance[^1-contre-nuit_24].

```{=tex}
\vspace{1\baselineskip}
```
Que ce livre n'existe pas, mais qu'il fasse exister des regards. Cela est suffisant[^1-contre-nuit_25].

Qu'il se disperse[^1-contre-nuit_26] s'il réussit à relier des expériences, des colères, des attentions et des oublis, des espoirs, des temporalités dans la nuit.[^1-contre-nuit_27]

```{=tex}
\vspace{1\baselineskip}
```
Nuit reliée faisant suite à une nuit reniée.

```{=tex}
\clearpage
\Huge
```
Où est ta nuit ?

dis-moi

Où en es-tu de ta nuit ?

```{=tex}
\normalsize
```
## Des étoiles mineures

Les étoiles sont devenues en minorité dans notre expérience du monde. Obsolescence du ciel. Notre *être-aux-étoiles* est diminué plus qu'à aucune autre époque de l'humanité.

```{=tex}
\vspace{1\baselineskip}
```
Aujourd'hui, *on décompte les étoiles*.

Ambiguïté d'un double sens. On les décompte et on les retire de notre univers mental et ventral, de notre nuit plurielle.

```{=tex}
\vspace{1\baselineskip}
```
On les décompte parce que jamais dans notre histoire on n'a détecté autant d'étoiles. Un clignement d'œil et des galaxies apparaissent dans l'espace profond qui n'était qu'un éclat de noirceur. Une année et des exoplanètes s'agitent dans les rétines synthétiques des télescopes.

Et pourtant.

Alors que la pensée devrait être galactique, jamais les étoiles n'ont été moins réelles pour nous et de concert leur imaginaire plus absent.

```{=tex}
\vspace{1\baselineskip}
```
Chaque jour on dé-compte les étoiles de nos ciels lourds de pollution et de lumières des monades urbaines où se pétrifie la population mondiale.

```{=tex}
\vspace{1\baselineskip}
```
La nuit fabrique nos imaginaires. Et aujourd'hui notre imaginaire du cosmos est un combustible qui prend feu.

Nous-mêmes prenons feu.

Hors sol, car aussi hors ciel.

```{=tex}
\vspace{1\baselineskip}
```
On a perdu la nuit comme on a perdu le ciel. Incapables de vision. D'écoute. De nuit.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{nsright}
\itshape
Il y a une vérité dans la nuit : un désir.
\end{nsright}
```
```{=tex}
\vspace{1\baselineskip}
```
Désastre, désir --- deux mots qui résonnent avec la nuit et les étoiles.

Sûrement qu'il fallait attendre aujourd'hui pour qu'il résonne de manière singulière, ce vieux mot de *désidération*.

```{=tex}
\vspace{1\baselineskip}
```
*De-sidérium, dés-astre* : désir comme perte de l'astre, perte de l'autre. Comme pour figurer sous l'espèce de l'étoile, l'être aimé (et par-delà entendre le Tiers, l'ailleurs, l'autre et le dehors) placé dans cet état d'éloignement infini, tandis que l'on erre, sans repère et sans étoiles, dans l'attente et l'espoir de son retour --- un ciel qui se dégage de ses nuages et laisse apparaître à nouveau le ciel étoilé.

Désir, désidération.

```{=tex}
\vspace{1\baselineskip}
```
Aujourd'hui ces faits de langue se sont vidés de leur sens. Dans le désir et le désastre, nous n'entendons plus la nuit d'où ils viennent. Alors on a pu parler à nouveau de *désidération*, pour marquer, par le déploiement visible de l'étymologie ce qui fait notre sidération et ce qui fait obstacle à notre désir de retrouver la nuit et les étoiles.

Comme si la perte du ciel et la perte du monde, du cosmos, sa destruction allaient de pair, et qu'elles s'entendaient maintenant aussi comme une terrible perte du désir, comme une pulsion de mort. *Malaise dans les étoiles* hanté par la libido morbide des technosciences.

Nous avons perdu le désir, et la nuit, et les étoiles.

De cette solidarité du désastre la désidération est l'apparition et la promesse d'autre chose, d'un désir à nouveau, d'une contre-nuit aussi.

```{=tex}
\vspace{1\baselineskip}
```
*Neon genesis* --- une nouvelle digenèse[^1-contre-nuit_28] de nos nuits à l'heure des néons, à l'heure de la disparition de la nuit.

```{=tex}
\asterisme
```
Pourquoi un jour la nuit s'est effacée ?

Pourquoi un jour les étoiles se sont absentées ?

Elles chantent désormais à des fréquences mineures, un peu tristes, que nous ne percevons même pas.

En majorité dans le monde, en minorité dans notre expérience quotidienne, mentale et culturelle.

```{=tex}
\vspace{1\baselineskip}
```
Nous ne nous souvenons pas de ce que nous avons oublié.

Et pourtant cet oubli fait toujours retour --- et nous devons le reconnaître et retourner vers le désir.

```{=tex}
\vspace{1\baselineskip}
```
Que la nuit n'existe plus, que ce trauma soit invisible et impossible est une grande "pauvreté en expérience" pour le dire avec Walter Benjamin.

```{=tex}
\vspace{1\baselineskip}
```
Chez Benjamin, c'est la Première Guerre mondiale qui laisse les hommes incapables de raconter l'horreur absolue, incapables de faire récit du vécu de la guerre. Il n'y a pas de fable, de récit à tirer de ce qui a ainsi été vécu : telle est l'histoire de nos désastres.

```{=tex}
\begin{centre}
```
"*Pauvreté d'expérience : on ne doit pas comprendre par là que les hommes aient un désir d'expérience nouvelle. Non, ils désirent se libérer des expériences, ils désirent un monde dans lequel ils puissent faire reconnaître leur pauvreté --- l'extérieure et finalement aussi l'intérieure --- de façon si pure et distincte qu'il en sorte quelque chose de convenable. Et ils ne sont pas toujours ignorants ou inexpérimentés.*"

```{=tex}
\end{centre}
```
D'un désastre l'autre, ce qui ne peut être dit, est l'impossibilité de dire, de transmettre, de faire récit à partir de la catastrophe, c'est ainsi que je veux dire que la nuit, notre nuit défigurée, n'arrive pas à être rapportée comme expérience, comme récit, comme rapport au dehors, à l'invisible, à l'inassimilable.

C'est à ce traumatisme de ne pouvoir dire, faire récit que j'indexe ce malheur.

```{=tex}
\vspace{1\baselineskip}
```
Je voudrais parler de la contre-nuit comme une manière de faire exister quelque chose de la nuit dans la nuit, contre l'image de la nuit et avec les récits fragmentés de la nuit. *Neon genesis* inventant la praxis, la poésie, la contre-épopée capable de faire entendre là où nous en sommes de la nuit.

```{=tex}
\vspace{1\baselineskip}
```
De la mélancolie de la *désidération* on passerait ainsi à la révolte amoureuse de la *contre-nuit*, façon de résister par l'imaginaire et le désir, la prise de conscience sensible et globale, d'allumer des contre-feux noirs dans le ciel trop lumineux.

```{=tex}
\vspace{1\baselineskip}
```
Faire la contre-nuit pour ce qui réanime, ce qui résiste, ce qui éclaire et refait l'obscurité, qui donne à la clarté du jour sa profondeur[^1-contre-nuit_29]. Pour ce qui donne des perspectives plutôt qu'une simple profondeur.

```{=tex}
\clearpage
```
```{=tex}
\begin{mdframed}
\footnotesize
\begin{nscenter}
\uline{\textbf{DISPARUE}}
```
```{=tex}
\vspace{1\baselineskip}
```
**Le néon clignote depuis si longtemps qu'on n'y prête plus attention.**

```{=tex}
\vspace{1\baselineskip}
```
**Pour beaucoup son apparition veut dire que dans cinq minutes on quitte la périphérie et qu'on va entrer dans les cinq cœurs trépidants de Capitale Songe.**

```{=tex}
\vspace{1\baselineskip}
```
**On la voit de loin, des cinq continents et même depuis l'espace depuis que des chenilles processionnaires y passent.**

```{=tex}
\vspace{1\baselineskip}
```
**Je me demande si un soir je n'ai pas vu une fille là-haut, sur ce néon annonciateur de Capitale Songe affichant « DISPARUE ».**

**Elle regardait ailleurs. Vers le ciel. C'est ça. Elle avait le coude sur le genou, une cigarette à la main. Ça me revient. Et puis elle avait cette tête rasée et tatouée qu'on se faisait à l'époque. D'aussi loin c'était beau de l'imaginer, mais aussi un peu triste. Pourquoi elle fumait seule en regardant la nuit, assise sur le néon la disant :**

```{=tex}
\vspace{1\baselineskip}
```
`\uline{\textbf{DISPARUE}}`{=tex}

```{=tex}
\vspace{1\baselineskip}
```
**Peut-être pensait-elle à cette boule de manque incompréhensible et chaude comme un animal de compagnie qu'elle aurait perdu et qui se serait appelé Nuit. Peut-être pensait-elle à autre chose. J'entrais dans le premier bar et je commençais ma nuit d'oubli.**

```{=tex}
\end{nscenter}
\end{mdframed}
```
```{=tex}
\clearpage
\Huge
```
"Vous êtes tous si fatigués --- et cela seulement parce que vous ne concentrez pas vos pensées autour d'un projet très simple, mais d'une très grande ampleur."

```{=tex}
\normalsize
```
## D'une Promesse

*Vois-tu ce que ça veut dire ?*

```{=tex}
\vspace{1\baselineskip}
```
On n'y voit rien.

Espoir noir --- promis.

Espace noir --- promis.

```{=tex}
% \vspace{1\baselineskip}
\asterisme
```
C'est donc la nuit, la nuit radicale, que je désire, la contre-nuit qui se forme contre toutes les dévastations du ciel nocturne et de la vie[^1-contre-nuit_30].

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
\itshape
Tout devrait parler depuis le futur
\end{nsright}
\vspace{1\baselineskip}
```
Car sinon, quelles que soient nos actions (ou plutôt notre profonde inaction) --- rien ne changera.

```{=tex}
\begin{centre}
```
C'est la cosmopolitique qui doit remplacer toute politique^.^

Cosmo-politique, vraiment, c'est-à-dire placer le cosmos au centre de toute politique populaire --- que le cosmos remplace le monde humain, remplace le monde animal, qu'il devienne le plurivers, les milliers de perspectives égalitaires.

```{=tex}
\end{centre}
```
```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
\itshape
```
Nos rêves ne nous jugent pas

Ils nous promettent

Ils nous prophétisent

Les yeux ouverts

Sur notre nuit présente

```{=tex}
\end{nsright}
\vspace{1\baselineskip}
```
Ceci est le contraire d'une utopie.

```{=tex}
\vspace{1\baselineskip}
```
C'est l'ailleurs qui se multiplie. Qui se fait inconnu. Désir de savoir. De changer. De se donner à l'ailleurs qui se donne partout.

Ailleurs inappropriable, désir de savoir dont les étoiles sont comme l'emblème.

```{=tex}
\vspace{1\baselineskip}
```
Faire des étoiles vives comme de vives araignées dans l'océan de la nuit morte.

```{=tex}
\vspace{1\baselineskip}
```
Faire que le plus lointain, le plus difficile à rapprocher de nous, soit le plus essentiel, c'est (déjà) ce que les peuples anciens de par le monde avaient réalisé en en faisant les corps de divinités vivantes.

```{=tex}
\vspace{1\baselineskip}
```
À l'heure où l'on pense vides ces mondes éclairés par les étoiles, et mortes ces lumières-fossiles qui nous parviennent durant la nuit, il faut parvenir à cette opération mentale de revivifier les étoiles et de changer notre imaginaire du monde.

```{=tex}
\vspace{1\baselineskip}
```
Ceci n'est pas une utopie.

```{=tex}
\vspace{1\baselineskip}
```
Car il ne s'agit de rien de moins que de réévaluer la valeur esprit dans l'organisation de l'économie et des sociétés[^1-contre-nuit_31] en faveur d'un *contre-bloc hégémonique* dont l'appel désespéré aux cendres de Gramsci ne change rien à l'ardeur du sentiment.

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
\itshape
```
Des cendres dans le ciel

Des taches

Appelle-les étoiles

Toi aussi

Des soleils morts

Bientôt

En vie

```{=tex}
\end{nsright}
\vspace{1\baselineskip}
```
C'est donc la nuit radicale à laquelle il y a à se promettre et que j'appelle contre-nuit.

```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit faite de nuit et d'autre chose que la nuit, de promesses et d'imaginaires. De tout ce que l'obscurité laisse imaginer et ne dévoile jamais (à qui le dire ?). Isis errante, laissant son voile noir se déposer comme la toile d'araignées communautaires et égalitaristes[^1-contre-nuit_32].

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
\itshape
```
D'autres rêves, d'autres fictions, d'autres mythes.

Parole

d'ailleurs.

```{=tex}
\end{nsright}
\vspace{1\baselineskip}
```
Nuit radicale revenant au principe premier de toute société humaine\ 
--- fiction[^1-contre-nuit_33]

Le monde est animé de fictions (animisme sans dieux, mais faitichisme bien réel[^1-contre-nuit_34]).

Alors il faut changer de fiction : promesse de l'inespérable, politique de l'impossible, exigence pourtant infinie.

Ce qui nous lie, obscurément dans la catastrophe.

Sans secours, miracle, salut --- politique[^1-contre-nuit_35].

```{=tex}
\vspace{1\baselineskip}
```
Promettre --- *pavot et mémoire*.

```{=tex}
\vspace{1\baselineskip}
```
Peut-être que nous, Extrêmes-Occidentaux, sommes incapables d'entendre cette voix des morts à venir, cette voix des morts passés et à venir, des morts-vivants présents ne cessant de fuir les mégafeux, les zoonoses des marchés de nuit, les villes-noyées, les villes-noyades, les villes-néons, les continents désertifiés, les zones irradiées. *Idées pour retarder la fin du monde* : adopter le point de vue de la nuit et de tous ses habitants.

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
```
« *Puisque nous sommes les étoiles. Puisque nous chantons.*

*Puisque c'est par notre lumière que nous chantons.* »

```{=tex}
\vspace{1\baselineskip}
```
Chant des Peskotomuhkati

```{=tex}
\end{nsright}
```
```{=tex}
\asterisme
```
L'obscurité de ce que désigne la contre-nuit ne se dissipera probablement pas totalement, mais elle t'indiquera un mouvement, un désir, celui de refaire la cosmologie de l'expérience nocturne, d'en refaire le récit, le mythe depuis sa défaite (*amitologie*), la nuit depuis son effacement (*contre-nuit*).

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
\itshape
```
L'obscurité sans ténèbres

Ni arrière-monde

Nous-les-sans-images

Nous-les-sans-visages

```{=tex}
\end{nsright}
```
```{=tex}
\asterisme
```
Que ce livre n'existe pas mais promette

mais *se* promette

promesse à égaler aux cieux.

Voilà mon vœu.

Voilà mon ciel et mon contre-ciel[^1-contre-nuit_36]

ma nuit et ma contre-nuit 

un événement et sa vacance

mon manuel de désistānce\*

ma contre-nuit

Elle qui n'est pas de la poésie, car retirée de l'image,

et se donnant comme expérience limite.

```{=tex}
\vspace{1\baselineskip}
```
En dernier lieu,

c'est cela que je voudrais qu'on garde à l'esprit : que la contre-nuit est autre chose qu'un concept, une idée, une histoire alternative à la nuit électrique qui est la nôtre, mais que la contre-nuit tente de désigner (mal, mais ce mal, dira-t-on, est nécessaire --- une expérience et une façon de penser) (ce qui ensemble forme pourtant aussi une fiction) quelque chose de la pensée dans sa suspension, de l'image dans sa soustraction, de l'existence comme dépassement.

```{=tex}
\vspace{1\baselineskip}
```
Et pourtant autre chose.

Toujours et autrement.

```{=tex}
\vspace{1\baselineskip}
```
On me demandera davantage de précisions, de définitions, de clarté, d'exemples, de fictions.

Je répéterai que ce que l'on tend à définir est en perpétuel déplacement. S'échappe, se transforme depuis l'informe. En chacun. À travers et par-delà. Ce ne sera jamais assez --- c'est essentiel qu'il en soit ainsi.

```{=tex}
\vspace{1\baselineskip}
```
Tu m'accuseras peut-être de nihilisme, de comploter à la résurrection d'une énième mystique négative, de mettre le néant à la place du monde --- alors que la contre-nuit a quelque chose de la joie du minuit profond et voudrait être chant, épopée, même oppositionnelle, voulant la joie du minuit profond[^1-contre-nuit_37].

```{=tex}
\vspace{1\baselineskip}
```
C'est la vastitude du bonheur et de l'impossible contre le manque du monde et de l'expérience nocturne qui se donne avec cette contre-nuit.

```{=tex}
\vspace{1\baselineskip}
```
Oui, et pourtant, encore : autre chose.

```{=tex}
\vspace{1\baselineskip}
```
C'est de cette réalité hybride (la pensée) que je veux te parler.

Une contre-nuit encore à reconnaître, et mieux encore à réaliser, un pas au-delà.

Par toi.

```{=tex}
\vspace{1\baselineskip}
```
C'est une promesse à accomplir par chacune et chacun.

C'est une autobiographie de la nuit à mettre en commun depuis nos impossibilités, nos silences, nos solitudes.

```{=tex}
\vspace{1\baselineskip}
```
Dire ce que la nuit détruite et déconstruite nous apprend, nous apprend à voir et à sentir et pas simplement le désastre réel des nuits défigurées ou les nuits du passé sublimées dans l'imaginaire (il faudra pourtant les dire aussi, tout ce qui en nous vient du Romantisme).

Dire ce que l'anthropologie, la philosophie, la science, la poésie permettent de penser du mouvement de la nuit et de la contre-nuit.

```{=tex}
\vspace{1\baselineskip}
```
C'est ce qui s'essaie. C'est ce qui se promet.

```{=tex}
\vspace{1\baselineskip}
```
Peut-être que ce livre ne sera que ce *peut-être*, que cette annonce, que cette approche, que cette tentative, peut-être qu'il ne sera qu'un livre des seuils, fait de points noirs dans le noir, des points d'une obscurité particulière formée à force de fermer trop fort les yeux, des points noirs comme des morceaux de texte dans la toile de nuit du monde, voilà encore ce qu'il faut dire et refuser d'un même mouvement.

```{=tex}
\vspace{1\baselineskip}
```
Ce livre n'existera pas, donc, je le sais, tu commences à t'y résigner toi aussi, c'est-à-dire peut-être qu'il reste à l'espérer, et c'est ce qu'il y aurait de mieux : qu'il n'y ait là que des paroles ouvertes, perpétuellement ouvertes, et des annonces faites pour le vide, lui qui n'entend rien, et qu'il faut pourtant traverser jusqu'à faire l'écoute --- c'est pourquoi il faudra écrire avec quelque chose de la nuit, d'une persévérance de nuit.

```{=tex}
\vspace{1\baselineskip}
```
Quelque chose s'annonce. C'est toujours ainsi. Une déliaison à un moment, un livre de seuils et du sable des étoiles.

```{=tex}
\vspace{1\baselineskip}
```
Recommencer.

```{=tex}
\clearpage
```
```{=tex}
\Huge
```
Prophétie

quelque chose

je ne sais quoi

paroles obscures faisant la vie

```{=tex}
\normalsize
```
## Nuitamant

Alors il faudra se répéter.

```{=tex}
% \vspace{1\baselineskip}
```
Alors il faudra aimer ces textes faits d'ellipses, ces orbites de fiction qui dessinent cette contre-nuit.

Contre-nuit comme un astre noir dans la nuit jamais noire --- nuit qui se devine par la déviation qu'elle imprime à la lumière, à notre pensée désastrée.

Contre-nuit tout à venir qui devra un jour se répandre sur toute la nuit jusque dans les ombres du jour.

Contre-nuit désirante contre toutes les prédations et toutes les conquêtes : "noyau irréfragable de nuit[^1-contre-nuit_38]".

Contre-nuit cruelle, fatale, féroce s'il le faut, contre-nuit abreuvée du lait noir d'Artémis, de Kali et de Chaos, contre-nuit Gorgone, Corail, Planctonide, s'il faut faire se rejoindre les mythes à la pensée pour changer de régime de ciel.

```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit béance

Contre-nuit géante

Contre-nuit ouverte

par sommeils et discontinuité

par générosité et pluricité

Contre-nuit aimante

```{=tex}
\vspace{1\baselineskip}
```
Il faudra tant de textes

et inlassablement les répéter

les faire s'altérer

chant --- contre-chant

recommencés sans cesse, approchant, distançant ce qui est autre chose qu'une idée, qu'un vertige depuis ce point critique de notre temps de désidération, une écriture depuis la nuit aveugle de notre présent : une histoire sans nuit, une mythologie sans récit, une figure sans figure.

```{=tex}
\vspace{1\baselineskip}
```
Restituer cette obscurité, ses mouvements neutres, ses contradictions puissantes et créatrices.

```{=tex}
\vspace{1\baselineskip}
```
Alors ferme les yeux, reprends ton souffle, ralentis, laisse les mâchoires préhistoriques disloquer la Modernité qui t'a fait croire à ta dissociation d'avec le monde, du jour d'avec la nuit, du passé d'avec le futur, du sujet et de l'objet, et te relier au passé, au futur, à l'impossible et au futur, à la nuit comme au jour.

La nuit à présent n'existe que sous forme d'absence, de creux, de soustraction.

Elle n'existe plus, et pire encore, elle ne te manque pas.

C'est la plus triste des histoires d'amour.

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
\itshape
```
Aime la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Ne pleure pas.

Résiste.

Et pleure, tout en résistant.

```{=tex}
\end{nsright}
\vspace{1\baselineskip}
```
La nuit cosmique n'est pas une, n'est plus cette unité magique et ordonnée des cosmologies antiques. Aujourd'hui, dans un monde désastré, elle peut être relation.

```{=tex}
\vspace{1\baselineskip}
```
Pendant un temps les civilisations se sont inventé des récits. La nuit a été le temps de ces récits. Les conteurs d'Afrique et de la Caraïbe, les veillées d'Occident, les poèmes d'Ovide où les humains ont la tête vers le haut afin d'être tournés vers les étoiles, la planète Vénus indiquant aux bergers par son apparition au crépuscule et à l'aube le moment de sortir ou de rentrer les animaux, les constellations scandant les saisons pour les agriculteurs, définissant le temps des semailles et des récoltes, les augures et les hospices politiques. Autant de leçons concrètes, de choses de la nuit, de craintes, d'espoirs, de rapports concrets, poétiques, complets qui faisaient civilisation. Il ne sert à rien de regretter ces temps et les cavernes merveilleuses où l'on rêvait aux animaux recréés dans le ciel.

C'est ce présent du désastre qu'il faut écrire dans nos récits, nos contre-chants. C'est avec le narcocapitalisme qu'il faut défaire ce qu'est la nuit comme perpétuel éveil et intensification, comme fête et défaite, excitant et somnifère, travail précaire.

```{=tex}
\vspace{1\baselineskip}
```
La contre-nuit est une des alternatives à cette nuit annulée, à cette nuit unidimensionnelle, illuminée en permanence, dissolue en absence de cosmos comme ce qui relie les expériences, les animaux, les pierres, les étoiles, les récits, cette nuit cosmique, unique mais n'existant qu'en constellation de relations, reliant le passé et le présent, le possible et l'impossible, le sujet et l'objet, le jour à la nuit pour inventer.

```{=tex}
\vspace{1\baselineskip}
```
Il est temps de rendre la nuit à la nuit, à la pluralité d'expériences de la nuit dans la nuit, non pas une, mais multiple, multiple et reliée en archipels stellaires, reliée comme des constellations, par l'esprit, par l'imaginaire.

```{=tex}
\vspace{1\baselineskip}
```
Et dont la langue porte la trace : désir --- désastre.

```{=tex}
\vspace{1\baselineskip}
```
Or la nuit n'est plus un récit fondateur, n'est plus la fille du Chaos, n'est plus l'œuf de Cobra Grande, elle est constellée de trous noirs, d'oublis : cette perte ne doit pas être la source fantasmatique de nostalgie pour cette unité divine du Grand Récit, mais doit nous amener à composer dans les trous une *amitologie* dont il nous faut à nouveau faire les récits proliférant comme les mites.

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
\itshape
```
Si je comprends

nous n'aurons jamais pleinement la mémoire

inespérée, intermittente

d'être de la même matière

dont sont faits Saturne, Jupiter ou le soleil

atteignant de la conscience cosmique

d'être poussières d'étoiles

car nous sommes aussi

autre chose

complexité qui est la vie

le ciel et la terre

l'univers-terre

étoiles-bactéries

poussières de Terre

et poussières de Nuit

magie des coïncidences fatales

nous-mêmes

poussières de destruction et de création

souffles d'étoiles

et larves du futur

```{=tex}
\end{nsright}
\vspace{1\baselineskip}
```
La nuit n'est plus un temps hors du temps, n'est plus une interruption merveilleuse du temps quotidien mais une continuité électrique, une Alpha-nuit pleine de lumière perpétuelle.

```{=tex}
% \vspace*{\fill}
\vspace{1\baselineskip}

\begin{nscenter}

  \hspace*{1em}*

  \hspace*{-1em}*

  \hspace*{-3em}*

  \hspace*{0em}*

  \vspace{1\baselineskip}

  \hspace*{3em}*

  \vspace{1\baselineskip}

  \hspace*{1em}*

  \hspace*{-2em}*

  \hspace*{-1em}*

  \vspace{1\baselineskip}

  \hspace*{0em}*

\end{nscenter}
```
La nuit n'est plus une initiation au dehors, une étrangeté à respecter, à affronter via l'épreuve, *nuit obscure*[^1-contre-nuit_39] ou fantastique --- le sens du temps de la nuit s'est forclos dans des lieux --- boîtes de nuit comme seul rituel de passage à l'âge adulte, enfermement dans des nuits secondes plutôt que face à la nuit cosmique, désormais invisible dans la plupart des villes.

```{=tex}
\vspace{1\baselineskip}
```
C'est une rupture anthropologique que ce retrait de l'expérience de la nuit qui a formé les communautés humaines, leur imaginaire, leur art, leur sociabilité, leurs initiations, leurs mythes.

```{=tex}
\vspace{1\baselineskip}
```
Alors il faut une lecture pour d'autres sommeils, pour la résistance de la nuit dans l'absence de la nuit : une contre-nuit intempestive "*contre ce temps, en faveur, je l'espère, d'un temps à venir*[^1-contre-nuit_40]".

```{=tex}
\clearpage
```
*Ne plus voir le livre, la porte, la nuit.*

*Voir l'espace, l'écart, la contre-nuit.*

```{=tex}
\vspace{5\baselineskip}
```
*C'est à ce genre d'exercice que je me plie. Finis les préludes, que commencent les impromptus, dans cette fragilité risquée à maintenir, dans le visage et dans le regard, dans le visage du monde et dans le regard du ciel.*

## Mille milliards de nuits et une nuit cette éclipse de nuit

Sortir de la lumière n'est pas revenir à la nuit.

Voilà un des principes de l'analyse radiale.

Quand j'ai fondé l'analyse radiale, on n'y croyait pas.

Parce qu'on m'a dit que les masses étaient stupides. Qu'il leur fallait un truc plus grossier. Des pullulements de Zarathoustra-zombies éclatés au sol, rampant et gerbant le nihilisme radioactif de notre époque.

Je n'y croyais pas.

Enfin, peut-être me suis-je trompé.

```{=tex}
\begin{centre}
\itshape
```
Le jeu m'a offert un empire. Je l'ai liquidé. J'ai investi dans le cosmos. J'en ai fait du fuel. Le fuel a rempli mes rêves. J'ai transformé mes rêves en bitcoins. J'ai dématérialisé l'univers. Je l'ai vendu à une autre divinité. Elle me l'a rendu. Je lui ai fait un procès. J'ai laminé toutes les garanties. Le bonheur était mal assuré comme toujours. J'ai récupéré des écailles de tortue complètement calcinées. Je dois refaire la nuit et l'univers avec ça.

```{=tex}
\end{centre}
```
Mais il faut pourtant donner à penser autre chose que la monstruosité de notre temps. Contre-nuit autre-que-noire, autre-que-nuit, musique faite de toutes les variations nocturnes qui font de la nuit autre chose qu'un mot : un temps devenu lieu, un lieu devenu souvenir, devenu temps, devenu animal --- tout ce qui s'est dérobé à notre existence.

C'est cela que l'analyse radiale (latérature, disions-nous) présente par déplacement par rapport à d'autres temps (passé, futur), à d'autres cultures (les nôtres, les autres). Façon non de se plonger dans la nostalgie mais d'exciter notre curiosité à venir, de provoquer à inventer d'autres nuits, pour le plaisir, et parce que l'urgence de le faire est absolue.

```{=tex}
\begin{centre}
\itshape
```
Nos illuminations ont le goût métallique du tungstène et l'odeur déchirante du néon. Oh, ça me plaît grave. Grave comme la douleur grosse comme le monde. Une boule de flipper cosmique. Oh oui, nos illuminations ont le goût métallique du tungstène et l'odeur déchirante du néon.

Je me rappelle de la lumière du distributeur de sodas comme d'autres l'apparition de la Sainte Vierge en fin de soirée, avec son collier de canettes pleines, ses yeux noirs, pétillants, et son réconfort glacé entre ses bras angéliques. Je la vois, j'entends son ronronnement divin, sorte de souffle électrique de ventilateur et de consolation infinie. Je me rappelle la gorgée de fraîcheur au bout de la nuit, dans une solitude sacrée et cette phrase qui me traverse comme la grâce : nos illuminations ont le goût métallique du tungstène et l'odeur déchirante du néon.

```{=tex}
\end{centre}
```
Cet éclairage intérieur est tout aussi important que d'adresser la question de l'éclairage extérieur, le "droit à l'obscurité" autant que le devoir à l'impersonnel et à la nuit. En vérité ce qu'il nous faut est une révolution dans nos façons de vivre, d'habiter, de produire, d'échanger, de penser ce qui n'a pas de prix. Voilà ce qui pourrait s'appeler l'éclairage intérieur. Nos nuits renversées. Ceci n'est pas un luxe, c'est la nécessité même.

```{=tex}
\vspace{1\baselineskip}
```
Ce sont ces sensibilités qu'il faut penser dans la persistance et le trouble se déployant en auras nocturnes.

```{=tex}
\vspace{1\baselineskip}
```
Parler de la nuit est politique. Toute politique construit un imaginaire. Toute nuit construit une politique. Il nous faut quelque chose de la nuit, du cosmos pour affronter la crise intégrale que nous vivons.

```{=tex}
\vspace{1\baselineskip}
```
Nous devrons toujours créer la nuit pour qu'elle soit un don.

```{=tex}
\cleardoublepage
```
ce qui veut mourir

ce qui ne veut pas mourir

l'étoile dans l'étoile

sans oubli et sans mémoire

témoigner pour ça

langue de la nuit des camps

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
```
langue de Celan

langue de Sachs

des Prophètes morts exterminés

cette langue sans langue

nuit sans nuit des camps

point aveugle de la Shoah

étoile sans rédemption

```{=tex}
\end{nsright}
\vspace{1\baselineskip}
```
TOUTES\*

```{=tex}
\vspace{1\baselineskip}
```
et parler de ce que l'on ne peut dire

"l'infracassable noyau de nuit" de Cobra Grande

l'obscurité qui appartient à l'autre

l'espoir

aussi

```{=tex}
\vspace{1\baselineskip}
\begin{nsright}
```
la peau noire qu'il faut se faire autrement

depuis les visions noires de l'afrofutur

parler depuis des noirceurs colorées d'autres possibles

depuis ces intimités extérieures

```{=tex}
\end{nsright}
\vspace{1\baselineskip}
```
# Deuxième Cercle / Deuxième Ciel / Désoccultation de la contre-nuit

```{=tex}
\def\addquotewb
{%
  \begin{tabular}[b]{p{3.5cm}}
    \begin{nscenter}
    « Les idées sont les étoiles,
 à l’opposé du soleil de la révélation »\\
Walter Benjamin
    \end{nscenter}
  \end{tabular}%
}
```
```{=tex}
\begin{pullquote}{object=\addquotewb}
\itshape
```
« Rien ne distingue autant l'homme antique de l'homme nouveau que sa manière de s'abandonner à une expérience cosmique, que le second ne connaît guère. Sa disparition s'annonce déjà à l'apogée de l'astronomie, au début des temps modernes. Kepler, Copernic, Tycho Brahé n'étaient certainement pas guidés par des impulsions scientifiques en tout et pour tout. Mais il y a pourtant dans l'insistance exclusive portée à la relation optique à l'univers, à laquelle, très tôt, l'astronomie a mené, un signe avant-coureur de ce qui devait arriver. Le rapport de l'Antiquité au cosmos se déroulait autrement : dans l'ivresse. L'ivresse est bien l'expérience par laquelle nous nous assurons seuls du plus proche, et du plus lointain, et jamais de l'un sans l'autre. Mais cela veut dire que l'homme ne peut communiquer en état d'ivresse avec le cosmos qu'en communauté. C'est le signe d'un égarement inquiétant du moderne que de tenir cette expérience pour insignifiante, évitable, et que de la laisser à l'individu en la considérant comme un rêve exalté pour de belles nuits étoilées. »

```{=tex}
\end{pullquote}
```
## Sortie de nuit

Peut-être commencer ainsi aussi

Nuit humaine trop humaine

De manière multiple

Constat de l'*anthropos* hors-sol ayant perdu toute sa belle flexibilité de scolopendre venimeux

Ayant perdu la nuit et les pattes et la tête

Se saoulant de son poison qu'il bave de partout

S'agitant sans fin

Étrange étranger

Dont les convulsions dessinent

Pourtant pour nous

Un *new dark age*

Important

Formant non pas un *nous*

Mais un *new dark sky*

--- À quelque chose malheur est bon

```{=tex}
\vspace{1\baselineskip}
```
***J'habite Capitale Songe comme tout le monde***

***je prends la N9 plantée de lampadaires ignobles et j'arrive sous les néons où grillent des insectes dont je ne connais même pas le nom***

***"des insectes volants", disent les bombes insecticides,***

***c'est mieux, c'est plus générique, abstrait,***

***mortel***

```{=tex}
\vspace{1\baselineskip}
```
***Eh ouais, les bombes parlent, et pas les insectes***

***ouais, ouais, et ça vous étonne ? Vraiment ?***

***Moi, ça me parle***

***ça pourrait être la mort elle-même qui me parle***

***Qu'est-ce que ça changerait ?***

***C'est peut-être la mort elle-même d'ailleurs qui me parle***

***Pas que celle des insectes***

***La mort même***

***C'est peut-être la nuit même***

***qui me parle***

***génériquement, abstraitement***

***et au générique aucun nom d'insecte ou d'étoile***

***que des oublis, des caractères noirs sur fond noir***

***Allez, va savoir, si la mort nous parlait***

***Et qu'est-ce que ça changerait ? Est-ce qu'on serait seulement capable de l'entendre ?***

***Je ne crois pas***

***je crois qu'elle nous parle depuis toujours***

***et qu'on est incapables de l'entendre***

***et tous ces insectes, toute cette mort, toute cette nuit, gazés, brûlés, consumés par les lampadaires de Capitale S***

```{=tex}
\vspace{1\baselineskip}
```
***Alors je reprends la route***

***et sous le béton une nuit immense se met à m'appeler par mon nom***

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{centre}
```
Alors peut-être commencer ainsi

Peut-être toujours et encore

Commencer par ce que l'on a perdu

Perdu littéralement et perdu de vue plus largement

Quelque chose du sens du temps de l'expérience de la nuit

Un temps qui n'est pas absence

Qui est au-delà du temps

Surtemps et suspens

```{=tex}
\end{centre}
```
```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[6em]{0em}
```
Histoires récits fictions

Prolongations

Temps de la nuit qui est le temps du récit

Ce qui fut la fable

La légende

Et la parole du mythe, du suspens

De la nuit

```{=tex}
\end{addmargin}
```
```{=tex}
\vspace{1\baselineskip}
```
***Je n'avais plus les paupières pour lire***

***personne n'a plus ni ce luxe ni cette patience***

***mais j'avais entendu parler des 21 chants de Tristāne Esver***

***toute la litanie du Grand Sommeil, de la désistānce, de l'Ultranuit, et toutes les figures de mites trouant le continuum de la ville vigilante***

```{=tex}
\vspace{1\baselineskip}
```
**Ça va, ça va, me la refais pas, je connais la chanson**

```{=tex}
\vspace{1\baselineskip}
```

***C'est juste que je ne me rappelais juste pas le nom du bar où l'on chantait ces histoires***

***putain***

***je me serais bien assoupie auprès d'un verre en écoutant ça***

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[6em]{0em}
```
fragmentation faite ville

individu fait absolu

insomnie faite illumination

intensité faite feu et cendre du monde

et du présent

tout le jour dans la nuit

voilà la maladie qui a dévoré la nuit

```{=tex}
\end{addmargin}
\vspace{1\baselineskip}
```
***"Nous les Sans-nuits"***

***c'est peut-être ça***

***C'était ça le nom du bar, je crois, quelque chose comme ça***

```{=tex}
\vspace{1\baselineskip}
\hspace{3em}
```
***Merde, j'ai raté la sortie***

```{=tex}
\begin{addmargin}[6em]{0em}
\vspace{1\baselineskip}
```
Ombre faite monde

Ville faite plante

Liaison faite vie

Sommeil fait fragilité et renaissance du monde

Et de l'absence

```{=tex}
\end{addmargin}
\vspace{1\baselineskip}
```
***Moi j'existe pour ces nuances de néant qu'il y a dans les larmes que j'imagine que tu verses le soir, pour je ne sais quel connard***

***pour moi, c'est la même chose***

```{=tex}
\vspace{1\baselineskip}
\begin{addmargin}[3em]{0em}
```
Ville Ville Ville accélérée par le Vingtième siècle

De l'ère du vertige

Du démon électricité

Au cœur d'uranium durci

Au cœur de barrage stagnant

Au cœur de charbon brûlant

```{=tex}
\vspace{1\baselineskip}
```
Alors recommencer ainsi pour rendre impossible de donner à l'origine une consistance

Au passé une lumière d'absolue vérité par laquelle tout procède

Se retourner perdre recommencer

Apprendre à vouloir-animal

Apprendre à vouloir-fluctuer

Apprendre à vouloir voir les ombres

En devenir et à vouloir

--- je veux dire accepter, penser toujours, prévoir ---

L'imprévisible, se faire bonheur et accident, nuit et contre-nuit, ce qui a décidé et qui décide encore de tout, nous faisant changer, de route, de signe, d'étoile, que sais-je, faisant en nous des étoiles bactériennes, des chemins changeants où l'on s'égare et où l'on invente de nouveaux chemins qui se referment sur nous

```{=tex}
\vspace{1\baselineskip}
```
Ici, en amont de tout discours, de tout savoir --- recommencer

Commencer pour ne jamais finir

Indiquer des points des futurs des dessins qui s'effacent dans le ciel même

C'est peu de le dire

Il faut tenir

```{=tex}
\end{addmargin}
```
```{=tex}
\vspace{1\baselineskip}
```
***Il y avait partout des slogans***

***et des assurances à prendre contre les cauchemars***

***des garanties contre les incertitudes qu'on te vendait mille cents, mille cinq cents, je sais pas comment les gens font pour rassembler de telles sommes***

***ça m'a fait penser à cette araignée qui m'avait sortie de son assemblage de cendre et de bave de veuve noire extraterrestre qu'elle prétendait être la "vraie" arachnomancie (j'en doute et je ne veux pas le savoir)***

***elle m'avait sortie ça de ce mélange gluant d'idéogrammes pour cinglé***

```{=tex}
\begin{nscenter}
```
**Aime l'incertitude de l'avenir**

```{=tex}
\end{nscenter}
```
***Ça m'a fait rire***

***On aurait dit un*** **fortune cookie.**

***Tu sais, dans les cinéfilms on vit ça.***

***Le type brise le biscuit, il ne le mange pas***

***et il lit un oracle.***

***Il le lit comme si le biscuit était fait pour lui alors qu'il a été fait à la chaîne, les mots agencés par un algorithme bidon***

***La destinée industrialisée. Les Parques à l'usine.***

***Mais après tout, c'était moins long qu'une longue cérémonie avec de l'encens ou je ne sais quels autres trucs des religions d'avant***

***Et puis d'autres allaient aux machines à sous, toute la nuit, pour que le seul oracle "sois riche" s'accomplisse.***

***Alors à chacun ses oracles, hein ?***

***Le mien, vomi par l'araignée-pythie, valait ce qu'il valait, pour notre temps, notre temps à nous***

***Mais "aime l'incertitude de l'avenir", c'était trop abstrait pour moi***

***C'est drôle. L'arachnoïde a ri aussi. Il savait que ça ne voulait rien dire.***

***Moi aussi***

***L'insensé***

***C'est moi***

***Et c'est ça que ça voulait dire***

***Être insensé***

***Aime l'incertitude de l'avenir***

***Aime la nuit noire***

***Ce qui arrive vraiment et qui n'est pas prévu***

***Fou à ce point-là***

```{=tex}
\vspace{1\baselineskip}
```
***Pourquoi les oracles putains parlent par abstraction et pas par fiction ?***

***Qu'ils te disent : la nuit viendra, tu auras froid***

***Tu voudras savoir où aller***

***Tes seules amies seront les étoiles lointaines et tu ne pourras pas trop compter dessus.***

***Mais ça ira***

***Ça ira.***

***Tu iras guidé par la distance qui vous sépare, qui t'espace et te guide***

***Ça ira parce que tu crois aux flammes, aux rencontres, aux étoiles, aux paroles et aux erreurs***

***Tout ça est surhumain, on le sait***

***Et on voudrait ça pour toute la surhumanité***

***L'incertitude de l'avenir, les dérives des rêves, la volonté de se survivre***

***à nouveau***

***à renouveler chaque nuit***

***Vouloir l'oubli***

***Vouloir vouloir***

***Vouloir voir***

***Vouloir vivre***

***Voir encore***

***D'autres nuits, d'autres étoiles incertaines.***

***L'incertitude***

***Le couteau des rencontres***

***Et toujours la Lune, calme à travers le ciel cependant.***

```{=tex}
\vspace{1\baselineskip}
```
Comment vouloir la nuit à nouveau dans sa profondeur et ses surfaces, dans sa matière et dans son esprit, dans ce qui la fait vie et infini ?

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
Je n'aurai donc pas su te convaincre.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
Tes paupières sont lourdes.

```{=tex}
\vspace{1\baselineskip}
```
***Si le repos était là***

***Si la nuit était là***

***Si le sommeil était autre chose qu'un temps perdu à la recherche de la vie***

***Si tu me racontais une histoire***

```{=tex}
\vspace{1\baselineskip}

\begin{addmargin}[3em]{0em}
```

Peut-être que cette absence et ce refus ont-ils eu un sens plus grand que je ne l'imagine pour désigner cet imaginaire négatif de la contre-nuit devenue mon ciel, mon très cher ciel, collé à ma peau, aux parois de mes veines, aux méninges de mon cerveau, aux membranes de mes rêves.

Maintenant pourtant il faudra dire cette nuit qui se retire à l'humain. Cette nuit qui s'est donnée et se retire, qui s'est donnée et qui s'est dérobée, qu'on nous a dérobée par tout un ensemble d'*apparatus*, mot magique surgi des calcifications de nos langues-poussières, celles qui, pourtant vives, vivent encore en nous.

```{=tex}
\end{addmargin}
```

## Prière d'insérer dans votre nuit

```{=tex}
\begin{centre}
```
***Et plus encore : de quelle nuit sommes-nous capables ?***

```{=tex}
\vspace{1\baselineskip}
```
On pourrait s'arrêter là

Il n'y a pas vraiment d'autres vérités que la question

*De quelle nuit... et cetera*

Encore encore encore

De quelle nuit sommes-nous capables

Sans raison ni fin 

Sommes-nous capables de cette tendresse neutre

Sans X ni Y ni ordonnée ni abscisse ni rien ni rien

Sommes-nous 

Défaits par ce qui fait la nuit

Différences en suspens

Sommes-nous capables d'être l'irréductible de la nuit dans la nuit

Corps de nuit, part de nuit

Sommes-nous capables d'être à la nuit comme à autre chose qu'à une image

Sommes-nous capables d'être présents à la nuit

De changer de temps et d'espace pour inclure d'autres nuits que la nôtre, d'autres espaces, d'autres référents

Ni penser ni rêver

Sommes-nous

Question sans réponse

Alors défaisons-nous

Communauté de nuits et de solitudes libérées

Par des absences

Médiatisées par le vide qui relie

```{=tex}
\vspace{1\baselineskip}
```
***Alors encore et toujours, nous demander :***

***De quel*** **nous** ***sommes-nous capables en cette nuit ?***

***Capables de*** `\textbf{\uline{nous},}`{=tex} ***ce serait déjà un miracle.***

```{=tex}
\vspace{1\baselineskip}
```
La nuit a fait nos corps-questions

disparates

et ensemble

avec la contre-nuit

anonyme --- sans sujet, radieuse

Contre l'image de la nuit même --- en faveur d'autres nuits

D'une polysémie de la nuit

***Les miracles et les révolutions n'arrivent-ils pas plutôt de nuit ?***

***Je vous laisse vérifier sur les Internets, auprès des rêves coincés dans les tourbillons des tempêtes solaires, dans les lumières perpétuelles des néons, dans les oracles téléphoniques, enfouis dans les sols électriques, parmi les béatitudes dansantes, dans les colloques loin des machines, des insurrections de place en place scandant des révoltes contre ce que le jour a désigné : l'Un-Unique-Absolu-Malheur, Malheur-Et-Pensée***.

Peut-être que cette diversité de nuits devenues lierres, devenues oppositionnelles, venins et poisons, épineuses et florissantes, nuits défigurées et réfigurées, nuits remédiantes et transfigurées

Peut-être que toutes ces nuits d'insomnie n'arrivent plus à accueillir cette ***Toute-Nuit***, ce cosmos sidéral qui ne nous occupe, Modernes croyons-nous, que comme une extériorité, un dehors que nous ne voulons plus voir qu'à distance, médiatisé par une absence au monde

Nuit dans la nuit, la contre-nuit est le don fragile, vulnérable, l'incroyable variabilité, la puissance impuissante et transformatrice de l'imagination

Nous sommes de la nuit, de ses récits face au ciel étoilé

Nous ?

```{=tex}
\end{centre}
```
**J'en appelle à toi**

```{=tex}
\vspace{1\baselineskip}
```
**Parle**

**Vis**

**Raconte**

**Témoigne**

```{=tex}
\begin{centre}
```
Nuits^n^ multipliées par les brouillards de pollution, les nuits d'oranges mortes, les nuits stroboscopiques, les nuits altérées d'autres nuits, toujours altérées, d'ailleurs, nuits penchées, nuits naufragées, d'autres ciels, allez voir, nuits que l'on croit sauvages et qui ne sont qu'archaïques --- nuits auxquelles rêvaient les dinosaures les plus intelligents, auxquelles songent peut-être les phalènes avant de grésiller contre les lampadaires, malédiction --- et la mienne aussi, la nôtre aussi (*capables de nous ?*) --- depuis tant de spectacles, de terribles spectacles, alors reste-t-il un peu de temps pour ce temps d'autres nuits, d'autres chants, pour notre nuit plurielle --- notre chant, notre pluriel absolu, notre absolu fait pluriel, ***étrange, absolu --- étranges, pluriels, imparfaits***, nos superlatifs intimes, d'ici, et d'ailleurs aussi, je ne sais pas, peut-être écrit-on pour le découvrir, on ne le découvre pas, *absolus et jamais absous de cet absolu, étranges, pluriels, imparfaits*, en tout cas cela nous qualifie bien, cela nous a fait et continue de nous constituer nuit après nuit

```{=tex}
\begin{nscenter}
```
« et l'abstraction fut ma damnation, ma présence

et mon éternité »

```{=tex}
\end{nscenter}
```
```{=tex}
\end{centre}
```
## Évangile vide de la nuit vive

*On me dira qu'il n'y a pas de bonne parole. On me dira que la nuit est la nuit. Dysangile du silence infini et du vaste chaos[^1-contre-nuit_41]. Nuit néante et résistante pourtant. Nuit à notre démesure. On me dira qu'il faudrait parler en se soustrayant, en se soustrayant soi pour faire apparaître le monde tandis que les chiffres de notre compte à rebours mortel est affiché sur la Lune. On me dira --- et je rirais sûrement, je rirais encore, car je me dirai qu'il faut toujours échapper en tous sens, toujours, se déborder en tout temps, pour atteindre cette nuit toute fracturée en vérité, en tous sens, étoiles --- dimensions.*

```{=tex}
\vspace{1\baselineskip}
```
*J'aurai des faiblesses tendres et des largesses aiguës pointant vers des mers des Sargasses placées dans la folie inhabitable du ciel, pour cette nuit qui nous emmène vers ce qui s'angoisse en libérant des glapissements sauvages, tandis que la nuit, oui,* tandis que la nuit *je voudrais ne te dire que ça, que cette phrase tranchée d'où sortiront toutes les têtes de toutes les hydres hybrides dont nous aurons besoin du tourbillon, de la douceur féroce et de la pensée proliférante.*

```{=tex}
\vspace{1\baselineskip}
```
Toi aussi, enfant du sifflement du bitume fondu, de la nuit couleur du vide, du larsen abyssal, écho de cette vérité déchirante qui n'a pas plus de voix, demande-toi.

```{=tex}
\vspace{1\baselineskip}
```
Demande à te soustraire à la voix, au langage, au discours, apprends le silence, la poésie, la multitude.

```{=tex}
\vspace{1\baselineskip}
```
Et apprends la contre-nuit abolissant tout ce qui est, restituant la nuit, refusant tout ce qui, soustrayant les corps, s'accomplit avec la marée noire du ciel et nous asphyxie tous autant que nous sommes, insectes aveugles, singes amoureux, cœurs gargouillant de grenouilles. Jusqu'à tout retirer jusqu'à ce que la nuit soit.

```{=tex}
\vspace{1\baselineskip}
```
On dira. Non, on ne dira pas. Justement. Précisément. Absolument. De tous les adverbes qui dépassent le verbe. Trahissant la nuit. L'éternisant. On dira qu'il n'y a pas de bonne parole pour la nuit, parce qu'il n'y a pas de parole même. Cela même (c'est émouvant jusqu'au néant) --- qui nous dit quelquefois que le discours est sans langue, la pensée sans paroles, nuit hors de tout langage. Alors. Nuit de l'alors. On ne dira plus. Alors elle n'apparaîtra que dans ce refus. Alors on dira adieu, comme on peut, à tout ce qui a été. Comme on peut, c'est-à-dire mal. Mal, mal, mal. Et ce mal, c'est peut-être l'adieu. Va savoir. Alors adieu les prophéties effondrées, les balbutiements des secrets éventés, adieu les trente-trois mots de passe de l'invisible faits pour embrasser le monde du revers de nos paupières, adieu les rages proférées contre les nuits en flammes, adieu les paraboles crashées dans les montagnes de la fable, adieu les corpuscules poétiques brûlés aux lampadaires, adieu les images sauvages, les extases trempées d'acide lactique pour d'autres métamorphoses que la nuit, adieu les mille yeux ouverts pour capter les chaleurs d'autres mondes, adieu les soupirs du ciel, les entrailles des terres-mers, les mascarades trouées par les nuages néfastes pleuvant nuit sur nuit des filaments rouges où des enfants d'ailleurs se blessent.

```{=tex}
\vspace{1\baselineskip}
```
Encore d'autres choses. Encore. Nuit de l'encore après la nuit de l'alors. Nuit du désir. De la parole en son suspens et sa répétition. Nuit de l'alors. Nuit de l'encore.

```{=tex}
\vspace{1\baselineskip}
```
Regarde, là, par terre, le lyrisme expirant à petit souffle, tabassé du métal bleu des profondeurs des choses ouvertes, des veines caves et vides, résonnant de pauvres cris --- mais tout cri n'est-il pas pauvre ? Il y a tant de pitié dans ce cri-là, celui de la nuit, je le crois, avec un dénuement terrible comme savent ceux qui disparaissent. Des fumées bleues montent de partout. On ne doit rien dire. De partout s'intoxique.

```{=tex}
\vspace{1\baselineskip}
```
Que la nuit soit la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Mais elle ne l'est pas. Elle gronde comme une chienne mauvaise à force d'avoir été battue.

```{=tex}
\vspace{1\baselineskip}
```
Que la nuit soit la nuit.

```{=tex}
\vspace{1\baselineskip}
```
C'est ce qu'il faut toujours recommencer. Ce qu'il faut sans cesse refaire. La nuit, contre la nuit. Défaisant l'image, le jour, la nuit, les cortèges de peurs et de bonheurs, les voiles et les nuages. Histoire de recommencement. Pas de magie. Pas de neige noire. Des paroles muettes --- des langages hors langages. Tout ce qui porte à des limites. Allez comprendre. Moi je n'y comprends rien. Je m'efforce. Que la nuit soit la nuit. On se recroqueville dans des formules. Pas même des prophéties. Des prophéties, ce serait bien. On n'a pas la veine. Que la nuit soit la nuit. On répète encore, dans une idiotie délirante, on retourne en soi encore ce mantra :

```{=tex}
\vspace{1\baselineskip}
```
Sans image

Sans langage

Parole sans destination

```{=tex}
\vspace{1\baselineskip}
```
Répétition où la nuit, espérons, se recrée et se disperse et se recommence sans cesse.

```{=tex}
\vspace{1\baselineskip}
```
Alors commencer toujours par l'obscurité. L'obscurité --- ce qui se dit mal. Le mal --- des cicatrices dans le ciel. Le ciel --- tout ce qui a disparu. La disparition --- là où se logent toutes les étoiles. Les étoiles --- les écailles de nos vies. Les vies --- ces sinuosités formant les traces de l'inconnu. L'inconnu --- l'ombre des virages sombres pleine de vierges aveugles, de mers inabordables, d'océans aux cendres liquides versant dans des nuits sans réponses. Des réponses adressées à des absences. Des absences adressées à des absences. À des impuissances triomphantes, des triomphes faits pour d'autres corps, des corps faits pour tous les corps, accueillis dans la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Alors, que la nuit soit la nuit.

# Troisième Cercle / Troisième Ciel / Désanthropologie de la nuit

```{=tex}
\def\addquotekumulipo
{%
  \begin{tabular}[b]{p{3.5cm}}
    \begin{nscenter}
« Rien\\excepté la nuit »\\
Kumulipo de Keaulimoku
    \end{nscenter}
  \end{tabular}%
}
```
```{=tex}
\begin{pullquote}{object=\addquotekumulipo}
\itshape
```
« À l'époque où la terre s'embrasa

À l'époque où les cieux se retournèrent

À l'époque où le soleil s'assombrit

Obligeant la lune à briller

L'époque où se levèrent les Pléiades

Le limon, telle était la source de la terre

La source des ténèbres qui engendrent les ténèbres

La source de la nuit qui engendra la nuit

Les ténèbres intenses, profondes

Ténèbres du soleil, ténèbres de la nuit »

```{=tex}
\end{pullquote}
```
## Amitologie

```{=tex}
\begin{nsright}
"Un récit ? Non, pas de récit, plus jamais."

--- Maurice Blanchot
\end{nsright}
\vspace{1\baselineskip}
```
```{=tex}
\itshape
```
Parfois on a besoin de fable quand on atteint les limites du discours.

Alors le mythe peut prendre la suite de la philosophie non pour la remplacer, non pour la clarifier, mais pour que les images et les pensées forment de nouvelles alliances ne laissant rien à la superstition, confiant tout à l'exigence de l'impossible et de la poésie.

```{=tex}
\vspace{1\baselineskip}
```
Je me rappelle cette fable du *Gai savoir*.

Un insensé se tient sur une place en plein jour, une lanterne à la main. Et c'est lui, l'insensé, celui qui annonce la mort de Dieu à laquelle personne ne veut croire.

"Il faut du temps à l'éclair et au tonnerre, il faut du temps à la lumière des astres, il faut du temps aux actions, même lorsqu'elles sont accomplies, pour être vues et entendues."

Cette mort du Ciel et ce délai cosmique viennent aujourd'hui à prendre d'autres dimensions. Après la mort du Ciel divin, la mort du ciel tout court.

```{=tex}
\vspace{1\baselineskip}
```
Lanterne brillant jour et jour, 24/7, nous avons supprimé le ciel de nos vies, comme nous en avons retranché tout le divin.

Et nous aussi n'arrivons pas à réaliser cette disparition meurtrière.

Cela est pourtant. Divins, le monde, la nuit.

```{=tex}
\vspace{1\baselineskip}
```
C'est une fable. C'est la vérité. C'est ainsi.

Le divin, le monde, la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Non pas qu'il s'agisse d'en revenir à quoi que ce soit (à Dieu, au monde, à la nuit), mais de penser jusqu'au bout les conséquences de cet acte. Non pas de s'en consoler, mais d'y remédier --- de réinventer des médiations, dans une pluralité d'approches (les divinités, les mondes, les nuits).

```{=tex}
\vspace{1\baselineskip}
```
Le désastre aujourd'hui nous oblige. Nous oblige de penser avec lui une cosmologie qui se promette à autre chose que le nihilisme, qui soit autre chose qu'une croyance, mais des manières d'être, de sentir, d'imaginer.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{centre}
```
À nouveau il nous faut renverser le monde, briser les anciennes tables, animer cette cosmologie du désastre avec toutes ces têtes sympathiques des véritables mites de l'écriture creusant dans la matière hégémonique des images résistantes, des contre-mythes (espèce en voie de disparition, cela va sans dire). Ne plus faire figure.

Couper la tête humaine de la mythologie et laisser l'essaim des mites former les étoiles vivantes de notre ciel et composer dans les trous du tissu de notre texte les espaces négatifs médiatisant vraiment la fiction, nos imaginations mais aussi nos impuissances, nos tentatives de métamorphoses, depuis ces figures devenues limaille d'imaginaire à aimanter de nouveau, afin de nous diriger dans la nuit avec notre boussole pointant l'extrême-Orion.

```{=tex}
\end{centre}
```
```{=tex}
\normalfont
\asterisme
```
Dans certaines cultures on écoute le déplacement des arthropodes la nuit. Leur déplacement prophétise le mouvement des étoiles.

```{=tex}
\vspace{1\baselineskip}
```
Je me mets par terre, j'étends les bras et j'écoute, humblement j'écoute.

Ils passent.

Je ne les compte pas. Je ne me débats pas quand ils passent sur moi.

Je veux ces nuits d'esprits et de chairs, de tambours intérieurs disant : *tu veux toi aussi quelque chose de présent et pourtant disparu*.

```{=tex}
\vspace{1\baselineskip}
```
Je veux.

Qu'est-ce que ce vouloir ?

La permanence du désir.

```{=tex}
\vspace{1\baselineskip}
```
Je voudrais interroger des rêves.

Je voudrais dormir dans mes rêves et aller ainsi, plus profondément, toujours plus profondément.

```{=tex}
\vspace{1\baselineskip}
```
Un parasite me rentre dans l'œil. Certaines disent "une étoile". Je la sens sinuer et parcourir mon corps comme pour en prendre connaissance.

```{=tex}
\vspace{1\baselineskip}
```
Je compose avec elle un alphabet secret fait de sommeil.

Je forme avec elle une empreinte dans le ciel.

```{=tex}
\vspace{1\baselineskip}
```
Je voudrais d'autres terres dans la terre noire de nos sommeils.

```{=tex}
\vspace{1\baselineskip}
```
Pour l'instant on règle nos querelles dans nos rêves, au Bureau du Technocapital ouvert 24/7 à Capitale Songe. J'entends à la mondovision qu'un anthropologue est né dans l'espace. Trop heureux il se fait dévorer par le cosmos avec le sourire. Il suffisait de rester sur Terre et de disséquer Jeff Bezos et Elon Musk pour comprendre vraiment l'inertie des sociétés narcocapitalistes. Vraiment, notre manque de gravité est incroyable. Il suffisait de relire Rilke. *Die Schwerkraft*. La puissance de la difficulté, la pesanteur qui fait toute la grâce de la légèreté. Mais il faut danser si longtemps pour tasser la terre jusqu'à la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Je parle trop, je te laisse trop peu imaginer, tendre ton long nez jusqu'au fond de cette nuit d'ailleurs, ici bouleversé.

```{=tex}
\vspace{1\baselineskip}
```
Plus de ça. Je veux faire des rituels même si je ne les comprends pas encore.

Je veux faire des rituels dont le sens est à venir, pour la prochaine génération peut-être, pour une lune aux nuages pétrifiés de rêves morts : il y a des corps écarlates qui en descendent en lumière et habitent la nuit. J'ai peur. Non, ce n'est pas vrai. Je n'arrive plus à avoir peur.

```{=tex}
\vspace{1\baselineskip}
```
J'apprends de nouveaux langages, qui sont anciens, qui sont passés et sont futurs. Éternels présents. Je dis : tes étoiles-plantes aux révoltes animales, tes traversées de lumière. Je dis : vous aussi, consumées, faites des mouvements, des gestes auxquels vous vous abandonnez.

Je dis : transforme ces étoiles en vie plutôt.

## Cosmogrammatologie

```{=tex}
\begin{nscenter}
```
*Ethnographie de la nuit --- notre peuple de nuit, notre imaginaire de nuit*

*Toutes choses à pluraliser*

```{=tex}
\end{nscenter}
\vspace{1\baselineskip}
```
Comment la nuit s'inscrit-elle en nous ? Quels cosmogrammes s'inscrivent dans nos existences traversées et traversant la nuit faite espace, faite temps, faite outre-espace et outre-temps --- faite de toute l'altération du jour ?

```{=tex}
\vspace{1\baselineskip}
```
Nous, on l'a dit

On a inventé des outils pour capter la nuit

Des fêtes pour la conjurer

Un monde sans sommeil

Et dans le ciel des vides se sont rejoints pour faire l'oubli de la nuit

Nuit parcellaire, nuit de liberté inégale entre ceux qui la subissent et ceux qui la vivent comme émancipation des contraintes du jour

```{=tex}
\vspace{1\baselineskip}
```
Nuit globulaire étendue à presque tout le globe dans une nuit mondialisée

```{=tex}
\vspace{1\baselineskip}
```
Pourtant ni la Terre ni la nuit ne sont une sphère

```{=tex}
\vspace{1\baselineskip}
```
Alors, désécrire cette nuit, et la former de nos corps

Nuit membranaire

Nuit se déplaçant avec nous, dans une même chair

Comme la Terre se déplaçant avec l'univers, déformée par la gravité, les météorites, toute bosselée sous son manteau aquatique, nuit jamais ténèbre, pleine de fluctuations, de nuances, de vortex, d'étoiles, d'éclipses et de zones vides

Nuit-Terre difforme

Telle est la réalité scientifique que notre imaginaire n'arrive pas à transformer de manière vitale, animale, imaginaire --- tout-un, toute-nuit

```{=tex}
\vspace{1\baselineskip}
```
On n'a plus que des différentiels pour appeler ça d'un bloc : ça nuit, ça jour

--- partages de la mesure

```{=tex}
\vspace{1\baselineskip}
```
Mais ce que la nuit a partagé fut la démesure

```{=tex}
\vspace{1\baselineskip}
```
Il faudrait alors retourner à l'intérieur de la Terre, à l'intérieur de la nuit ?

Mais l'intérieur et l'extérieur sont ce qui a disparu

```{=tex}
\vspace{1\baselineskip}
```
Alors --- c'est la tâche sérieuse, orgueilleuse, risible, essentielle, impossible : parler d'autres langues de la nuit. Essayer de restituer les fluctuations de la nuit, sa lumière, sa présence, son insistance dans nos existences

```{=tex}
\vspace{1\baselineskip}
```
Reformer la nuit depuis les sciences et les animaux en nous

Car d'autres animaux ressentent la nuit, le vent, la terre

Et dans les profondeurs marines, d'autres nuits encore

Il faut avoir les animaux à l'esprit

Sans cesse

Voir ainsi se métamorphoser les néons de la nuit se peuplant d'insectes et peut-être un jour, avec eux, changer de différence, de nuit, d'intensité

```{=tex}
\vspace{1\baselineskip}
```
Dans les nuits trop lourdes de la fatigue, dans le filet des jours, le cours de la vie est toujours à la baisse. Tendanciellement. Tangente avec l'horizon jaune, est-ce l'aube déjà ? le crépuscule ? Sache-le, oublie-le

```{=tex}
\vspace{1\baselineskip}
```
On danse, on danse encore aujourd'hui la nuit : là seulement on se rappelle le corps et la nuit, on est un être d'un temps et d'un espace autres, d'une expérience et d'un savoir qui appartiennent à d'autres, médiatisé par d'autres

```{=tex}
\vspace{1\baselineskip}
```
Être de la traversée, peut-être est-ce ce que j'appelle la nuit

```{=tex}
\clearpage
\Huge
```
Ici

insérer d'autres nuits

```{=tex}
\clearpage
\normalsize
```
### Signes de nuits

**Cosmogramme**

Nuit d'*homo sapiens* : la nuit nous descendons des arbres. Puis nous faisons la nuit magique. Avec le feu nous apprenons la nuit. Nous apprenons à enterrer nos morts, à enterrer nos déchets, nous apprenons à créer dans la nuit seconde de la caverne --- comme si la nuit était essentielle à la création (la Bête innommable et nous-mêmes, dans cette nuit mortelle réunie et approchée par l'art).

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Peuples nocturnes, les Celtes faisaient commencer la journée avec le début de la nuit et les Gaulois eux aussi comptaient le temps en termes de nuits plutôt que de jours. Calendrier des nuits. Vivre selon la Lune (calendrier hégirien). Calendriers de nos destructions. Vivre selon le désastre.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Nuit plus primitive que le jour. Du Chaos naît la Nuit, de la Nuit (Nyx) naît le Jour (Hémera) dans la mythologie grecque rapportée par le poète Hésiode. La nuit serait l'authentique, le jour comme mensonge. Inversion des valeurs. Qu'est-ce que la nuit ? La vérité du jour. De Nyx à la nuit astronomique, quelque chose de commun.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Les religions du Livre : horreur de la Nuit, de ses créatures. On referme les villes et les foyers. On craint les rêves qui étaient vénérés dans l'antiquité. Les moines chrétiens veillent la nuit pour protéger de leur prière la communauté des croyants.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Travail de nuit (primeurs apportées en pleine nuit). Nuit des prolétaires. Nuit de galère. Noir de la cale. Cosmopolitique de la nuit. Elle aussi.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

L'œuf contenant toute la nuit du Dieu-fleuve Cobra Grande. Une noix poilue, douce et mystérieuse, pleine de bruit, que les hommes ouvrent et par leur transgression libérant ainsi la nuit et ses animaux.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Nuits galantes. *Mille Nuits et Une nuit*. Pour échapper à la mort. À l'amour ravageur. Pour s'y livrer. L'amour, la mort, la nuit. Nuit sexuelle, matrice des scènes primitives.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

La nuit, le crime, la mort : criminalisation, obscurcissement de la nuit au XVIII^e^ et XX^e^ siècle en Occident. Les "nuitards", révolutionnaires, ouvriers (*La Nuit des prolétaires*) sont suspectés.

Propagande romantique, fantasme de la nuit transgressée.

Faire transmigrer les nuits dans d'autres imaginaires.

Il n'y a pas plus de crimes la nuit, mais l'accroissement des lumières artificielles les a favorisés.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Nuits bleues des attentats. Nuits blanches des bombes --- étoiles terribles, obus de la Première Guerre mondiale, missiles de la guerre du Golfe vus à travers l'infrarouge : la nuit ne sera jamais plus la nuit.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Nuit Dagon. Lovecraft a transformé l'horreur cosmique de Pascal en l'horreur noire de la peur de l'Autre. Pourtant survit dans ces nuits horribles quelque chose de la transformation du ciel privé de tout --- déprivation / privatisation du ciel nocturne.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

La nuit des pêcheurs. Fatigues et lumières. Dans les grandes profondeurs de la fatigue, ils apprennent à voir. Pourtant si peu d'histoire. Des orages, des naufrages : autres nuits.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Les rituels de nuit pour conjurer la nuit. Partout. Chez les Yucuna d'Amazonie. Chez les Inuits de la terre de Baffin. Chez les Otomi du Mexique. On chamanise. On discute avec les esprits de la nuit. On conjure la nuit comme on conjure les fantômes. Ambiguïté merveilleuse : *conjurer* peut vouloir dire repousser et en même temps appeler. C'est la puissance paradoxale de ces techniques du sacré et du rapport existentiel à la nuit.

Il faudrait nous aussi apprendre à *jurer avec la nuit*.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

Démons et merveilles. Accueil de l'invisible. Se faire thermosensible. Chauve-souris.

```{=tex}
\vspace{1\baselineskip}
```
**Cosmogramme**

"What is the night?" (Macbeth). Vous avez quatre heures.

```{=tex}
\begin{nscenter}
\clearpage
```
### Constellation de la Contre-nuit. Répétition.

```{=tex}
\end{nscenter}
```
```{=tex}
\vspace{3\baselineskip}
```
Changer de récit.

Rêver d'apercevoir à notre tour une constellation négative.

Constellation qui arrivera par d'autres histoires.

Tentative de voir autrement.

Pas cette histoire.

Pas la nôtre.

Un récit du temps, du rêve, des animaux --- non des humains seulement.

```{=tex}
\vspace{1\baselineskip}
```
D'autres ciels --- parce que ce lointain, ce désir, cet absolu, cette esthétique sont comme la polarisation de notre être, langage sans langage de la nuit.

D'autres ciels à composer.

```{=tex}
\vspace{1\baselineskip}
```
Composer pour notre époque de *désastre* une *constellation négative* autrement que négative.

Composer autre chose qu'une constellation dans une époque où les étoiles ont été oubliées.

Composer autre chose qu'une mythologie dans une époque où les grands récits n'existent plus, où les récits minoritaires s'éloignent les uns des autres comme les galaxies, comme les étoiles distantes composant du point de vue de la Terre l'image fallacieuse d'un ciel à plat.

Restituer la profondeur, la distance, l'attraction, la conjonction astrale du récit, de la science, de la politique et de l'imaginaire.

```{=tex}
\vspace{1\baselineskip}
```
Pour cela, créer, résister, recréer, faire converger. Faire la contre-nuit, réinventer la nuit depuis ses constellations négatives.

```{=tex}
\vspace{1\baselineskip}
```
Défaire l'histoire de notre rapport à la nuit liant l'obscurité au danger, le danger au mal, le mal à la négativité absolue, dont la formule terminale dans notre discours a pour nom : nihilisme.

Nom de nom.

Rien de rien.

Un ciel en forme de néant nous laissant en état de *désidération*. Désastre et désir combinés. Malades historiques. Disséminés plus qu'aucune étoile ne l'a été.

Changeons.

Bifurquons.

Mieux, *étoilons* vraiment, dirait Levania.

```{=tex}
\vspace{1\baselineskip}
```
Il n'y a pas à chercher loin pour trouver des alternatives à cette absence de nuit qu'on appelle "nuit", à cette histoire de la nuit, à ce marasme du désir et de l'utopie, à cette désincarnation moderne de l'espace et du temps.

Car il y a des *Idées pour retarder la fin du monde* nous dit Ailton Krenak.

Des idées pour sauver la nuit.

"Comment les blancs vont-ils faire pour s'en sortir ?" se demande-t-il, quand notre brutalité, notre système, notre croyance sont bien moins résistants que la "pensée sauvage" de la société amazonienne "primitive" ?

```{=tex}
\vspace{1\baselineskip}
```
Une pensée donc du retard et du délai.

Une pensée sans sursis cependant.

Une demande de changement de coordonnées mentales, de coordonnées de nos constellations mentales, de notre cosmologie, de notre cosmotechnologie. Changement d'espace-temps dans notre "temps de la fin" ayant remplacé toute fin des temps selon Anders. Sans salut. Dans le temps et l'attente. Si peu seulement. Alors. Alors quoi ? Peut-être moins agir qu'*inter-agir* dans ce jeu, cet espace, ce délai sans délai, cette urgence, ce défaut. Une pensée du retard et du détour, de la relation aussi. De la pensée par le dehors. Par la relation au-dehors. Non pas la nature. Non pas la technique. En nous-mêmes des parts de nuit.

```{=tex}
\vspace{1\baselineskip}
```
Penser par le détour et le dehors. Par un détour qui pourrait prendre la forme des autres modes de relations au monde --- médiation sans médiation cependant : car on sait sur quelles ruines on se tient, dans quel assujettissement nos pensées sont produites. Mais un biais. Une transformation. Une pensée transformative venant de la reconnaissance d'une altérité. D'un dehors. D'un visage. Que le monde ait des visages. Celui d'autres animaux. Des plantes. Des étoiles. Des bactéries. Des pierres et de la mer. De la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Curiosité et soin (*cura*).

```{=tex}
\vspace{1\baselineskip}
```
Ce n'est pourtant pas que l'on puisse redevenir animiste, mais que l'on doive faire muter le *naturalisme*. L'hybrider si possible. Le réinventer toujours. Atomystique d'un nouveau genre. De genres toujours nouveaux et pluriels. Désanthropologie car ne se contentant pas de l'anthropologie des sociétés autres mais anthropologie déconstruisant notre propre nuit occidentale. Et puis désanthropologie car se proposant une perspective non plus centrée sur l'humain mais sur la nuit.

```{=tex}
\vspace{1\baselineskip}
```
On doit essayer de déplacer l'ensemble des étoiles, l'ensemble des mythologies déniées de notre pensée --- n'est-il pas question que de ça, ici ? Redire et répéter ces choses, je ne m'en lasserai pas, jusqu'à ce que le monde change. J'en ferai des prophéties litaniques, des essais, des narrations, des performances jusqu'à ce que ça devienne *forme sensible de vie*.

```{=tex}
\vspace{1\baselineskip}
```
Alors, penser la nuit par exemple par le détour des Aborigènes d'Australie.

```{=tex}
\vspace{1\baselineskip}
```
Quand on dit cela, bien sûr, il faut faire attention à tout ce qui se mêle et se transforme. Notre nuit, leur nuit, faisceaux synchrones : la nuit elle aussi s'est mondialisée. Les heures ne se comptent plus en cycles et en animaux (heure du Rat, heure du Bœuf, heure du Tigre... vêpres, complies, vigiles, laudes...). Nulle nostalgie cependant. Jamais.

```{=tex}
\vspace{1\baselineskip}
```
Non, ce qu'il faut formuler c'est toujours une transformation (ça te semblera familier, tu l'as déjà lu).

Alors la nuit aborigène.

Non pas dans le Temps du Rêve. Je laisse ça à d'autres.

C'est toute une philosophie de l'obscurité (*blackness, darkness* et autres points aveugles de l'Occident[^1-contre-nuit_42]) qu'il faut conjuguer au présent le plus contemporain, à l'histoire la plus terriblement emmêlée et traumatique, sans Tradition et pourtant toujours renaissante.

```{=tex}
\vspace{1\baselineskip}
```
C'est ce que j'essaie de te faire saisir par la répétition et le détour et tout ce qui dit autour, à travers cette remise en question générale de la nuit. Ce qu'il faut ainsi inquiéter c'est cette façon de se promettre sans cesse à un but : idéalisme du rendement de la nuit, des expériences, de la conquête, du projet, passant tout par pertes et profits, ce qui se dit en fiction : aventure, narration, odyssée.

Alors bien plutôt, il s'agit de se mettre à l'écoute de ce qui déjà était nuit, notre épisode de nuit en plein jour dans notre tradition, cette éclipse silencieuse et fascinante du chant des sirènes selon Kafka :

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{centre}
```
"*Et de fait, quand Ulysse arriva, les puissantes Sirènes cessèrent de chanter, soit qu'elles crussent que le silence seul pouvait venir à bout d'un pareil adversaire, soit que la vue de la félicité peinte sur le visage d'Ulysse leur fît oublier tout leur chant. Mais Ulysse, si l'on peut s'exprimer ainsi, n'entendit pas leur silence ; il crut qu'elles chantaient et que lui seul était préservé de les entendre ; il vit d'abord distraitement la courbe de leur cou, leur souffle profond, leurs yeux pleins de larmes, leur bouche entrouverte, mais il crut que tout cela faisait partie des airs qui se perdaient autour de lui. Mais bientôt tout glissa devant son regard perdu au loin ; les Sirènes disparurent littéralement devant sa fermeté, et c'est précisément lorsqu'il fut le plus près d'elles qu'il ignora leur existence.*"

```{=tex}
\end{centre}
```
Des sirènes passent dans la nuit. Le gyrophare éclaire les nuits puis disparaît. Le son s'estompe. La vie reprend, la nuit s'oublie.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[3em]{0em}
```
Pourtant, c'est autre chose que l'imaginaire qu'il s'agit de faire changer de signe, de sens. Certes. C'est une révolution cosmologique qui n'appartient à aucun texte, aucune réalisation de l'art. Qui est en chacun dans les limites de sa capacité à faire face au désastre et à mobiliser cette plasticité destructrice dont nous parle Catherine Malabou --- à partir de la perte, de l'irrémédiable perte.

```{=tex}
\vspace{1\baselineskip}
```
Mais pas seulement. Pas seulement. Et ce pas, seulement ce *pas* est significatif, c'est Blanchot qui nous l'a appris. Ce "pas au-delà", qui est à la fois interdiction et franchissement. Ce n'est pas suffisant et pourtant décisif. Ça me rappelle cette fable qu'on lit en quatrième de couverture de son volume *De Kafka à Kafka* :

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{2em}
```
*« Comme si Kafka portait en lui ce bref dialogue :*

```{=tex}
\hspace*{2em}
```
*--- De toute manière tu es perdu.

```{=tex}
\hspace*{2em}
```
*--- Je dois donc cesser ?*

```{=tex}
\hspace*{2em}
```
*--- Non, si tu cesses tu es perdu. »*

```{=tex}
\vspace{1\baselineskip}
```
C'est de cela qu'il est toujours question. De ne pas abandonner, ni les luttes au niveau de l'imaginaire, ni les causes politiques, ni les solidarités actives. Cela semblera très ordinaire. La zizanie l'est elle aussi.

```{=tex}
\vspace{1\baselineskip}
```
J'arrive ici au terme de ma dérive heureuse et hasardeuse.

J'aurais voulu défaire les *catastérismes* formant les constellations d'étoiles que presque toutes les civilisations ont constituées imaginairement dans le ciel comme un Dehors pour les guider, les inspirer. Car je préférerais hériter de la leçon (hériter, c'est-à-dire l'impossible, c'est-à-dire abandonner et transformer, reprendre et réactualiser), leçon des Aborigènes qui imaginèrent une constellation négative singulière dans l'amas des Pléiades.

À la nuée des étoiles brillantes définissant les contours elliptiques de figures mythiques, ils ont ajouté le contour plus sombre formé par les nébuleuses bloquant la lumière par leur gaz, dessinant des masses plus sombres dans la nuit, des absences de lumière formant des *constellations négatives*, constellations neutres dans le ciel.

Que les Occidentaux ignorent ces formes ultranoires est un effet bien sûr de notre ciel pollué de tout, mais plus profondément de notre rapport aux étoiles et à la nuit comme rapport à la lumière, au savoir, au lointain.

Ce qui se promet dans ces constellations opaques c'est un rapport à l'informe, au non-savoir, au non-maîtrisable, au dehors.

Voir la nuit dans la nuit, la contre-nuit. *Eurêka* est aussi merveilleux que celui d'Edgar Allan Poe découvrant pourquoi la nuit est noire et non pas lumineuse[^1-contre-nuit_43].

```{=tex}
\vspace{1\baselineskip}
```
Il est des nuits dans la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Parler de cet univers, vraiment, de ces constellations négatives qui ne sont pas des opacités à dissiper mais une nuit à observer dans son obscurité la plus profonde, c'est ce qu'il faudrait faire pour changer, depuis notre ontologie romantique, naturaliste, changer le sens de la nuit et faire apparaître le mouvement neutre et paradoxal de la contre-nuit.

```{=tex}
\end{addmargin}
```
### Animaux de la nuit

Les animaux sont la nuit.

```{=tex}
\vspace{1\baselineskip}
```
C'est l'histoire.

Il y en a d'autres. Celle-ci est ma vérité, née d'une noix velue venue d'ailleurs.

```{=tex}
\vspace{1\baselineskip}
```
Les animaux sont la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Nous avons perdu la nuit.

Nous avons perdu les animaux.

Nous aussi.

```{=tex}
\vspace{1\baselineskip}
```
Tous sont la nuit, l'écoute obscure, l'être en sa dissimulation.

Tous les yeux sont la nuit, tous sont les animaux.

```{=tex}
\vspace{1\baselineskip}
```
Sur la rivière passent les yeux jaunes et brillants de la panthère.

```{=tex}
\vspace{1\baselineskip}
```
C'est la nuit, écoute.

Tous les animaux qui sont la nuit, qui font la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Les insectes disparus.

Les mammifères en voie d'extinction.

Les oiseaux en flammes dans le ciel en flammes.

```{=tex}
\vspace{1\baselineskip}
```
Les animaux sont la nuit.

Avec la nuit nous avons perdu les animaux.

```{=tex}
\vspace{1\baselineskip}
```
Et comment être humain sans la nuit et les autres animaux ?

```{=tex}
\vspace{1\baselineskip}
```
Ne plus faire des listes pour nos désanthropologies comptant toutes les manières d'être avec la nuit que l'on perd avec eux.

```{=tex}
\vspace{1\baselineskip}
```
Des listes et des listes pour quoi faire ?

Une récitation dans une maison de la poésie ?

Qui a encore le cœur à ça ?

```{=tex}
\vspace{1\baselineskip}
```
Il faut dire la vérité.

Les animaux sont la nuit, ma nuit.

Autant que des étoiles inconnues.

```{=tex}
\vspace{1\baselineskip}
```
Je ne sais ni les étoiles ni les animaux,

Ni les plantes ni les cris,

Ni les chants ni les contre-chants.

```{=tex}
\vspace{1\baselineskip}
```
Je voudrais faire des listes de nuits,

De nos nuits animales, électrocutées aux barrières des villes.

```{=tex}
\vspace{1\baselineskip}
```
Je voudrais des nuits guanaco.

Et vivre dans l'estomac noir d'une Grande Ourse.

```{=tex}
\vspace{1\baselineskip}
```
Parmi la nuit.

```{=tex}
\vspace{1\baselineskip}
```
Je ne sais pas.

Les animaux sont la nuit et nous ne nous connaissons pas.

```{=tex}
\vspace{1\baselineskip}
```
Alors je vais commencer, au hasard, même si je sais que l'on mettra la nuit en feu et la poésie dans des livres, les mots dans des espaces où ils ne brilleront pas comme des étoiles dans un champ, parce qu'aucun enfant n'y veille.

```{=tex}
\vspace{1\baselineskip}
```
Alors je te donne mon enfance de nuit.

Mon animal de nuit.

Je te donne mon animal nocturne et tu me diras le tien.

Et ainsi, peut-être.

Je ne sais pas.

Peut-être que quelque chose survivra --- maigres animots faméliques de l'imaginaire qu'il faut pourtant protéger et faire vivre encore de notre souffle, de nos espoirs

Mon animal de nuit a été le Aye-Aye.

De la race des lémures.

Des lémures qui chez les Latins désignent les ombres mauvaises des morts.

Et qui désignent ici un animal presque disparu.

Un rongeur dont le nom malgache signifie "je ne sais pas".

Qui nous dit, à nous aussi : le sais-tu ?

Je fixe les yeux jaunes, énormes et globuleux de l'aye-aye.

Je fixe ta solitude de chauve-souris qui aura peut-être bientôt disparu.

```{=tex}
\vspace{1\baselineskip}
```
Tu frappes à la porte des arbres.

Tu frappes à la porte des fruits.

Tu frappes à la porte de la nuit.

Qui te répondra ?

Nous qui devons en répondre.

Répondre de la nuit, répondre des animaux, répondre du *je ne sais pas* de toute connaissance.

```{=tex}
\vspace{1\baselineskip}
```
Dans mes nuits j'entends encore l'aye-aye.

Animaux de nuit --- nous et moi.

Animaux attentifs aux prophéties de la pluie.

À la fortune des larves et des fantômes en train de grossir dans les arbres.

Tu ne vois pas le ciel.

À terre, un humain t'a récupéré et t'a coupé ton long doigt fascinant qui ne pointera plus ni la nuit ni ses fruits que sont les étoiles.

```{=tex}
\clearpage
```
```{=tex}
\Huge
```
Nous sommes des formes de peste dans le ciel de nuit

```{=tex}
\normalsize
```
## Circa noctem

Plaines de fumées, nuit consumée dans des incendies immenses faisant notre jour sans nuit, sans sommeil, sans vie

Nous volons au-dessus depuis des jours-maintenant

Depuis des nuits-jamais

Et la fatigue dans nos ailes, et le vertige des fumées

Nous

Menacent

```{=tex}
\vspace{1\baselineskip}
```
L'océan de nous-mêmes en flammes

Nous ne pensons plus des choses aussi génériques que l'apocalypse

Aussi creuses que des images

Des mots

Ça aussi s'est consumé

```{=tex}
\vspace{1\baselineskip}
```
Étoiles de l'œil

De pensée devenue

Nous ne comprenons plus les directions magnétiques

Les étoiles sont devenues des cris

Ce sont peut-être les nôtres

```{=tex}
\clearpage
```
```{=tex}
\begin{addmargin}[3em]{0em}
```
Pensée sauvage

Pensée sauvage du futur

Pensée sauvage du futur où la nuit nous revient

Pensée sauvage du futur où la nuit nous revient non pas d'un passé archaïque mais

Pensée sauvage du futur où la nuit nous revient d'un passé pluriel

d'un avenir dangereux et incertain

Oui, pensée sauvage

Pensée sauvage du futur où l'expérience nous revient par un étrange détour,

par un langage incompréhensible

Pensée sauvage du futur pour ce silence à décrypter depuis une patience à inventer

Pensée sauvage du futur où perdre nos repères

Nos voix, nos cœurs, nos passés, nos convictions,

Pensée sauvage du futur à entendre depuis des distances où les vivants et les morts se parlent

```{=tex}
\vspace{1\baselineskip}
```
Pensée sauvage pour cette connaissance défaite et refaite.

```{=tex}
\vspace{1\baselineskip}
```
Pensée sauvage pour cet animal inquiet dans la nuit

Pensée sauvage du futur

Pensée totem du silence

```{=tex}
\end{addmargin}
\bfseries
\vspace{1\baselineskip}
```
Toute ma vie durant, j'ai été de la nuit.

Ma famille, mes amis, signes distants, étoiles, planètes, tous étaient de son signe seulement

Ses mouvements les miens

Ses hésitations

Ses tremblements

Tous miens

Tous mes proches sont du signe distant

De la nuit

```{=tex}
\vspace{1\baselineskip}
```
Vicieusement lors mon initiation, on m'a donné pour survivre dans notre univers toxique un masque de lépidoptère, croyant me faire plaisir.

Mais, je vous l'apprends, les papillons de nuit n'existent pas.

Tous sont des créatures du jour et de la nuit en même temps.

Pourtant peut-être faut-il changer de point de vue. Considérer que la nuit continue toujours, malgré l'éblouissement que nous cause notre étoile.

Discrètement tout est toujours la nuit. Et les papillons alors, vraiment tous des papillons de nuit.

J'ai pourtant gardé ce masque que j'ai maudit quand je l'ai vu, avec sa trompe ridicule, ses yeux à facettes, ses couleurs de cendre et de terre.

Puis j'ai appris à l'aimer. J'ai appris à tympaniser les organes de la nuit. J'ai appris la nuit avec le jour et le jour avec la nuit.

```{=tex}
\normalfont
\clearpage
```
**2 129 poèmes par nuit**

```{=tex}
\rule[0mm]{1\textwidth}{1mm}
```
2 129 poèmes par nuit

chaque seconde un poème perlait de son front

le vent séchant en poèmes, en métamorphoses, en pierres et en images

les arbres fanaient dans le ciel

les chauves-souris cristallisaient de mauvaise grâce

des mondes s'ouvraient et se refermaient sans un regard

sans possibilité d'un regard

même traversée

2 129 fois répétée de nuit

de la nuit à la nuit

sans cesse, sans reste,

conduisant et non conduisant

prenant la route des hasards et des minuits

sillonnant l'univers

```{=tex}
\rule[0mm]{1\textwidth}{1mm}
```
```{=tex}
\hspace*{\fill}\rule[0mm]{0.62\textwidth}{1mm}
```
```{=tex}
\clearpage
```
```{=tex}
\begin{addmargin}[6em]{0em}
```
Pourtant, tu le sais, il faudra aussi parler de la nuit des grottes préhistoriques, des maraudes, des nuits lointaines des hypothèses.

Il faudra lire.

Les mots non inscrits.

Les mais.

Les ailleurs.

C'est ce que l'on veut lire. Les profondeurs de la nuit depuis la nuit des temps.

```{=tex}
\end{addmargin}
```
Depuis que les hommes et les femmes ont fait de la nuit quelque chose d'autre qu'un sommeil vigile : une fiction.

```{=tex}
\hspace*{3em}
```
C'est de là que l'on vient.

```{=tex}
\hspace*{3em}
```
De la fiction.

```{=tex}
\hspace*{3em}
```
De la nuit.

```{=tex}
\begin{addmargin}[6em]{0em}
```
Alors il nous faudra parler de la langue obscure et des langues de feu.

De ce que l'on ne connaît pas.

Des pierres et des os.

De ce qu'elles ont dit, de ce qu'elles ont tu.

```{=tex}
\end{addmargin}
```
Trouver la nuit

```{=tex}
\begin{addmargin}[3em]{0em}
```
Déterrer quelque part ses cœurs palpitants et les remettre au ciel.

```{=tex}
\end{addmargin}
```
Alors encore il nous faudra imaginer ---

```{=tex}
\begin{addmargin}[6em]{0em}
```
Des langues oubliées, des langues perdues --- des histoires qui ne nous appartiennent pas.

Des paroles, des peuples, des langues, des fictions.

Avec lesquels il nous faut trouver.

Une façon de nous transformer.

Il nous faudra parler la langue des morts, cette langue que l'on a oubliée, oubliée avec toute la nuit et toute la contre-nuit.

Notre rapport à la terre, au ciel, à la mort --- la terre, le ciel, la mort, unis dans la nuit.

```{=tex}
\end{addmargin}
```
```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[3em]{0em}
```
Ne pas trahir notre patience.

Ce dont je voudrais te parler ce soir, ce sont des virtualités de nos nuits éteintes, de nos nuits de cendres lumineuses.

Je veux dire de nos relations en plastique avec la nuit plastique.

Je veux dire que ta relation à la nuit c'est celle à un sac plastique jetable et avec lequel certains s'asphyxient pour s'amuser. Hypoxie de la nuit.

Voilà toute notre extase.

Évidemment, je pense à ces nuits-là. À l'hypernuit que le capitalisme mondialisé a instaurée.

Mais pas seulement.

En fait je ne veux pas lui donner tant d'importance.

Malgré tout.

Quelques lumières rouges brillent en haut des immeubles et des grues de construction.

Un vigile écrit un poème sur son portable.

Un homme se penche sur le visage de son enfant endormi.

Des rires et des caresses s'échangent.

C'est vrai.

Mais je veux te parler de ces autres nuits.

Des contres-nuits.

Des contes de la nuit que l'on a oubliés et qui ont fait monde, qui ont fait le monde jusqu'à la fragmentation universelle actuelle.

```{=tex}
\vspace{1\baselineskip}
```
Bien sûr tu ne peux pas oublier qu'on vient de cette nuit mondialisée.

Bien sûr, tu ne peux pas oublier que le point de départ de la nuit a explosé dans le contemporain.

Que le contemporain est la fragmentation.

La bombe invisible.

Nuit sans nuit étendue sur tout le cosmos.

```{=tex}
\end{addmargin}
```
```{=tex}
\vspace{1\baselineskip}
```
Mais la fiction est aussi une forme de résistance.

Tu le sais depuis longtemps.

Force des liens faibles.

En mille langues répandues.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[3em]{0em}
```
Tes histoires passées, futures, réelles, inventées de la nuit portent toutes la marque noire de ce présent où la nuit a disparu.

Bien sûr.

Je n'invente rien en disant ça.

J'aimerais bien, crois-moi. Vraiment.

Mais elle s'est consumée avec le reste de l'expérience.

```{=tex}
\end{addmargin}
```
```{=tex}
\vspace{1\baselineskip}
```
La nuit des astronomes, la nuit des boîtes de nuit, la nuit des urgences surchargées, la nuit des moustiques tigres, la nuit des soirées trop alcoolisées, toutes les nuits que tu veux n'ont plus rien en commun que l'on pourrait nommer d'un seul mot.

```{=tex}
\vspace{1\baselineskip}
\hspace*{3em}
```
***La nuit*** est devenue un leurre.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[3em]{0em}
```
Moi ça me rend fou qu'on ne s'étouffe pas du manque d'absolu.

Qu'on ne soit pas gêné de se fabriquer en Afrique des poumons en plastique. Qu'on continue à utiliser des mots morts : nuit, poésie, philosophie --- réalités de plastique.

```{=tex}
\vspace{1\baselineskip}
```
*Tu exagères*, tu me redis ça de ta petite voix que j'aime tant.

```{=tex}
\end{addmargin}
```
```{=tex}
\vspace{1\baselineskip}
Vrai
```
```{=tex}
\hspace*{3em}
Vrai
```
```{=tex}
\hspace*{6em}
Vrai
```
```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[3em]{0em}
Mais c'est tellement ce dont je veux te parler, de toutes ces nuits, de tous ces récits de la nuit, et de toutes les contre-nuits, la défaite de l'image comme notre ultime façon de faire la nuit.

Et danser oui, oui, oui, si tu veux.

\vspace{1\baselineskip}

Après je pourrais dire le reste dans les limbes de soleils électriques sous perfusions bleu acide, jusqu'à épuiser le temps, l'espace --- on pourrait faire une belle chanson pour ça.

\vspace{1\baselineskip}

Ça ne dirait rien qu'on ne sache pas.

Ce n'est pas l'expérience.

Ce n'est pas le temps. La fiction.

\vspace{1\baselineskip}

La nuit n'est rien qu'un passage feutré d'un univers à l'autre.

Pas un monde.

Pas une fiction.

Pas une fraction.

Pas autre chose qu'une image.

Qu'un mot qui te fait haïr tous les mots.

\vspace{1\baselineskip}

Mais la réalité est cette pauvre chose.
\end{addmargin}
```
# Quatrième Cercle / Quatrième Ciel / Contre-chants de la contre-nuit

```{=tex}
\def\addquotemb
{%
  \begin{tabular}[b]{p{3.5cm}}
    \begin{nscenter}
    « Où est la nuit ?\\
Il n’y a plus de nuit. »\\
Maurice Blanchot
    \end{nscenter}
  \end{tabular}%
}
```
```{=tex}
\begin{pullquote}{object=\addquotemb}
\itshape
```
« La première nuit est accueillante. Novalis lui adresse des hymnes. On peut dire d'elle : *dans* la nuit, comme si elle avait une intimité. On entre dans la nuit et l'on s'y repose par le sommeil et par la mort. Mais *l'autre* nuit n'accueille pas, ne s'ouvre pas. En elle, on est toujours dehors. Elle ne se ferme pas non plus, elle n'est pas le grand Château, proche, mais inapprochable, où l'on ne peut pénétrer parce que l'issue en serait gardée. La nuit est inaccessible, parce qu'avoir accès à elle, c'est accéder au dehors, c'est rester hors d'elle et c'est perdre à jamais la possibilité de sortir d'elle.

Cette nuit n'est jamais la pure nuit. Elle est essentiellement impure. Elle n'est pas ce beau diamant du vide que Mallarmé contemple, par-delà le ciel, comme le ciel poétique. Elle n'est pas la vraie nuit, elle est nuit sans vérité, qui cependant ne ment pas, qui n'est pas fausse, qui n'est pas la confusion où le sens s'égare, qui ne trompe pas, mais dont on ne peut se désabuser. Dans la nuit on trouve la mort, on atteint l'oubli. Mais cette autre nuit est la mort qu'on ne trouve pas, est l'oubli qui s'oublie, qui est, au sein de l'oubli, le souvenir sans repos. »

```{=tex}
\end{pullquote}
\clearpage
\bfseries
```
Ici mon histoire de la nuit tant annoncée

Épopée noire, sans sujet, du point de vue seul de la nuit

Tombeau ouvert pour 500 000 nuits

pour la lune et la soif

pour l'espace dispersé

*etc., etc., etc.*[^1-contre-nuit_44]

essai, essai, essai

départs pour d'autres images, arrêts et extensions,

tombeaux ouverts sur 500 000 vies, devenues racines invisibles du temps

marre, marre, marre

de toutes ces énumérations cérémonielles

et que le vide, la parole et la mort palabrent

cela m'agace 500 000 fois

Que 500 000 fusées explosent et que la nuit disparaisse plutôt que ces signaux menteurs

ces confusions érigées en mystères, ces mystères érigés en vérité

Plutôt que la vie et ses contradictions maintenues ---

Signer la nuit du moins que l'on peut

Voilà vraiment la nuit

--- Nuit de tout ce qui n'est plus rien, ni fiction, ni essai, et qui devrait être à soi-même sa propre poétique --- rien de moins --- inventer tout le moins, toute la grande faiblesse que serait une parole, un rythme, une dépense vertigineuse de la pensée qui rende cette façon de sentir, d'éprouver, de faire exister la nuit et la contre-nuit d'un même mouvement.

```{=tex}
\vspace{1\baselineskip}
```
Cela nous le reconnaissons dans les méandres brillants de nos désirs. Il faudrait faire forme. Un chant. Non, pas un chant, mais des contre-chants, lignes de chants entremêlées pour cette contre-nuit réelle d'onirisme réel. Sentiments plus lointains que les distances entre les étoiles.

Peut-être ne pourrons-nous qu'aborder les parages, que circonscrire des cercles autour de cette contre-nuit, esquissant des signes pour les fantômes à venir. Ils sauront toute ma sincérité et vous-mêmes aussi. C'est tout ce que l'on peut faire. Arrêter même de vouloir le dire. Vivre à de tels points de compression. À de tels points que toute l'humanité deviendrait peut-être nuit et pétrole. Et se rendrait à d'autres flammes. À d'autres lumières, à d'autres points obscurs où le sens se donne aussi.

```{=tex}
\vspace{1\baselineskip}
```
La nuit est la fiction ininterrompue de l'humanité. Encore, et toujours. De tous les jours. De tout ce qui nous renverse. De tout ce qui nous bouleverse. D'autre chose que l'humain. D'autres royaumes. Des esprits qui lui retirent le monde et lui en restituent la multiplicité en étoiles. Un jour de pluie elles tomberont, peut-être. Non, précisément non, plus de telles prophéties, mais plutôt des trajectoires vivantes et mourantes. Regarde, ce sont des constellations organiques qui me font respirer avec la patience énorme de tout ce à quoi je me promets alors, ici et avec ça. Malgré l'inessentiel et l'impossible. Je le sais, à tout cela il faut se donner sans limites, à tout cela donner un récit et un contre-récit, une parole et sa défaite, une épopée millénaire et l'éclair de l'instant, une fiction et son éparpillement --- nuit, contre-nuit.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{nscenter}
\textbackslash ko.mã.s\textsf{ɔ̃}\textbackslash
\end{nscenter}
```
```{=tex}
\normalfont
\clearpage
\asterisme
```
```{=tex}
\begin{nscenter}
\uline{\itshape Conjuration de la nuit}
\end{nscenter}
```
```{=tex}
\vspace{1\baselineskip}
```
Le sens, la passion primitive et encore, et contre la nuit, le répéter avec les nuits, le ciel, le son, l'image et le fond de la gorge animale, avec cette passion archaïque, ah, je viens de là, de ce ciel en étoiles pleines de gouffres, jusqu'à ce que tout se défasse, son, image et sens, tout encore, mais cela qu'on a voulu, trop voulu, comprendre, saoul comme les hurlements de la nuit à vouloir comprendre, le ciel, et pourtant on l'aurait voulu, ces quatre lettres aussi inaccessibles que dieu ou la langue quand la langue se délie dans les étoiles, ce qui, faussement, devrait être alors un autre mantra, contre-nuit répétée jusqu'à s'imprimer sur vos nuits telle une persistance rétinienne faite du creusement de l'absence de son, d'image et de sens, contre-nuit trente-trois millions de fois, jusqu'à ce que, moi aussi, déjà, opérateur du noir et du vide, cherchant d'autres langages, contre la nuit, nuit contre nuit, le répétant trente-trois millions de fois, jusqu'à ce que ce mantra imprime, jusqu'à ce qui, jusqu'à ce que, comme une négativité devenue vivante, devenue aimante, devenue une assomption neutre, contre-nuit, nuit de nuit, et répéter ces échos, ces variables jusqu'à ce que tout se défasse, son, image et sens, et tout cela qu'on a voulu, trop voulu, comprendre sous ces quatre lettres aussi inaccessibles que dieu ou le ciel --- nuit et pourtant on l'aurait voulu, chiennement, comme les chiens hurlent à la nuit, comprendre, avec cette passion primitive, ah, je viens de là, moi aussi, déjà, la magie opère, noire de la langue quand la langue se délie dans le fond de la gorge animale là où je vous emmène, avec les chiens, les étoiles, les nuits, le ciel, le son, l'image et le sens, la passion primitive et encore, et encore, la nuit à défaire, à refaire, mais ce qui faussement devrait alors être un autre langage, alors, relater cette épopée de la nuit sur 8 000 révolutions solaires avec chaque personnage enduit de toute sa nuit, avec des craquelures dans lesquelles faire encore les découvertes d'autres nuits, avec les déliaisons des idées, des images, souffles réinventés, ciels variables. Météomorphoses de la nuit en des corps nouveaux, en des divinités décapitées dont sort la nuit de notre modernité, elle sans images, elle dénuée de sens, elle recouverte de milliards d'absences d'une vie à venir. Peut-être. D'autres milliards de nuits et d'autres milliards de peut-être, de cette, de cette contre-épopée balbutiante de toute la nuit épuisée d'aujourd'hui, à refaire demain, c'est-à-dire aujourd'hui. Encore à dire et à dédire trente-trois millions de fois, à traverser avec des météomorphoses devenues vivantes, aimantes, assomptions neutres de quelques signes noirs dansants dans la nuit ensorcelée, par qui, par quoi. L'oublier. Danser la nuit lente, les signes innombrables, multipliés, à même la nuit. Se confier plus loin. Écrire avec une ardeur qui n'est plus la nôtre une poésie qui n'est plus la nôtre, des continents, des espèces, des ombres de plantes qui ne sont plus les nôtres, où circulent des figures sans visages, nôtres encore, et pour rappeler alors, ce ne sera que des éclats, si lointains, de ciel, de son, de chiens, souvenirs comme vous nous avez faits, imparfaits instants empruntés à des siècles et des siècles de lectures --- et chaque poussière, chaque atome de plomb et de fer, un univers. Un univers et une nuit. Tout ce qui revient là, dans l'eau trouble de ces nuits d'aujourd'hui, c'est-à-dire de demain, passées, avec tant d'absence, revenues avec tant de latences, que maintenant, signes innombrables, il faut les conjurer avec tant d'éclats, avec tout ce qui, tout ce que la nuit d'aujourd'hui a rendu impossible, à la pensée et à l'espace, à des espèces et à des ombres de plantes qui n'existent plus.

```{=tex}
\clearpage
\begin{nscenter}
•

\hspace*{3em}*

\uline{\itshape Nuit sans yeux des premières nuits sur Terre où apparaît la vie}
\end{nscenter}
\vspace{1\baselineskip}
```
Terre Vive. Seule Flamme. Terre Vive. Terre Lente. Terre Flammes. On vise le ciel étoilé et ciel on retombe dans la boue de la prose avec des siècles d'échecs. Os cassés dans les lointains. Des nuages se forment. Des poussières. Des membres pétrifiés. Des lointains. Des ici. Des masses fluides. De la boue des siècles et d'échecs toujours recommencés. À la faveur de la nuit. Des pierres contenant toute la nuit à venir. Cellulairement. Terre Vive. D'ailleurs, comment suis-je arrivé·e moi-même ici ? D'ailleurs. Depuis une noirceur sans nom. Oui, ma nuit aussi vient d'ailleurs. Vient d'une autre nuit, nuit mêlée au ciel. Terre Vive. Terre Flammes. Boue Perpétuelle. Si loin, en ce passé des rêves d'Estrella Monera. On découvre la vie qui s'y lie à d'autres nuits. À moi-même. Au limon des existences intermédiaires, à des nuits instables. Personnages instables. Survivances. À la faveur de la vie. De proses en bouches noires. De nuit en nuit. De terre en terre. On le voit, on le reconnaît, ce ciel découronné. Au rire large et sans dents. Des nuages se forment. Des lumières. Dans le ciel traversé en permanence. Avec le ciel de traversées d'étoiles mourantes, d'étoiles sifflantes. Pour donner la vie. Étoilé, traversé, ouvrant nuit sur nuit. Traversant jours et nuits. Jusqu'à faire volcans. Terre Vive. Terre Flammes. Boue du corps de trente-trois milliards de nuits et de proses bactériennes, de côtes de lumières sans tête. De boue et de prose. On tourne la tête et on voit le ciel, la Terre, petite chose, on tourne la tête dans la boue et on voit les trente-trois millions de mantras s'allumer dans la lumière de la nuit. Terre Nuit. Terre Crâne. On tourne la tête, on lève la tête et on voit ces météores s'éteindre, partir, murmurer à d'autres, chanter peut-être, là encore trente-trois millions de peut-être, et le ciel de petits échecs de clignoter, et la nuit, on tourne la tête, oui toujours, le corps enfoncé dans la boue de la prose et des défaites et l'on voit de toutes petites choses. Des siècles qui font souffrir. Illisibles. Et puis, dans le ferment de la chute et de la boue. La tête penchée, les yeux instables et pleins de brume, on reconnaît ces petits amphibiens, peut-être des gobies, autre chose, espérons, ouvrant et fermant la bouche en silence, nous regardant, dans la même boue, de leurs yeux à l'expression neutre. À quoi pensent-ils quand elles me voient ? Et à la nuit et à moi. À des impersonnalités flagrantes comme des explosions. À moi. Le ciel et le ciel. Trente-trois millions de ciels que je ne reconnais pas, que je ne regarde pas. Je vois l'instabilité. La tête penchée. Je regarde ces gobies de boue ouvrir et fermer la bouche. Respirant quelque chose du ciel qui me manque. Un ailleurs plasmatique. Qui ne s'étend pas jusqu'à moi, à demi-enseveli·e dans la boue. Pensent-ils avec la brume où le ciel et la terre se mêlent ainsi en des flammes de limon, en des saisons liquides et parfois brûlantes, et parfois glaciales ? Toute l'impermanence traversant mon corps. Cela me fait du bien malgré mes côtes et les autres os cassés par ma chute. Je découvre quelque chose comme une expression. D'où suis-je tombé·e ? Déjà. Ma branchie droite me fait souffrir. Je ne sais pas si je pourrais respirer à nouveau. Je rejoins l'ailleurs de la Terre. Si loin. D'ici. Maintenant. Des nuits. Que je rejoins par la Terre. Que je rejoins par la brume. Et par toutes les nuits. Par les défaites et par la prose. Le ciel. Le ciel ici est traversé en permanence de météores.

```{=tex}
\clearpage
\begin{nscenter}
\hspace*{-5em}*

*
\end{nscenter}

*

\begin{nscenter}
\uline{\itshape Trois figures de nuit pour défaire nos nuits}
\end{nscenter}
\vspace{1\baselineskip}
```
Filles de la parole et de la légende, étoiles pouvoir, étoiles savoir. Elles qui font l'immense nuit des signes, les paroles confuses, la nuit qui seule sauve l'Appelée, venue de la nuit de l'Alors. Autant de prophéties depuis maintenant. Depuis toujours. Depuis les temples en déshérence et le temps perdu. Dans les eaux de la désincarnation. Aujourd'hui encore et pour longtemps, les mites dévorent et dévoreront les astres cunéiformes indéchiffrables. Notre nuit de transformations inférieures. Notre nuit d'autres temps, en faveur de la saveur du temps, de l'amour et de la nuit, de la mort, et de la vie, don de la terre en la nuit. La litanie des nuits d'Enheduanna. Enheduanna, venue du désir du ciel. Le Temps de la nuit incessamment retrouvé dans l'éternité. Enheduanna. C'est la nuit qui seule sauve le Temps, les sincérités des étoiles mêlées d'un même corps, chiffré. Enheduanna la déchiffreuse. Et dans temples déconsacrés des temps se mêlant, de pierres et d'idées, nuits sauvages et divinisées, sensuelles et étoilées. Enheduanna. Étoiles pouvoir, étoiles savoir mêlées d'un même corps, Enheduanna. Aujourd'hui encore et pour longtemps, Babylone rêva et rêvera encore dans nos rêves, dans les labyrinthes de notre futur. Nuits incarnates. Enheduanna. Dans nos nuits des astres cunéiformes indéchiffrables. Notre nuit chiffrée. Enheduanna. Ta nuit claire. Enheduanna. Notre nuit incompréhensible. Enheduanna. Ta nuit aux entrailles d'étoiles. Enheduanna. Notre nuit de métal. Des immortalités immobiles, nées des profondeurs de l'espace entre les étoiles. Enheduanna. Combien de nuits ? Trente-trois millions de nuits. Trente-trois millions d'oublis. Babylone-Rêve-Nuit. Enheduanna. Et la terre en la nuit. Des signes et des poussières. Étoiles devineresses, étoiles traîtresses. Prophétisant l'inestimable. Enheduanna. Étoiles promontoires dans la grande forêt d'astres abattus, forêt fragmentée, boréale, protéiforme, appelant à des êtres appelés La Nuit. D'un chiffre de pierre noire. Isis venue du désert du ciel, de la mort et de sa survie --- don de ces nuits scarabées. Isis errante. Nuit des transformations inférieures. Isis errante. Les sincérités d'autres temps, les paroles contre-nuit. Ses replis d'ombres sans larmes, corps, Isis errante. Lieu de naissance des éclairs. Des signes et de la poussière. Larmes-corps faites étoiles. Isis errante, trace de notre trace, et dans le creux ce qui naît avec la nuit, la contre-nuit. Qui ne vient pas. Qui vient. Isis errante. À la voix cliquetante d'insecte et de pulsar. Aux replis de ses voiles d'ombres sans larmes, au fil tisserand de la fille du Chaos. Encore une fois appelée vers la nuit de l'Alors. Qui ne viendra peut-être pas. Combien de fois pierre et parole ? Fille du Chaos. Toi aussi. Étoiles pétrifiantes, étoiles désirantes. Contre-nuit, fille du Chaos. Écrire cette histoire qui ne vient pas. Fille du Chaos. Combien de fois ? Autant de fois étoiles qu'il le faudra pour faire le ciel de partout, le ciel de nulle part, la contre-nuit, fille du Chaos. Des signes et des poussières. Pour notre nuit en poussière et sans étoiles. Fille du Chaos. Étoiles sommeils, étoiles réveils. Temps du monde. Vers le ciel d'or, fille de la fille du Chaos. Isis errant dans le ciel avec les fantômes invertébrés des trous de ver, dans son linge troué par les mites et imbibé de tant de nuits. Isis errante. Abandonnant le destin, fils de la fille du Chaos. Abandonnant les rêves, fils de la fille du Chaos. Abandonnant la mort, l'espoir et la pitié, fils et filles de la fille du Chaos. La nuit se chiffre définitivement. D'un chiffre de pierre noire. Isis errante ondule, serpente dans ces grandes immobilités. Enheduanna fait le sacrifice à la parole, à la légende. Fille du Chaos. La désastreuse.

Toutes les pierres, les morts, les promesses, encore une fois, toutes les fois et combien de nuits depuis. Le vouloir toujours. Profondeurs qui de moi s'ignorent.

```{=tex}
\clearpage
\begin{nscenter}
\hspace*{3em}•\hspace*{3em}*

\hspace*{2em}*\hspace*{10em}*

\uline{\itshape Cosmogonie nocturne}
\end{nscenter}
\vspace{1\baselineskip}
```
Vous aussi, feux, qui, de nuit, demeurez dans cette steppe renversée, renforcée de contre-nuit, confiez-vous à ces noirceurs abandonnées de la nuit, faites de votre peau cette immense peau trouée de nuit, cet immense animal sans plus de tête ni d'espoir, animal globulaire sans plus de passion que celle des trente-trois millions de nuits se convulsant dans les éclats de chaque rage nucléaire dont on ne connaît ni le nom ni l'âme mais qui forment par des courants d'une mer sourde les maladies de l'invisible, celles que l'on a aujourd'hui oubliées et dont on souffre encore, oubliant les feux stellaires, les souffles des nuages, l'espoir de trente-trois millions de nuits oblitérées. Je ris. Je ris solitairement. Faisant de ces découvertes. Encore, toujours, parfois. Je me répète. Je tente de donner consistance à cette nuit-contre-nuit, à ces aboiements d'étoiles et de sang de fantômes, d'esprits et de baisers, plus que je ne pourrais exprimer. Je te vois toujours dans ces nuits vagabondes. Je lève à nouveau la tête et je revois la chevauchée des grands Khans fantômes et leur meute meurtrière de nuits. Tout y manque dans cette histoire depuis les nerfs conducteurs que j'invente dans le devenir de la nuit, perpétuel et changeant aux plages de lumières lointaines, aux montagnes oubliées, aux équations découvertes dans le sable du désert, aux moustiques aveugles, là, là, et là. Laisse-moi encore te dire. Moins gravement que tout à l'heure. Que toutes les heures. Ici manque aussi un fluide, peut-être t'en es-tu aperçu. Paix fracturée d'où la contre-nuit siffle. Embrasse ce serpent. Tous les venins sont bons à prendre, compressant la poitrine, faisant rentrer un peu de cette nuit très fine qui te fait revenir aux continents des Khans fantômes. Fluide des halos, des avidités, des morts rendus à toute l'expansion qu'est la nuit. Oublie les sables des consistances de leur grand désert. Chante. Ris de tout ce que l'amour n'aura su exprimer. Qu'aucune voix ne saura exprimer. Fors la nuit, et dans la nuit, la contre-nuit. J'ai la tête nuageuse des envies renforcées dans le revenir, je veux tenter la vie, le contre-chant, la contre-nuit contre l'oubli et la mémoire de siècles ainsi désarticulés trente-trois millions de fois, et d'âmes. Pluie du ciel animal. Forme avec moi tes pactes de cristal avec le ciel des oubliées et chante. Faisant apparaître les Khans fantômes aux aboiements de bienveillance. Aux souffles nucléaires. Aux trente-trois millions de lunes grasses, aux largesses de mer de lait découvertes dans le creux de la nuit. Un fluide nous répète. Je le répète. Je crie dans les aigus avec ce chant écrabouillé par la constance de la nuit. Entends. Déporte-moi. Ressens ici, ce qui manque. Ce qui laisse ici imaginer des manques, des espoirs, des étoiles et des déserts. Toutes les couleurs et les trente-trois devenirs du rouge. Grands bonheurs s'étalant sur les grèves noires de l'infini vagabond. Tu vagabondes toi aussi. Tu n'entends pas. Pas assez. Et ta voix. Toujours ce gouffre où les étoiles ne respirent pas. Toujours, parfois. Où en es-tu de ton corps ? Avec tes os, tes clavicules où le monde est gravé et où tous les anges à venir viennent pleurer : accueille-les avec bienveillance. Souffle. Étends ton souffle. Recrache le venin qui est devenu ton sang noir où vivent tes étoiles. Plus encore parcours l'espace, chiennement. Fais passer par tes poumons l'air rouge des chaleurs renversées, des trente-trois milliards de choses mortes en toi, des trente-trois millions de pactes faits avec les fantômes des passés qui sont aussi ceux de l'avenir. Encore, toujours, équations à inventer, chants à reprendre, parce que dans le chant, dans le contre-chant, deux voix, diphonie ébranlant toutes les neiges aux révoltes soudaines, se relatent des envies renforcées par la vie, par la lune, par les nuages amoureux. Au fond, c'était ça la vie, la nuit, la contre-nuit, une basse continue et de petits cris aigus d'animal écrabouillé. Perpétuelle et changeante. Nous reviendrons à ces nuits. Porterons des vœux plus loin, dans des régions reculées qu'aucune révolte n'a encore consumées comme moi, rage des contre-nuits contre toute l'avidité, les destinées parasites auxquelles on nous demande de mordre, à contrecœur.

```{=tex}
\clearpage
\begin{nscenter}
*\hspace*{5em}*\hspace*{8em}*

\hspace*{2em}*

\hspace*{-12em}*

\uline{\itshape La nuit trahie seconde par seconde}
\end{nscenter}
\vspace{1\baselineskip}
```
Témoins de ces nuits vides, rappelant le feu noir des ruptures, avec nous, les terres désertes, les cieux déserts. Nous, les terres désertes. Alors devenir le bleu et les rêves pauvres de ce monde. Alors résister avec l'obscurité de ces *ainsi la nuit*, les dépravés des étoiles, et mener d'autres enquêtes maltraitées avec la nuit, avec elles reconnaître --- la nuit aux secondes par seconde dépassée. Une seule longue seconde alors enterrée dans tant de terre et ce qui ne reconnaît pas ces clartés intranquilles. Terre d'une nuit des ruptures, des mouvances, des vagues bleues du désert de jour, de cette respiration faite de la nuit. La nuit dévore, avec ce souffle de patience végétale. J'en sais tant de torturées. Nuitamment. Alors, devenir les trente-trois millions de trahisons que le jour pourtant abrite et qui se taisent. Tout ce qui s'est tu avec les attentes dans des sables sans consistance. D'où ces résistances --- nuit et contre-nuit. Étoiles de chair dans la Terre dans la nuit de toutes les secondes, disant quelque chose des espoirs de la nuit vague des lumières trop fortes, du clair-obscur désiré du ciel étoilé. À vous de devenir les chaleurs renversées et d'autres synchronicités et d'autres symbioses avec toutes les étoiles qui manquent, privées de vie, et de visions. Contre-nuit contre tous les pactes morbides de la nuit et de la mort. Témoin du rêve et de l'instant. Tenir dans le feu noir le contre-feu de la contre-nuit. Restituer les sinuosités déployées sur tant de temps. Les douceurs des vers traversant l'histoire de la Terre dans une nuit seconde, enterrée dans tant de terre et la faisant respirer. Dans les temps rougeoyants. Alors d'autres nuits se diront à même la nuit. Avec les ciels nébuleux des volcans et des révolutions, le Romantisme en feu aussi. Avec les rêves pauvres de ce monde inconscient. Devenir ce contre-feu maintenant, les odeurs des amandiers la nuit. Dans un instant, l'infini. Ayez le cœur magnanime. Si vous n'en avez pas, faites-vous en pousser plusieurs. Il nous en faudra tant pour éprouver ce qui se passe maintenant dans les trente-trois milliards de nuits du vivant. Pour nos trente-trois milliards de morts. L'utopie angoissée. Le rêve désastré. Vacillant dans des états intermédiaires. Devenir intermédiaire. Crépusculaire. Pour mener d'autres enquêtes, maltraitées avec la lune coyote, les histoires enterrées avec les fleurs coupées, l'amour cruel, avec nous, et vous encore, avec les fragilités désabritées. Devenir le râle assassiné dans un motel la nuit, devenir les trente-trois milliards de brûlures de cigarettes dans la nuit torturée. Devenir les trente-trois millions de fenêtres pour entendre les voix que rien n'interrompt. Vous aussi, détectives stellaires, qui arrivez avec les lumières que la nuit dévore, avec ce souffle court des abysses spatiaux, disant quelque chose des espoirs de la nuit, enquêtez sur ce qui a maltraité ainsi la nuit, dépravé les étoiles, obscurci la lune avec un romantisme goudronneux, des meurtrissures de patience, des fonds de cale où la nuit a toujours été plus que la nuit. Écoutez quand les coyotes songent, eux aussi, et dans leurs rêves n'en finissez pas avec le temps rouge des utopies, des odeurs des orangers dans les cathédrales de vos corps, éclairées par autre chose que la nuit, les vagues bleues du désert. Cathédrales de feu, aussi. Vous vous devez à tant d'autres choses. À l'ininterruption de la nuit. Contre-nuit. Mais le Feu, aussi. Des mouvements nocturnes pour ne pas céder. Tenir dans le feu noir des grottes. Graver des énigmes à d'autres que soi. Observer, toujours plus finement, les clignotements des lumières lointaines d'invisibles dedans. Clignement des paupières des Alors, volcans dans les graviers de l'allée, fantômes avec qui se lier comme aux étoiles. Avec qui céder et se tendre à nouveau. Témoins d'une nuit de sable dans le ciel vide de mystères. Étoiles de chair nocturne meurtrie.

```{=tex}
\clearpage
\begin{nscenter}
\hspace*{2em}o

\hspace*{5em}\$\hspace*{12em}*

\hspace*{-12em}*\$\hspace*{3em}*

\uline{\itshape La nuit vue du capitalisme}
\end{nscenter}
\vspace{1\baselineskip}
```
Un ciel d'attente comme un fond d'écran, de combien d'écrans exactement, de patience dépassée, de valeurs décomposées, vendues à la pièce ? Un ciel d'attente composé de combien de sommeils abolis ? Une sonnerie retentit ailleurs. Ici et maintenant on fixe ce ciel d'attente comme un fond meurtri, un écran de rêves bleuis au maximum, bleu de Prusse nucléaire poussant en forêt dans nos rétines. Combien de temps a-t-il fallu pour obscurcir la lumière de toute la vie des étoiles, toute leur signification, toute leur présence, toute leur présence autres que d'infestation mortifère ? Un rêve s'endort. On ne le retrouve plus. On s'en fout, on l'abandonne : il y en aura d'autres. Il y en aura d'autres. C'est toujours la même chose. Des catastrophes qui ne se craignent plus les unes les autres, qui parcourent en bandes la grande nuit future, s'agglomèrent, fondent une holding, boivent des bières électriques dans des bars célestes, des typhus de 8 000 ans, crachent des glaviots noirs sur la nuit noire. Nous avons dépassé l'humanité en nombre d'écrans. En nombre de morts. En terres inutiles. Nous avons désappris à compter les Grands Nombres et les minuscules vivants. Je ne crois pas. En rien. Forêt dans la gorge, graines de corps qui pousseront encore ? Encore ? Dans l'ozone stagnant pour tout placenta. Les continuités que l'on vit maintenant clignotent de tant d'yeux. Nuits électriques où j'ai perdu la langue avec la nuit. Comment reprendre langue ? Comment embrasser de nouveau ce qui s'est fait spectral, reflet, simulacre. Lumière faite de spectres, bleuis au ciel blanc d'yttrium, de tantale, de cobalt, de toute la famille des lanthanides composant une nouvelle constellation dans nos nuits intermittentes. Des nuits qui maintenant se retournent blanches de rêves, noires de monde, d'un monde dont personne ne veut et qui s'étend pourtant. Qui s'étend comme l'entropie galopante de tout système. Nuit obscurcie d'écrans à vouloir boire ces ciels instables faits de voltages incroyables. Dans les méandres des bunkers pleins de câbles. De vraies et de fausses lunes. Des animaux faits de voltages incroyables. De tempêtes solaires. Je clique sur "refuser". Je clique sur "éteindre". Rien ne s'éteint. Rien ne se défait. Tout veille et continue, s'étend. Se lie à soi. Et nos nerfs, nos nerfs eux aussi, ont été faits à cette intensité maintenant. Comment vouloir faire muter ces fables, ces forêts de fibres, ces continents de data, les faire retourner dans la terre, se lier avec les tubercules. Nuit. Faire en soi le désert et la nuit, composer les dunes blanches, les dunes rouges. Regarder le grain du monde, diffracter l'attention, la lecture patiente du monde. La part manquante du monde, de lumière et d'ombre. Composés de tant de fractions, de lumières. Nous avons dépassé la nuit future, nous l'avons abolie d'une ombre si noire que l'on n'imagine plus comment se lier autrement avec la nuit qu'avec des arcs électriques, composer d'autres continuités que celles des tempêtes minuscules de nos attentes. Comment retrouver la nuit. Regarder la langue muter en des étoiles de terre. Elles fondent maintenant. Se répandent en limon jusqu'à la Terre Vive, jusqu'à l'Égypte antique et jusqu'à Blue Babylon Elles désignent des nuits où nous avons désappris le monde et le sens, pour accueillir le désert, le bleu de nos nerfs, les fonds des catastrophes aux profondeurs de sommeils patients où dans notre aveuglement nous nous faisons pousser de nouveaux yeux. Nous renaissons avec des animaux de fibres, des lunes végétales, des manques miroitant avec le vivant, le reflet, le simulacre, promettant quelque chose d'une contre-nuit où du monde on vit avec tant de vivants, de défaites, d'intensités délicates, de langueurs inutiles, de nuits lentes, de danses extatiques. J'embrasse ces paroles du vide, je les adresse au rien pour qu'il les diffracte dans la forêt de l'attention, dans la nuit noire aux auras mortes ou vivantes, solaires, étoilées.

```{=tex}
\clearpage
\begin{nscenter}
*

*\hspace*{8em}°

·

*\hspace*{4em}*

*

\uline{\itshape Au milieu de la nuit}
\end{nscenter}
\vspace{1\baselineskip}
```
Nous sommes toujours dans la nuit par le milieu. Par tout déploiement et tout obscurcissement. J'aurai du mal à le dire et tu auras du mal à le penser. Nous faisons chacun un pas vers le milieu. Faisant exister ces rapports, ces chants inconstants, articulant de nos mandibules le penser dans des champs immenses --- maïs à l'infini. Il faut des mandibules aujourd'hui pour penser ce milieu et cette nuit maintenant que nous sommes chacun un ciel consumé. Par le milieu. Que de résonances passées, archaïques, renouent avec les ravages, figures contre les figures, points de vies et de disparitions traversés des mêmes lignes d'existence. Alors il aurait fallu notre disparition vibrant encore de la nuit, avec l'espace. Avec les signes du milieu si difficile à reconnaître. Il aurait fallu des existences par le milieu. Toujours renaissant et se liant par le milieu de la nuit. Enchevêtrements. Univers-ventre d'un ventre à un autre. Tant et tant. Nous avons fait du *qu'importe* notre "raison d'être". Nous avons... Qu'importe. Autre chose que les morts de l'univers. Démort, démort, démort, comme autant d'invocations au monde. Comme autant de vœux pâles. Tu le sais. Faire autrement que Callisto. Pourtant chère à mon cœur. Nous n'avons eu que Callisto et quelques autres. Faisons chacun des étoiles, et nos visages, des visages faits de mites, et en si peu de temps. Si peu. Au milieu de la nuit, nous y sommes toujours par le milieu. Peu de temps. Le temps de naître. Trente-trois millions de fois. Peut-être moins. Défendre ces peut-être. Un peu. Si peu. Orion, nous l'avons vomi avec tant de violence, lui et toutes ses âmes brutales, ses pensées arrachées. J'aurai du mal à le dire --- la Terre, cet au-delà enfonçant au-delà, dans cet au-delà reconduit à des insomnies d'extases et de vie. Nous avons rejeté la tête en arrière, le sang a afflué et nous avons cru voir des étoiles. Il aurait fallu nous couper la tête à ce moment-là, à ce moment précis. Nous avons vu des fantômes d'étoiles. En des clignements de l'œil noir de l'univers. Nous avons préféré (vraiment ?) cette noirceur. Il nous en coûte. Il faudrait des larmes versées pour tout, pour tout ce déploiement et tout cet obscurcissement. Nous avons cru nous passer de la croyance. Nous avons avalé la nuit, nous l'avons recrachée avec tant de violence, en si peu de temps. Des Orions acides dans l'estomac, en cachets acides, en insomnies techniques, minutieusement laborchestrées. Mais croire, par le milieu, la nuit et les étoiles, et les tenir dans les pierres de nos cœurs, j'aurai du mal à le dire et tu auras du mal à le penser. Au milieu de la nuit, nous y sommes toujours par le milieu. Nous avons nos pensées dans la terre et au milieu de la nuit, dans nos images. Nuit, nous, images. Vie faite de complexes fils d'étoiles sans tête, sans figures, purs désirs transitifs que ces étoiles-là, figures à réinventer, des espoirs à voir des fantômes d'étoiles. Faire autrement, nous n'avons pas su. Préserver les étoiles pour ce qu'elles sont et au-delà, dans cet au-delà reconduit à la Terre, de cet au-delà enfonçant ses racines dans la terre et poussant par le milieu. Des champs d'étoiles plutôt que de maïs. Philosophie pop-corn animée par le fantôme de ces étoiles-là. Et tu te plains maintenant *j'ai la tête photocopiée par les flashs des satellites*. Il faut apprendre à croître avec le désastre, et les adventices, à pousser étroitement, stellairement, dans les ruines et les pierres de nos cœurs. Le temps s'avale, erreurs et raisons et marée. Tout ce que l'on croyait avoir oublié revient avec la marée. Et pourtant il faudrait aussi se débarrasser d'Orion et d'Andromède, avec des figures, des espoirs, des gestes à réinventer, des espoirs à faire dans la nuit, avec des chrysalides qu'on appellera étoiles, un temps, le temps seulement, le temps qu'elles redeviennent autre chose que divines --- présences contre les ravages, figures contre les figures, points et suspens, faisant exister ces rapports tendus entre nous et la toile de notre disparition, vibrant encore de chants inconstants, articulant de nos mandibules maladroites ces constellations de vides, ces visages faits de mites et de trous dans l'espace de la vie. Figures contre toute la description, vœux formés des fils d'étoiles au-delà de leur image. Vie faite de complexes, de fictions reconnues et aimées. Des lumières de Callisto. Elle encore. Elle malgré tout. Que de divines présences contre les lumières. Faisons chacun des étoiles. Des constellations nouvelles. Noires ou blanches. Maladroites constellations de vides, millions de terreurs dans lesquelles nous nous nouons. Qu'importe. Autre chose que les morts et les résurrections et les trente-trois millions de terreurs dans lesquelles nous avons baigné --- négativité absurde du néant. Il faudra autant de cœurs que d'étoiles, de pensées déliées, d'insomnies, d'extases et de vie. Croire, décroire, faire des vœux autrement que pour les larmes versées pour tout le ciel consumé. Croire, décroire, ne plus décrire, ne plus crier, faire de la nuit, de l'espace, des vies et des fantômes, enfin autre chose que des résonances passées, archaïques, renouer avec eux, avec elles, des fils de vie, des chrysalides de rapports tendus avec les disparitions vibrant dans le présent, tenir leurs promesses, retisser, ensemble, solitaires, les vœux ininterrompus de la nuit et des étoiles, *tenir la parole des fantômes*.

```{=tex}
\clearpage
\begin{nscenter}
\hspace*{-3em}*

*

\hspace*{3em}*

*

\hspace*{3em}*

\hspace*{6em}*

\hspace*{3em}*

\hspace*{6em}*

\vspace{1\baselineskip}
\uline{\itshape Nuit déclassée}
\end{nscenter}
\vspace{1\baselineskip}
```
J'ai bu, trop bu, désolidarisé toutes mes vertèbres, une à une, peu à peu, elles toutes je les ai appelées de noms inconnus et les ai portées dans un ciel fragile, et elles, même brisées, elles ont été constellations, elles ont été. Sûrement, aussi sûrement que l'été. Alors dans ce ciel calcifié et pourtant si peu, de si peu, alors dans les éclairs de leurs fissures j'ai dû apprendre le langage de leurs tremblements, les miens sûrement. Un peu, si peu. J'ai appris quand je pouvais continuer ou arrêter. Parler encore un peu. Un peu. Malgré la douleur ou ce sifflement continu des paysages, de la nuit, dont le dernier visage, effacé à même la peau, est celui des insomnies. Il y a là une douleur véritable. Vraiment ? Si peu. Quelque chose s'efface. Il y a là une grande vérité. Je pensais parfois (et je me trompais) que nous n'avions de sens que dans ce genre de passage, là où, s'effaçant, dans ce moment où toute la nuit se creuse d'espaces discontinus, globulaires, se compose alors, un peu, si peu, l'oubli et le monde, et pas seulement cela, mais des présences --- des proximités fatales, des ondes qui ressemblent à la danse et au bonheur. Mais pensant à tous ceux-là, je m'efface. Je m'efface deux fois, mille fois, trente-trois millions de fois. Et je lève la tête. Et la cabine de la grue d'osciller, vertèbre après vertèbre. Et le ciel. Compact comme l'été et la mort. Nous touchons par là, étrangement, à d'autres étoiles, à nos corps et à notre désastre. Partout ce n'est plus la nuit, c'est une continue discontinuité de corps de chantiers, d'animaux sacrifiés, d'obscurités malades errant d'hôpital en hôpital, de nos corps à nos corps. Nous avons répété tes illusions bavardes, tes taux de profit faits la nuit dans le dos du Grand Serpent, aux coûts répercutés trente-trois millions de fois sur trente-trois milliards de chantiers, où l'on fuit, presque totalement saoul de fuir sans cesse, de cet alcool de nuit inondant la terre, saoul d'avoir trop bu de désespoir, tu le sais bien, toi qui as cru à la nuit des satellites, aux Mirage 2000, aux nations sales à millions. Nuits sales. Je vous ai connues et tous vos visages se sont maintenus dans des fragilités sans cesse reconstituées.

Je lève la tête et des constellations fissurées s'échappe un plasma qui nous absorbe. Et ce n'est pas la nuit. C'est une contre-nuit faite corps. Et d'autres douleurs faites nuit, dans le corps. C'est un corps là, d'été renversé. De fatigue incompressible. Des os assemblés de nuits si noires. C'est un corps là, livré à la surveillance de tous les fauves indomptables pour toutes les chasses sauvages répétées trente-trois millions de fois dans les bois chétifs, dans la nuit rose orange, dans les champs de colza oscillant sous le passage des Mirage 2000 sous lesquels des corps fuient, paniqués, tentant de sauver leur vie, d'une nuit à l'autre, avec leur tête levée vers les carcasses du ciel. Ce ciel de si peu. Si dur et poreux. C'est un corps là, c'est un corps, c'est un corps défendant et défendu. C'est un corps là qui vit à l'écart de toute nuit, entre la nuit et la contre-nuit. Entre la nuit et tous les désordres, entre la nuit et toutes les errances, entre la nuit et toutes les exclusions dont se nourrissent toutes ces enseignes, tous ces regards de métal, tous ces vœux adressés à ce qui, on le sait, ne peut nous sauver. Une chapelle expiatoire de filaments dorés et roses sont allumés. Les pièces de l'hôpital s'éteignent une à une. Dans les déserts, tous les déserts, dans tous les chemins inventés par le désarroi, fabriqués par le capital, des corps. Des corps et des corps formant véritablement de leur corps ce que j'appelle la contre-nuit. Plus qu'aucun·e d'entre nous, plus qu'aucune négativité. Je lève la tête à nouveau et ne regarde même pas les espoirs satellites tourbillonner dans la nuit sale. Je le sais maintenant --- le sais-je vraiment, Grand Serpent ? --- que tout cela n'était en rien une fatalité. Nous avons cru comme de la viande à attendrir, comme de la viande à hacher, nous avons cru à la fatalité, à la douleur, à l'effacement aussi. À ce que d'autres enfin. Enfin. Nous si peu. Et ces corps encore. À peine un peuple, si peu un peuple.

```{=tex}
\clearpage

\begin{nscenter}

\hspace*{6em}*

\hspace*{2em}*

\hspace*{4em}*

*

\hspace*{2em}*

\hspace*{-6em}*

\hspace*{-4em}*

*

\hspace*{-1em}*

\vspace{1\baselineskip}

\uline{\itshape Leçon des lézardes qui font la nuit d'or}

\end{nscenter}
\vspace{1\baselineskip}
```

Avec cette histoire de contre-nuit je suis allé·e si loin, j'ai appris des gestes parce qu'elle était seule. J'ai appris des rêves parce qu'elle était seule. J'ai appris des corps. Parce qu'elle était immobile, pour qu'à travers moi, à travers les déplacements grossiers de ma langue pleine de nuages et de cendre, à travers les quelques signes de mes bras et de mes mains, à travers mes passions désincarnées, mes gestations vaines, mes nuits d'or indigeste avalées d'un coup de gorge en arrière, avec le siècle et les autres siècles, pour qu'elle seule défasse le temps pour d'autres temps s'attouchant à des continuums de spectres, de passé et d'avenir, respirant à travers moi et pour elle --- alors elle un peu, cette contre-nuit a existé, ce peu de temps. Si peu. Mais ce peu de chose me convient. Ces lacunes font aussi cette réalité formée des puissances du vide. Alors, même ce que cela a eu de grotesque et d'épique, cela que je n'ai pas évité, cela que j'ai appelé, convoqué depuis des lunes fades, des images sonnant comme des animaux à l'agonie alors qu'ils nous appelaient, nous, nous seuls, à ces déplacements, à ces sentiments à distance que je n'ai toujours trop su comprendre qu'à travers le malheur, quand il faudrait tant d'autres résonances dans les os de mes pensées, cela même était nécessaire, peut-être. Faites-nous grands comme l'espérance attentive à toute la nuit --- dans ce que la nuit nous rend attentifs au monde, à ce monde dont on est la proie, dont on est le bonheur pour quelques instants, dont on est la beauté que je n'ai jamais pu évoquer parce que je n'aime pas convoquer de chimères. Il faudrait des souffles et d'autres histoires, les protagonistes invisibles de la contre-nuit : résistances aimantes, flammes des morts, terreurs désirantes et toutes les fertilités du négatif traversant la nuit, dans ce vent de contre. Ah, cette langue de contre-nuit qu'il faudrait pour en dire l'écart et la fluidité, l'erreur et la succession hors de l'image, hors du récit, cela était peut-être impossible à dire, car à dire en soustrayant, peu à peu, tous les mots, les abstractions, en faisant aussi sentir qu'autre chose que le temps, qu'une autre dimension du temps et de l'espace se donnait dans ce qui n'est pas --- dans ce qui n'est pas du tout une belle idée, ou un concept forgé par un idéalisme aveugle et aveuglément décuplé, un désespoir déconcentré par les désirs. Voulant nuit. Apparaissant jour. Nous défaisant. Nous à dire. Mais que dire. Ce siècle corps. Visages de pierre. Voilà ces nuits. Je les embrasse avec des morts vertigineuses. Elles nuits. Nous nuits. Nuits d'Alors. Échappées d'un cri. Flammes chiennes. Lointain avalé d'un coup. Expire. Désincarne. Appelle. Mais fais-le bien. N'oublie pas des mondes en chemin. Appelle la nuit et la contre-nuit. La lune et la contre-lune. La vie et la contre-vie. Il faudrait redire non seulement la défaite et ce qui n'est plus l'espérance mais l'attention. Une concentration insensée, présentement. J'ai voulu autre chose que l'image, comme toujours, et ce qui s'est donné, dans la nuit comprimée, en voulant extraire autre chose que des huiles saintes de fleurs de plus en plus bleues, de plus en plus froides, ce sont des odeurs de pétrole et d'essence. Nous avons été les flammes des morts, et surtout des cendres. La vie, ça m'a toujours échappé. C'est ainsi. Sauf une seule nuit. Une nuit qui ne tient pas à la nuit, corps étrangement échangeable pour d'autres, irréductible pour nous. Nous avons été une nuit, cette nuit dédoublée pour d'autres mondes. Tout se concentre pour nous en de telles expériences vertigineuses. Elles seules ont été la nuit pour nous. Celles qui ont été faites avec nos corps. Voilà donc le moment des grandes sensitivités ayant conjuré la sensibilité de l'ultraromantisme --- et notre seule mystique tenue éveillée par les étoiles. Tous les anges à inventer, faits de nos gestes. Souffles. Fluides. Expirations. Voilà. La nuit oblitérée dans une autre nuit. C'est ainsi que la contre-nuit apparaît aujourd'hui, dans des nuits en boîtes dans la nuit, inaugurant un passage vers le vide, vers des noirceurs inventées, vers des désirs ayant pourtant aussi tant à voir avec ce qui a effacé la nuit et la contre-nuit, ce qui a fait aussi, le désastre. *Confersupra*. Mais que te dire. Embrasse. Regarde ces éclats de nuages artificiels dans la nuit artificielle aux désirs bien trop bleus. Que te dire. Te dire. Voilà. Car à côté de ces espaces, nos nuits sont toujours là. Nuits désappropriées pour des nuits inconnues. Trente-trois milliards de chiennes lointaines aboient à ces nuits de l'avenir. L'espace s'animalise, se peuple d'absences heureuses enfin. Je souris, je ne voudrais que ces sentiments inventés se dédoublent autrement. Trente-trois milliards de fois. Nuits et contre-nuits.

```{=tex}
\clearpage
X\hspace*{12em}x

\begin{nscenter}
\hspace*{-4em}X

\hspace*{-5em}x\hspace*{8em}X

\hspace*{-8em}x

X

x\hspace*{4em}x

\hspace*{2em}X

\hspace*{4em}x

\vspace{1\baselineskip}
\uline{\itshape Péroraison de la contre-nuit}
\end{nscenter}
\vspace{1\baselineskip}
```
La nuit est devenue poétique au moment où l'on a perdu la nuit.

Les étoiles sont devenues poétiques au moment où l'on a perdu les étoiles.

On a multiplié l'image par l'absence.

Les coefficients de marée étaient pharaoniques.

Nous avons été submergés de nuits et d'étoiles.

Étoiles de nuit.

Et des vertes, et des animales, des rayonnantes et des absentes, tout ce que l'on connaît et ne connaît pas a été nuit, tout ce que l'on a cru être beauté spectrale, oubliée, impossible a été étoile. Tout a été dans son absence. Multipliée. Prolifération sidérante. Contre-nuit inoubliable pour nous, oubliée partout --- catastrophe sur laquelle on a fermé les yeux avec tant de douceur, tant de bonheur, aveuglés par la poésie des mots, les vertus insidieuses de l'image. Toujours l'oubli nous guidait dans ces fables. Nous parlions si bien. Si joliment. De nuits et d'étoiles filantes. L'espace est devenu spectacle. Le leurre de leur spectacle. Et le ciel aboli. C'est pourquoi maintenant nous voudrions prendre chair d'étoiles et de nuit. C'est pourquoi nous avons perdu la connaissance et l'image. Que l'on a vécu cette contre-nuit faite de l'oubli rayonnant qui s'endurcit dans nos yeux aveuglés par le malheur et la beauté, submergés par les leurres multipliés des images-fables, des images-écrans ayant perdu la profondeur, c'est-à-dire l'espace, de l'absence. Éternels étés de la nuit, marée du ciel devenue la nuit des absentes, la nuit poésie devenue pour nous ciel aboli --- ciel de désastre.

Faire encore

```{=tex}
\hspace*{5em}
```
le vide de l'image

```{=tex}
\hspace*{10em}
```
--- la contre-nuit

# Cinquième Cercle / Cinquième Ciel / Autobiographie de ma nuit

```{=tex}
\def\addquoteapo
{%
  \begin{tabular}[b]{p{3.5cm}}
    \begin{nscenter}
« Et je cherche au ciel constellé\\
Où sont nos étoiles jumelles »\\
Guillaume Apollinaire
    \end{nscenter}
  \end{tabular}%
}
```
```{=tex}
\begin{pullquote}{object=\addquoteapo}
```
« ILS ÉTEIGNENT LES ÉTOILES À COUPS DE CANON / Les étoiles mouraient dans ce beau ciel d'automne / Comme la mémoire s'éteint dans le cerveau / De ces pauvres vieillards qui tentent de se souvenir / Nous étions là mourant de la mort des étoiles / Et sur le front ténébreux aux livides lueurs / Nous ne savions plus que dire avec désespoir / ILS ONT MÊME ASSASSINÉ LES CONSTELLATIONS / Mais une grande voix venue d'un mégaphone / Dont le pavillon sortait / De je ne sais quel unanime poste de commandement / La voix du capitaine inconnu qui nous sauve toujours cria / IL EST GRAND TEMPS DE RALLUMER LES ÉTOILES »

```{=tex}
\end{pullquote}
```
## Langues de nuit

```{=tex}
\begin{nscenter}
```
**1^er^ mouvement**

```{=tex}
\end{nscenter}
```
```{=tex}
\vspace{1\baselineskip}
```
J'abrite trop de voix. Et toutes sont un peu de la nuit. Et aucune, et chacune.

Ce qu'il faudrait c'est une voix, la cohérence fausse et pourtant entière d'une constellation.

Cela même --- écrire en constellations, je l'ai dit, le redirai encore, de tant de figures anonymes, de nuit faite d'ondes, de corpuscules et de désaxements.

Alors je prends l'impossible pour un genre de récit, et l'inconnu pour un type de réalité.\
Je voudrais former la nuit sincère de toutes ces nuits traversées.

Parler, mais je ne dis rien. Ouvrir le sens et la discrétion du sens qui m'échappe, dans ce qui ne devrait plus s'énoncer sous la forme du soupir ou du vent stellaire --- mais plutôt une force de dispersion et de vie, malgré tout, ou de ce qui dans le sens porte quelque chose, mystérieusement sûrement, de la vie.

```{=tex}
\begin{nscenter}

\vspace{1\baselineskip}
```
**2^e^ mouvement**

```{=tex}
\end{nscenter}
```
```{=tex}
\vspace{1\baselineskip}
```
Former la nuit sincère

```{=tex}
\hspace*{3em}
```
d'une force de récit. Et toutes ces

nuits.

```{=tex}
\hspace*{3em}
```
Et toutes ces nuits.

```{=tex}
\hspace*{3em}
```
Et toutes

ces

```{=tex}
\hspace*{3em}
```
nuits sincères

d'ondes, de désaxements.

Alors je l'ai dit,

```{=tex}
\hspace*{3em}
```
le sens

de la nuit. Et toutes ces nuits

Encore

Et toutes ces nuits encore sont une forme du

```{=tex}
\hspace*{3em}
```
sens et

chacune,

et

```{=tex}
\hspace*{3em}
```
chacune,

et aucune

et chacune

forme

de vie, mais former la vie, malgré toutes ces nuits faites de trop de

vie,

mais former

```{=tex}
\hspace*{3em}
```
la nuit

faite de trop d'oubli de tant d'entière

```{=tex}
\hspace*{3em}
```
dispersion

alors ce n'est rien

rien

```{=tex}
\hspace*{3em}
```
voix traversées

de dispersion

```{=tex}
\hspace*{3em}
```
sous la forme du sens

dans ce qui

ne

```{=tex}
\hspace*{3em}
```
devrait plus s'énoncer sous la forme

```{=tex}
\hspace*{3em}
```
du sens

```{=tex}
\vspace{1\baselineskip}
```
sous le sens

```{=tex}
\hspace*{3em}
```
sous le vent

et la discrétion

et l'absence

```{=tex}
\hspace*{3em}
```
faisant un genre de récit

faisant nuit

```{=tex}
\hspace*{3em}
```
avec la nuit

```{=tex}
\vspace{1\baselineskip}
```
Alors je ne

dis rien

```{=tex}
\hspace*{3em}
```
alors je respire

faisant nuit

```{=tex}
\hspace*{3em}
```
avec la nuit

avec l'inconnu pour soupir

```{=tex}
\hspace*{3em}
```
avec la nuit

pour seule nuit

```{=tex}
\hspace*{3em}
```
mystère sans mystère

```{=tex}
\hspace*{3em}
```
Sans limites de parole

Vent et vent

```{=tex}
\hspace*{3em}
```
Là encore

et

Le sens et la

vie

Et la nuit qui m'échappe dans ces nuits

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
ouvertes

```{=tex}
\vspace{1\baselineskip}
```
À l'inconnu

entière cohérence

```{=tex}
\hspace*{3em}
```
de ce qu'il faudrait de nuit

```{=tex}
\hspace*{3em}
```
de toute la vie

et c'est une

```{=tex}
\hspace*{3em}
```
voix.

Et tout, ou du

sens et chacune encore

encore

```{=tex}
\hspace*{3em}
```
quelquefois une voix

```{=tex}
\vspace{1\baselineskip}
```
sous d'autres formes que du souffle

```{=tex}
\hspace*{3em}
```
sous d'autres formes que des étoiles

```{=tex}
\vspace{1\baselineskip}
```
stellation

```{=tex}
\hspace*{3em}
```
de figures anonymes

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
c'est une voix malgré tout

```{=tex}
\hspace*{3em}
```
je ne sais que

cela

```{=tex}
\hspace*{3em}
```
tout cela

écrivant

```{=tex}
\hspace*{3em}
```
cela même

le vent

le vent en forme de vie

espace

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
d'une constellation

```{=tex}
\hspace*{3em}
```
de

```{=tex}
\hspace*{3em}
```
nuits

```{=tex}
\hspace*{3em}
```
malgré

```{=tex}
\hspace*{3em}
```
tout

```{=tex}
\hspace*{6em}
```
d'autres formes de vie et de récit

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
d'autres formes

```{=tex}
\hspace*{6em}
```
encore

```{=tex}
\hspace*{3em}
```
je prends l'impossible pour

```{=tex}
\hspace*{6em}
```
une voix

```{=tex}
\hspace*{6em}
```
une vie

```{=tex}
\hspace*{6em}
```
une traversée

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
corps sous le ciel

```{=tex}
\hspace*{6em}
```
déplacements

```{=tex}
\hspace*{3em}
```
souffles de paroles annulées

```{=tex}
\hspace*{6em}
```
corps et corps

```{=tex}
\vspace{1\baselineskip}
```
tout ce qui brille, trace, déploie

```{=tex}
\hspace*{3em}
```
ces constellations

```{=tex}
\hspace*{6em}
```
de paroles annulées

de vie

```{=tex}
\hspace*{3em}
```
chacune et chacune

rayonnant

```{=tex}
\hspace*{3em}
```
excédant

```{=tex}
\hspace*{3em}
```
souffles encore pour que la nuit

forme la vie

```{=tex}
\hspace*{3em}
```
pour que

rien

```{=tex}
\hspace*{3em}
```
aucune

```{=tex}
\vspace{1\baselineskip}
```
pour que les corps et corps

```{=tex}
\hspace*{3em}
```
brillent

d'un souffle

d'une voix

d'une traversée

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
retirée d'absence

```{=tex}
\vspace{1\baselineskip}
```
donnée

à des nuits et des nuits

```{=tex}
\hspace*{3em}
```
noires de paroles

```{=tex}
\vspace{1\baselineskip}
```
écrites `\hspace*{3em}`{=tex} à `\hspace*{3em}`{=tex} la `\hspace*{3em}`{=tex} dimension

```{=tex}
\hspace*{3em}
```
de l'absolu

du fer froid des étoiles

```{=tex}
\hspace*{3em}
```
des brûlures lointaines

de ce qui

```{=tex}
\hspace*{3em}
```
nuit après nuit

```{=tex}
\hspace*{3em}
```
avec le vent

```{=tex}
\hspace*{3em}
```
le souffle

ce qui se retire

```{=tex}
\hspace*{3em}
```
ce qui se donne

```{=tex}
% \vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
d'absence en absence

```{=tex}
% \vspace{1\baselineskip}
```
écrit

```{=tex}
\hspace*{6em}
```
nuit sur nuit

```{=tex}
\vspace{1\baselineskip}
```
ces constellations

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
nuit de toute nuit

```{=tex}
\vspace{1\baselineskip}
```
absentée dans la nuit

```{=tex}
\vspace{1\baselineskip}
```
formée et déformée pour

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
ce mouvant désir

```{=tex}
\vspace{1\baselineskip}
```
n'appelant rien

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\hspace*{3em}
```
faisant seulement la nuit

```{=tex}
\vspace{1\baselineskip}
```
en l'absence de toute nuit

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{nscenter}
```
**3^e^ mouvement**

```{=tex}
\end{nscenter}
```
```{=tex}
\vspace{1\baselineskip}
```
C'était une nuit et toi non plus tu ne t'appartenais pas. Elle et toi, et toi aussi tu laissais venir quelque chose de malaisé. C'était des noms trop étroits, étranglés. Des noms de nuits et je ne sais pas comment tu faisais pour vivre cela. Moi je ne sais pas. Les distances devenaient fabuleuses. Non pas merveilleuses. Non, sûrement pas. La dissolution de chaque instant dans ces moments de nuits. Que je vivais, et toi pas. Parce qu'elle et toi étiez la nuit. Et pourtant. Tu me diras aussi. J'aurais voulu être cela qui se dit. Ce n'est pas vrai. Je cherchais des nuits plus sincères à dire. Hors des murs clos. Des gestes aveugles. Mais des vagues sombres venues depuis cette nuit. Que toi. Et elle encore. Ondes. Il y a quelque chose à dire qui résiste à la forme du dire. Je ne cherchais que cela et ce que tu fuyais, cherchant à dire dans cette fuite je ne sais quel bonheur qui ressemblait trop à la nuit et aux traînées des nuages. Ce mouvement s'est perdu. Pourquoi ne peut-il plus nous porter, nous donner un peu plus, ne serait-ce qu'un pas, là-bas étranger. Je soulève mon corps avec mon corps. Je lève les yeux que le temps a durcis, que les changements ont constellés d'autre chose que la patience. Je voudrais des souffles et des nuits. Pour elle et pour toi ne cessant. Reprenant un dialogue dont je ne sais, qui ne cesse de se perdre et se disperser. Alors sans voix, sur la terre humide, le goût de sel venu de je ne sais quel inconnu. De personnes et de présences. De ce qui s'ouvre, mais peut-être ne sauve pas.

```{=tex}
\clearpage
\Huge
```
j'aurais

besoin

de ta nuit

```{=tex}
\normalsize
```
## Autobiographie de ma nuit contre-nuit

Alors que tu faisais autre chose que dormir et que veiller, tu voudrais écrire cette vie, ta vie et cette vie qui est ta nuit commencée, recommencée, cette nuit et l'histoire de ton rapport à la nuit, à la nuit creusée par son absence en une contre-nuit.

```{=tex}
\vspace{1\baselineskip}
```
Mais la nuit, la contre-nuit, ne sont-elles pas ce qui nous retire de nous-mêmes ?

Alors écrire serait être fidèle à cette impossible autobiographie de la nuit.

Alors l'écrire, s'écrire soi-même, serait se vouer à cet impersonnel, à ce pluriel de toutes ces nuits sans noms, à toutes ces nuits ivres de vivre et de mouvements, de chant et de contre-chants.

Alors nous n'aurons plus de noms pour nous.

Nous n'aurons plus de noms univoques, nous nous nommerons avec le ciel, avec des noms-univers.

```{=tex}
\vspace{1\baselineskip}
```
Il faudrait toujours se livrer ainsi à l'exercice de penser son existence, sa biographie, du point de vue des nuits. Des nuits vivipares, des nuits-scissipares, de toutes les nuits qui ont tourbillonné pour former ce chaos turbulent que tu es, ayant transformé toutes ces nuits en cosmos pour enfin exister en ce ciel.

```{=tex}
\vspace{1\baselineskip}
```
Alors, écrire depuis l'infini. Écrire depuis la nuit. Écrire depuis le dehors.

Depuis le contre-clair de lune, depuis la face cachée de la Lune.

```{=tex}
\vspace{1\baselineskip}
```
Peut-être tout est-il contenu dans ce ciel de craie d'un âge friable.

Oui, j'en ai l'impression que toute notre vie est à lire dans le récit fragmentaire et la perspective de l'histoire de nos nuits.

```{=tex}
\vspace{1\baselineskip}
```
Mais peut-être est-ce là aussi la défense du seul secret, la nuit impénétrable de la contre-nuit. Peut-être ne peut-on alors qu'en désigner les puissances et jamais en faire les descriptions vaines ou les narrations sans équivoque. Seulement créer des rêves tremblants, éveillés et évanescents, et des voix faites de plusieurs mondes.

```{=tex}
\vspace{1\baselineskip}
```
Alors, si je ne racontais rien, soustrait de l'énonciation d'une subjectivité défaite, rendue au centre du cosmos, quelque chose s'écrirait pourtant.

Alors on dira ainsi ces directions tentaculaires, ces étoiles vivantes étendant leur bras dans le passé comme dans l'avenir.

Alors on dira ces vies d'orages, ces nuits-louves, ces contre-nuits aux grandes orgues de pluie.

```{=tex}
\vspace{1\baselineskip}
```
Alors je dirai pour moi, l'apparition de la contre-nuit, lente à se révéler le temps d'un été, et au-delà, et en deçà --- février blanc --- octobre noir --- le temps d'apprendre un autre temps, un autre désastre.

```{=tex}
\clearpage
```
```{=tex}
\begin{nsright}

"En cette nuit en ce monde"

--- Alejandra Pizarnik

\end{nsright}
```
```{=tex}
\vspace{1\baselineskip}
```
Faite, en moi, de tant de visages et de terreurs

Faite encore, avec tes bonheurs

Et moi, là, sur ta peau aux écritures changeantes

Changeant l'ivresse et la peur en des corps de nuit

En des formules conjurant les ombres et les larmes d'avant en des oublis gardés par l'âge

```{=tex}
\vspace{1\baselineskip}
```
Entre nous, j'aurais voulu sauver tes visages perdus

Et pardonner ta part de nuit et nos enfances sauvages

```{=tex}
\vspace{1\baselineskip}
```
*Tout ce qui ne brûle pas ne m'intéresse pas*, disais-tu

Mais qu'est-ce qui ne brûle pas ?

Des mots et des mondes, faits pour les démons du rêve acharnés à nous poursuivre

Nos regards par ailleurs

```{=tex}
\asterisme
```
Réel et ce que je suis

nuit toujours plus lourde de nuits

boitant de la bouche

bavant les noms

les jours

et les compléments de nuits

la grammaire et la syntaxe

plaquant des accords dépassés

sur des consoles anciennes, formulant d'autres espaces

parce que

faits pour l'oubli et la vitesse

```{=tex}
\hspace*{3em}
```
la vitesse de l'oubli, brûlant

```{=tex}
\hspace*{3em}
```
de toutes ses flammes bleues

un combustible dont on nous cache la provenance

car il n'est autre que nous-mêmes

```{=tex}
\asterisme
```
La nuit qui s'infecte, le corps qui --- prévenant --- fait une boule de chair tout autour.

Nodosité de l'amour. Dire un jour quelles nébuleuses révolutionnent là-dedans. Et créent. Écrivent. Illuminent.

```{=tex}
\asterisme
```
Croire à chaque visage, à chaque douleur, à chaque présent ramifié comme l'éclair

Croire à chaque ciel, à chaque étoile qui promet, à chaque fois, à chaque fois où tu es, où tu n'es pas, impermanente bête de brume et d'éclipse

Dans les noyaux d'ombre des planètes

Je t'ai rêvée et vue

Mourir à chaque étoile

Mourir à chaque espoir

```{=tex}
\asterisme
```
Ah plus de juge --- la solitude dans le blanc spectral de mes yeux, seul témoin de ma nuit. 

Toi non plus tu ne me regardais pas.

Alors ça va. 

```{=tex}
\asterisme
```
Vers ton corps, ma nuit, la langue à des degrés plus extrêmes, volcans des morts soulevés par les cris de toutes les confusions.

```{=tex}
\asterisme
```
Nous, liés autour d'un jamais, c'est-à-dire une solitude enroulée sur elle-même dans une torsion étrange, tendue vers un soleil absent, penchant bientôt sous sa propre gravité. Appelle-la la nuit.

```{=tex}
\asterisme
```
Un texte interrompu, repris, rompu, vagues devenues vivantes, pensantes, fluctuant dans tous les sens --- sans nom, sans narrateur conscient, sans flux, seulement dans la reprise.

```{=tex}
\asterisme
```
La cendre de ces rêves trop compacts que tu me fais former, brûlant jusqu'à s'effondrer sur eux-mêmes.

```{=tex}
\asterisme
```
Qu'un seul nom efface tout le reste des noms, des messages vides pour ma tête vide, ne retenant que les messages où ce nom apparaît, blanc dans la nuit noire.

```{=tex}
\asterisme
```
Lèvre de tes lèvres je t'ai aimée avec une profondeur que la nuit n'a pas.

```{=tex}
\asterisme
```
Noirci sur la place du bonheur, que de foudres lointaines ---

qui ne tomberont que sur moi.

```{=tex}
\asterisme
```
J'aurais tellement besoin de toi, mais la nuit est là, absolument là, nuit, dans chacune de mes respirations, nuit, dans chacune de mes audaces, nuit, dans chacune de mes pensées sauvages,  nuit, m'appelant, m'appelant et me rappelant à toi, nuit, me rappelant à tout ce qui, nuit, vis en toi, nuit, tout ce qui, nuit, brille en toi, nuit souffrante et d'étoiles-soupirs, nuit toujours nuit, toujours plus claire dans ton visage, nuit, nuit toujours et toujours collant à ta voix, à tes phrases, nuit, nuit, nuit et cette nuit et je voudrais qu'elle l'unisse à ta nuit. 

En ciels incomparables, et nuit, peut-être incompatibles. 

Qu'aurais-je fait pour les rendre à la nuit ? Pour les faire respirer encore et encore, jusqu'aux soupirs, jusqu'aux abandons. Pour donner corps, pour donner flammes, pour donner forme à toutes les formes, à ce qui nous fuit, à tout ce qui se promet, à tout ce qui se défait, à tout ce qui, perdu se retrouve, à tout ce qui s'échappe et s'embrasse, nuit de la nuit, corps de tous les corps, flamme de partout, flamme de nulle part, transformations invisibles.

C'est un peu plus tard dans ma nuit que je m'apercevrai qu'il faudrait peut-être que je te livre autre chose. Toujours d'autres choses. Dont l'inexplicable, l'insensé effaçant mes traits, brouillant ma voix, exténuant ma pensée de manière exponentielle, jusqu'à n'être que ça. Nuit de ma nuit invisiblement confondue. Sans autre figure que l'oubli.

Mais vouloir encore, nuit, plus encore à mesure, nuit, sans mesure, nuit contre-nuit, après nuit, contre-nuit, alors que tout semble se perdre, parole de fantôme, et atteindre un instant, nuit, plus rien qu'un instant, un peu de nuit, un peu de joie, un peu de cette pensée folle qui te rejoint, un instant, nuit, plus rien qu'un instant, nuit, contre-nuit, du plus lointain, pour former un vœu pour que cette étoile qui s'abîme dans ta nuit me permette encore et encore de former des vœux, toujours le même, litanie adressée à ta nuit.

J'aurais besoin de ta nuit.

```{=tex}
\asterisme
```
Nuit blanche. Nuit noire. Nuit bleue. Nuit rouge. Combien de fois si absolue ? Comme si tout devenait le bonheur et le malheur. Ne sachant plus ce qui nous sépare elle et moi avec des idées qui n'appartiennent qu'au rêve.

```{=tex}
\asterisme
```
À force d'y penser --- la nuit de ma nuit a la couleur de ton visage.

```{=tex}
\asterisme
```
Horizon de couleurs et de sentiments sombres.

```{=tex}
\asterisme
```
Devenir la substance des ondes contacts des fonctions éloignées 

Le vide

L'acide

```{=tex}
\vspace{1\baselineskip}
```
Des poings pour entendre des vérités inacceptables 

Approcher le triple cœur des étoiles --- dire le malaise et le désir de ces choses éloignées qui nous sont parfois les plus *chair*

Pulser d'autres dimensions du bonheur et du malheur

```{=tex}
\vspace{1\baselineskip}
```
Vérité : je n'aime pas pleurer la nuit.

```{=tex}
\asterisme
```
Je suis tombé du mauvais côté de la Lune

Ça fait longtemps maintenant.

```{=tex}
\clearpage
\Huge
```
Théories

de cette nuit

toujours à venir

```{=tex}
\normalsize
\clearpage
```
Nous sommes de la nuit à chaque instant. Nous n'avons pas à nous y convertir.

A.S.I.F.

Sans idoles et sans salut, car tu es ce refus et cette acception --- nuit de la nuit.

Et nous-mêmes en toi :

A.S.I.F.

Notre seul mystère inconditionnel.

A.S.I.F.

Comment faire d'autres choses que de t'appeler ?

A.S.I.F.

Comment te faire advenir autrement que de manière sourde ?

A.S.I.F.

Comment t'assembler depuis les fragments d'aphasie et d'amnésie combinées que l'on appelle constellation et que l'on voudrait fiction ?

A.S.I.F.

Sans divinité, mais résistant pourtant au néant.

A.S.I.F.

Pour parler du vague à l'âme quand l'âme n'existe plus. Quand les vagues font la nuit, les marées stellaires,

Ce genre de choses, quoi.

La nuit, quoi.

A.S.I.F.

Ce qu'il faudrait, c'est concentrer l'obscurité.

A.S.I.F.

Autre chose que le mot. Déployer la nuit.

*A.S.I.F.*

En ton cœur neutre et sauvage, sentir dans nos veines le sang noir de la nuit qui ne fige jamais.

A.S.I.F.

Alors, parler sans images. Car la nuit n'a pas besoin de livre, même pas de pages arrachées au livre de la vie et avalées par l'ange du cosmos. Non, elle n'a pas même de messager, car elle n'a pas de message.

A.S.I.F.

A.S.I.F.

A.S.I.F.

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{nscenter}
\itshape

Lactoserum noctis, souvenir d'un mois d'août à Oaxaca

\end{nscenter}
```
```{=tex}
\vspace{1\baselineskip}
```
Boire à des proses plus vives. Non, pas plus vives, mais plus vivaces. S'enracinant dans nos unités décomposées. Florissant aux syntaxes de chair faite de ciels que l'on ne reconnaît plus. Changeant trop de fois dans trop d'endroits. Faits d'étoiles traîtresses et de constellations hybrides. Larmes dont je ne sais ni la tristesse ni la joie. Toutes injustifiées. Sinon la nuit. Sinon la démesure totale que j'appelle, provisoirement, la nuit. Encore. Invalide pourtant. Même ma médiocrité, je l'ai appelée, un jour, la nuit. C'est dire, c'est dire tout ce qu'il faut perdre encore pour dire ce que l'on doit restituer à la nuit. Comment l'on doit boire jusqu'à des comas d'étoiles pour apprendre les langages du corps et de l'oubli.

```{=tex}
\vspace{1\baselineskip}
```
Il est tellement tard. Bouger, moi je ne le peux pas. Je fixe l'abîme d'un verre au fond duquel se débat comme un minuscule calamar au corps si pâle dont la squame part en lambeaux. J'ai pitié de lui. C'est peut-être moi. J'ai le vertige. Je ne bouge pourtant pas. Mais je n'ose pas boire davantage de cette liqueur infusée au bois noir des étoiles mortes.

```{=tex}
\vspace{1\baselineskip}
```
Il y a une odeur de brûlé dans tout l'univers. Des histoires de singes dans la nuit. Je renifle mes aisselles. Ce n'est pas moi. C'est la Grande nuit des crimes. J'ai les lèvres trop meurtries pour des confidences. Ce qui arrive quand le temps devient ce verre, ce calamar, cette liqueur. Ma main glisse et le verre tombe par terre. Il se répand en une mare noire.

```{=tex}
\vspace{1\baselineskip}
```
Je me demande si la nuit est tous ces corps qui fuient la lumière.

## Contre-nuit A

Il n'y aura pas de "contre-nuit B". Parce que les seuils se répètent. Que les portes ouvrent sur d'autres portes. Les nuits à d'autres nuits.

```{=tex}
\vspace{1\baselineskip}
```
On parlera la langue des serpents de mer, d'étoiles et de plumes.

Des coraux se mettront à pousser sur nous.

Des poulpes viendront s'y cacher.

Des anémones y trouveront des recoins pour récupérer dans le courant interstellaire un plancton délicat.

```{=tex}
\vspace{1\baselineskip}
```
Nous dirons ce qui commence.

Ce qui se promet.

Car ce qui est à tenir nous dépasse.

"Passe la force humaine." ([f.k.](https://www.error.re/desidentite/#fk))

On doit tenir, tenir à ces rêves et à ces métamorphoses.

Je veux dire à ces métamorphoses de rêves en actions, dans des translations qui échappent à ce que la langue a de définitif, d'archaïque, de figé malgré tout dans son système de signes.

```{=tex}
\vspace{1\baselineskip}
```
Libérer le signe de cette nuit.

En faire autre chose qu'une image.

Autre chose qu'un temps.

C'était beaucoup espérer.

Autre lieu : un espace-temps magnétique pour la pensée.

```{=tex}
\vspace{1\baselineskip}
```
Dans nos constellations toujours plus étendues.

Ce ciel aussi.

## Langues de nuit --- bis repetita

```{=tex}
\begin{centre}
```
```{=tex}
\begin{nsright}
\itshape

Disons la nuit --- contre-nuit

Comme nous disons le jour --- contrejour

avec la nuit sur le fond d'autres nuits

\end{nsright}
```
```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit, mouvement

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{nsright}

Bouge encore, sans sommeil

\vspace{1\baselineskip}

trouve

\end{nsright}
```
```{=tex}
\vspace{1\baselineskip}
```
tout ce qui se métamorphose

```{=tex}
\begin{nsright}

\vspace{1\baselineskip}

le chemin discontinu

\end{nsright}
```
```{=tex}
\vspace{1\baselineskip}
```
de la nuit à la nuit

```{=tex}
\begin{nsright}

\vspace{1\baselineskip}

des échappées

\end{nsright}
```
```{=tex}
\vspace{1\baselineskip}
```
pour en apprendre autre chose

```{=tex}
\hspace*{3em}
```
que les déplacements scandant des lignes

```{=tex}
\hspace*{9em}
```
de partitions aux lignes brisées

des contrepoints accrochés aux étoiles

```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit contre les malheurs du ciel

```{=tex}
\vspace{1\baselineskip}
```
Contre la nuit et pour toute la nuit

```{=tex}
\vspace{1\baselineskip}
```
Contre la nuit aux icônes de basalte

```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit aux flammes faites d'un miel acide

```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit pour des langues de sel et de lune

```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit contre la nuit faite d'autre chose que la nuit, faite présence aussi de tout ce que l'on aura perdu et qui sera de cette translation où le désert se fait éclair, conjuration des espaces vides, des espaces clairs, des ivresses de métal, de l'immortalité somnolente, des raisons déraisonnées.

```{=tex}
\vspace{1\baselineskip}
```
Pour faire entendre ce que la nuit ne veut plus.

```{=tex}
\vspace{1\baselineskip}
```
N'arrive plus à être.

```{=tex}
\vspace{1\baselineskip}
```
Nous aussi.

```{=tex}
\vspace{1\baselineskip}
```
Dans ce mouvement de nuit.

```{=tex}
\vspace{1\baselineskip}
```
Renaissantes toujours

```{=tex}
\vspace{1\baselineskip}
```
Flamboyantes toujours, d'un feu pâle

```{=tex}
\vspace{1\baselineskip}
```
Lettres perdues

```{=tex}
\hspace*{6em}
```
Et retrouvées dans les étoiles.

```{=tex}
\clearpage
```
De quoi s'alarmer encore des rêves-capsules

Claquant contre notre palais dans le vacarme de nos vies

```{=tex}
\end{centre}
\clearpage
\Huge
```
Point, à la nuit

Étoile
```{=tex}
\normalsize
```

# Sixième Cercle / Sixième Ciel / APPELS

```{=tex}
\def\addquotesp
{%
  \begin{tabular}[b]{p{3.5cm}}
    \begin{nsleft}
    « Les étoiles tombent\\silencieusement »\\
    Sylvia Plath
    \end{nsleft}
  \end{tabular}%
}
```
```{=tex}
\begin{pullquote}{object=\addquotesp}
\itshape
```
« Tes comètes

Ont un tel espace à traverser,

Tant de froid et d'oubli.

Alors les gestes se défont ---

Humains et chauds et leur éclat

Saigne et s'émiette

À travers les noires amnésies du ciel.

Pourquoi me donne-t-on

Ces lampes, ces planètes

Qui tombent comme des bénédictions, des flocons ---

Paillettes blanches, alvéoles

Sur mes yeux, ma bouche, mes cheveux ---

Qui me touchent puis disparaissent à tout jamais.

Nulle part. »

```{=tex}
\end{pullquote}
```
## L'inéternité par les astres

Le péril croît. Rien ne sauve. On est revenu des mondes pleins de sosies de l'astronomie sans socialisme de Blanqui[^1-contre-nuit_45].

```{=tex}
\vspace{1\baselineskip}
```
Blanqui s'est trompé. Ce ne sont pas les étoiles *infinies* qui sont des mondes futurs, passés et à venir où tous les possibles se réalisent et où, peut-être, l'anarchie triomphe parfois. Une telle idée n'est que la faible reprise matérialiste d'un Paradis placé infiniment loin. Blanqui, enfermé, s'est refait un paradis plus lointain que l'Éden. Véga de la Lyre, c'était déjà quelque chose. Mais ce n'était pas assez. Il l'a multipliée dans un plurivers indéfini. Autour de chaque étoile de la nuit étoilée.

Or, le Jardin, désolé, obscurci par les cendres, est ici.

Et c'est chaque chose, chaque être qui est un monde.

Autant de mondes que d'étoiles vivantes sur Terre.

```{=tex}
\vspace{1\baselineskip}
```
Autant d'étoiles mortes, autant de Terres déshabitées, avec la nôtre pour triste paradigme ? Non pas.

Veut-on être consolé ? Non pas.

Il faut en revenir.

Revenir de tous ces paradis.

Refaire la nuit.

Nous autres, peuple de revenants.

```{=tex}
\vspace{1\baselineskip}
```
Nous autres, revenants du ciel, de l'éternité, de l'immortalité, des mondes infinis. Autant d'idées morbides qui ont permis d'enfoncer dans les crânes l'idée d'un progrès, d'une croissance sans limites. Qui ont fait de la politique une pensée *hors sol*. Hors imprévu. Hors terre. Hors monde. Hors étrangeté. En dehors de tout dehors.

La tristesse inhérente aux illimitations mêlées d'exclusions tout aussi terribles et radicales de cette idéologie s'est diffusée dans presque tout l'écoumène.

```{=tex}
\vspace{1\baselineskip}
```
Ce que promet l'anarchisme cosmopolitique de la contre-nuit, c'est la conscience de l'entropie.

Notre classe (effondrée) aujourd'hui est celle du désastre.

Atomisée. Dépossédée.

Qu'elle en prenne conscience[^1-contre-nuit_46].

Voilà tout ? Non pas.

Ce n'est rien. Ça ne relie en rien.

Pourtant la pression mortelle sur nos vies est là, sur le plus vivant et le plus précaire.

```{=tex}
\vspace{1\baselineskip}
```
On nous dit qu'il reste à formuler une utopie. Une cristallisation. Un réactif imaginaire qui fait bouillonner les 99 %.

Des désirs épars en constellations et archipels célestes.

```{=tex}
\vspace{1\baselineskip}
```
Est-ce vraiment nécessaire ?

```{=tex}
\vspace{1\baselineskip}
```
Nous autres. Revenants du passé, du présent, du futur.

```{=tex}
\vspace{1\baselineskip}
```
Revenants de l'universalisme des Lumières pour un Diverciel où le Divers s'accorde à l'obscurité, à l'univers et au ciel[^1-contre-nuit_47]. Revenus de la réaction du Romantisme aux nuits aristocratiques, d'élections solitaires[^1-contre-nuit_48] pour une communauté de nuits, déliées et reliées d'un même mouvement.

```{=tex}
\vspace{1\baselineskip}
```
Revenants des impasses du labyrinthe postmoderne.

Nous la Bête, nous le Temps, nous la Cène.

```{=tex}
\vspace{1\baselineskip}
```
Sur notre Cène où *la Terre a une maladie de peau, et cette maladie, c'est l'homme*, c'est Nietzsche, qui prend la place de Blanqui. C'est Nietzsche, professant une vérité au sens extra-moral : *il y eut une fois, dans un recoin éloigné de l'univers répandu en d'innombrables systèmes solaires scintillants, un astre sur lequel des animaux intelligents inventèrent la connaissance. Ce fut la plus orgueilleuse et la plus mensongère minute de l'"histoire universelle". Une seule minute, en effet. La nature respira encore un peu et puis l'astre se figea dans la glace, les animaux intelligents durent mourir.*

```{=tex}
\vspace{1\baselineskip}
```
Naître et mourir. Savoir et se détruire. Leçon des guerres, leçon de la Bombe, leçon de la 6^e^ extinction des espèces. Même pas besoin d'attendre le futur refroidissement du Soleil. Le poison de la connaissance est là, solaire, divin, et enivrant pour beaucoup. L'arrogance devenue une vertu. L'intelligence une sotte raison de désespérer. On ne se sauvera pas. Car "on", c'est le monde avec lequel il faut être et penser --- exister. Il faut être bête.

```{=tex}
\vspace{1\baselineskip}
```
*Sol invictus.* Soleil invaincu. Nous aussi. Ainsi Nietzsche répondrait à Blanqui : *les penseurs dont les étoiles suivent des routes cycliques ne sont pas les plus profonds ; celui qui voit en lui comme dans un univers immense et qui porte en lui des voies lactées sait aussi combien toutes les voies lactées sont irrégulières ; elles conduisent jusque dans le chaos et le labyrinthe de l'existence.* Et bien sûr, *il faut porter en soi un chaos pour mettre au monde une étoile qui danse*.

```{=tex}
\vspace{1\baselineskip}
```
Dans notre monde, des cosmonautes flottent sous la terre. Nous les ferons revenir. Témoigner.

```{=tex}
\vspace{1\baselineskip}
```
On me demande des listes d'héroïnes et de martyres à célébrer. J'abandonne à nouveau Blanqui, et j'en appelle à nos sœurs les mouches, dont l'ADN en fait nos sosies génétiques à 98 %. Elles donc. Toutes communes. Elles qui furent les premières exploratrices de l'espace à bord de fusées V2. Avant les chiens. Avant les chimpanzés. Avant les humains.

Hommage aux mouches fantômes, effacées de l'histoire spatiale !

```{=tex}
\vspace{1\baselineskip}
```
Ainsi. Il faudra revenir sur l'héritage et l'imaginaire cosmiques.

Le couplage de l'exploration et de la colonisation est trop fort pour ne pas faire renoncer à tout ce qui se promet comme "aventure" avant d'en avoir conduit la radicale déconstruction.

Dire qu'on n'a même pas écouté la leçon de ténèbres de *Solaris* nous rappelant, conjugué à l'avenir, que *l'homme est parti à la découverte d'autres mondes, d'autres civilisations, sans avoir entièrement exploré ses propres abîmes, son labyrinthe de couloirs obscurs et de chambres secrètes, sans avoir percé le mystère des portes qu'il a lui-même condamnées.*

```{=tex}
\vspace{1\baselineskip}
```
Dans notre monde, nulle étoile de rédemption. Les constellations de nous-mêmes, atterrées.

```{=tex}
\vspace{1\baselineskip}
```
On me demande comment faire la contre-nuit. On me demande ma politique de sédition. Comme si je pouvais afficher aux yeux de tous nos ennemis le dispositif qui vise à les faire vaciller. La peur est de notre côté. C'est déjà beaucoup. Ce n'est pas assez.

```{=tex}
\vspace{1\baselineskip}
```
On me demande comment diffuser cette contre-nuit infuse. Je dis *infuse*, parce qu'elle transpire, elle s'infiltre, depuis les cauchemars de cette nuit qu'on nous fait, elle se fait de manière négative (et cette négativité n'est pas un malheur, est une sensibilité au malheur plutôt, à la limite). La contre-nuit chante. Contre-chante. Elle s'entend en creux de cette mise en résonance de toutes ces sphères autrefois disjointes et rassemblées dans le motif d'une même constellation (politique, cosmologie, poésie, philosophie, astrophysique, écologie, mythologie comparée, sociologie, éthologie, tout ce qui partage et mêle les savoirs, *the queerness of it all*). C'est ainsi qu'apparaît la constellation de la désidération, celle du désastre et du désir, désir de retrouver le ciel, la vie, la mort, l'échange et le partage.

```{=tex}
\vspace{1\baselineskip}
```
On me demande des sacrifices.

Or j'avais dit pas de religion.

*Ni Dieu ni maître* a lui-même écrit le premier Blanqui. Cela, je le sauve.

Car qu'est-ce qui sera sacré ? Qu'est-ce qui sera saint ?

Qu'est-ce qui sera sacrifié ? Qu'est-ce qui sera indemne ?

Pourtant.

```{=tex}
\vspace{1\baselineskip}
```
Contre-nuit, pour autre chose que *l'indéfini*, ce *mauvais infini*, ce ciel brouillé par la lumière de Neon-City, de Capitale Songe. Pour une politique du dehors, du cosmos, de l'attention, du regard (mélange de rêve et d'égard) --- politique depuis cet impossible, cette révolution impossible --- car toute révolution est impossible avant la surprise de sa venue (*survenue*).

```{=tex}
\vspace{1\baselineskip}
```
Dans nos villes à détruire, creuse des trous.

Que chacun soulève le béton, les kilomètres de béton qui nous étouffe, qui étouffe tout, nous libérant de l'ère de l'étouffement, de l'horrible temps du pnigocène[^1-contre-nuit_49] (manière de rire encore de cette volonté mégalomane de nommer l'époque) ? Nous étouffons socialement (*black lives matter*), nous étouffons climatiquement, urbainement (sous des kilomètres de ciment, de béton et d'asphalte).

On se met à faire un trou.

On s'étonne.

La terre respire.

Les araignées sortent.

La puissance publique fait un ravaudage.

Les trous se multiplient.

La fiction prolifère.

La terre éblouissante en dessous.

```{=tex}
\vspace{1\baselineskip}
```
Et pourtant --- nuire.

Nuire à ce qui nuit, ne devrait-il pas être l'axiome de la justice, et le principe politique présent dans cette contre-nuit ?

On avait espéré qu'avec Gaïa, ressuscitée de sa haute Antiquité[^1-contre-nuit_50], viendrait Némésis, impitoyable déesse de la Mesure, de la Rétribution, fille de la Nécessité.

On a épuisé les odes venimeuses et vengeresses à Némésis.

```{=tex}
\vspace{1\baselineskip}
```
Car je te le dis à nouveau, méfie-toi des vengeances, des ressentiments, des prêtrises qui voudront se faire les représentants de ce qui nous dépasse --- je veux dire la vérité, la grâce, la beauté. Gaïa, Némésis, et toutes les figures fabuleuses que l'on est obligé de se donner pour changer d'imaginaire. Car c'est une lutte de fictions, une cosmopoétique pour une autre cosmopolitique qui se joue.

```{=tex}
\vspace{1\baselineskip}
```
Méfie-toi des Internationales des astres --- car des frontières, des nations, on a vu l'abomination, les corps barbelés, les corps noyés et les guerres toujours renaissantes. Au concert désaccordé des nations, préfère la souveraineté vide du cosmos.

Toute chose vue depuis sa nuit.

```{=tex}
\vspace{1\baselineskip}
```
C'est le système de représentation qu'il faut métamorphoser.

Ces *Métamorphoses*, il faut les écrire. Imaginer que le système actuel, de lui-même, va opérer ce changement est un leurre grossier. Ce qu'il faut changer n'a jamais eu lieu dans l'histoire de l'humanité, ou plutôt jamais aux échelles de temps qui sont les nôtres. C'est d'une cosmopolitique dont il est besoin, d'une politique du point de vue de l'harmonie du monde, de son chaos aussi.

Or nous n'avons que des politiques humaines pensant en termes humains les choses du monde. Comme le disait Simone Weil, ce n'est pas de "droit" dont nous avons besoin, car le droit porte avec lui ses guerres et ses procès. Ce dont on a besoin, c'est de justice.

```{=tex}
\vspace{1\baselineskip}
```
Je rêverais d'une écriture qui reformulerait cela à la manière de Lucrèce ou d'Empédocle, parce que leurs œuvres sont de la tradition occidentale et manifestent une cosmopolitique qui résiste au désir d'un ailleurs, d'un exotisme inatteignable --- tous jaguars, tous perspectivistes, tous revenus à la société contre l'État.

Lucrèce et Empédocle, dont les textes lucioles disent de manière entremêlée le ciel et la terre, la poésie et la philosophie, les sciences et les arts, la politique et la création.

```{=tex}
\vspace{1\baselineskip}
```
Que les atomes soient tes étoiles, aussi imaginairement ronds, aussi agités de forces démentielles.

Que les bactéries soient tes galaxies vivantes.

Que tout ton ciel, toutes tes représentations révolutionnent sans trahir ni la vérité, ni la beauté, ni la justice.

```{=tex}
\vspace{1\baselineskip}
```
Dire "imaginaire", ce n'est pas en faire un parti, une cause belle car désespérée.

Dire "imaginaire", ce n'est pas un songe creux, un paradis à la Blanqui, une fuite excitée par sa propre défaite face au monde.

Dire "imaginaire", c'est porter le combat sur les principes. Sur ce qui nous agit : sur nos désirs, sur nos idées.

```{=tex}
\vspace{1\baselineskip}
```
Apprends ces états altérés de réalité.

Apprends à changer de pensée.

Expérimente la nuit.

L'*internité* par les astres[^1-contre-nuit_51].

Le vertige d'une autre pensée.

Une conférence de neuf heures sur le rêve lucide.

Apprends la transe et l'hypnose.

Apprends que cette nuit est une autre nuit les yeux ouverts. Et sois les dimensions repliées dans les circonvolutions de l'invisible.

Apprends à voir le monde.

Regarde la terre. L'intérieur de la Terre est l'intérieur d'un astre et les défauts dans les pierres rares (elles ne sont pas précieuses du fait de ces défauts) te racontent l'histoire de l'univers et de la Terre.

Conteste, avec la beauté de chaque chose, la laideur défigurée de ce monde.

Refais visage avec les mondes.

Apprends à entendre le monde.

Un concert de neuf heures de grillons et de torrent. Et le ciel. Et le ciel étoilé toujours, toujours manquant, toujours recherché.

```{=tex}
\vspace{1\baselineskip}
```
Parle toujours depuis la nuit. Depuis ce qui est exclu, menacé de disparaître. Parle depuis le point de vue de l'inconnu, d'une altérité sans nom et sans accès. Change avec elle. Tais-toi depuis le vide intersidéral de telles pensées. Fais-toi humus. Vraiment. Écoute avec l'oreille des chauves-souris et des radiotélescopes toutes les vies. Et laisse-les tranquilles.

## Debout la nuit

La nuit, des chiens prient

Dans la rue, près des palissades de bois clair

Aux clous rouillés par l'espoir

Moi aussi, je les appelle, et je prie avec eux

```{=tex}
\asterisme
```
Sur le rouge et la nuit

Les drapeaux des nuages

Les espoirs des étoiles

La contradiction permanente du vent

Et encore et encore

```{=tex}
\asterisme
```
Formant des cercles autour de la planète X

Tu attends que les corps de tous ces monarques périssent

Mais jamais ils ne deviendront hétérocères

Jamais

Le temps devient acide

```{=tex}
\asterisme
```
On en appelle encore à la dixième lune murmurante dans la mosquée souterraine

On les a aussi formés ces vœux d'échecs

Rien n'arrive à la surface

Faudra-t-il miner le ciel pour que cela arrive ?

```{=tex}
\asterisme
```
Et les villes s'animeront d'histoires inventées

passage des planètes grondant dans le ciel

paroles de comètes --- tout ce qui est flamme sera là

On ne pourra rien en dire

Peut-être cela agira

```{=tex}
\asterisme
```
Dans le camp des formes de torture

Que l'on prend pour des étoiles

En raison des aiguilles

```{=tex}
\asterisme
```
Nos corps mangés par la nuit chienne

D'aucun ciel réassemblés

Dans la gueule du monde souffler

Passer à travers

Continuer

```{=tex}
\asterisme
```
Alerte noire

Mer tendre --- des pluies qui ne ponctuent plus rien

À cela nous vouer

```{=tex}
\asterisme
```
Dans le ciel éclaté par la nuit

Dans les mouvements tu te tiens

Tu concentres en toi d'autres mouvements

Prêts à secouer le silence bavard

```{=tex}
\asterisme
```
Cristal sans prémonition

La nuit noire --- les brouillards

Des rêves hors champ

Magnétisant des lois --- brisant des lois pour d'autres brisants

Des falaises où se briser --- à nouveau

```{=tex}
\asterisme
```
Mal Mal Mal

À la fin du cauchemar tu te regardes et reconnais ta part

Une tête souffrante pourtant

```{=tex}
\asterisme
```
Les cloches n'appellent rien

Les criquets, les merveilleux criquets, dit le poète

Nous aussi, nous hélas

```{=tex}
\asterisme
```
L'oubli de la peur

Nous méritons cela

Un soulèvement d'étoiles

Nous méritons cela

Les terres

Les mers

Les horizons

Nous méritons

Un soulèvement d'étoiles et des révolutions

```{=tex}
\hspace*{6em}
```
Des spectres du futur nous disent

Qu'avez-vous ?

Nous, nous les sommes toutes

Erreurs, étoiles, terres, mers et horizons.

```{=tex}
\asterisme
```
Des points de cigarettes dans la nuit en guise d'étoiles

Ton corps aussi

```{=tex}
\vspace{1\baselineskip}
```
Ainsi, l'espoir avait donc ce goût de fumée ?

Ainsi, l'amour avait donc ce goût de fumée ?

```{=tex}
\asterisme
```
Des cris s'élèvent

Ressuscitent la nuit, pas les morts

Des cris appellent toujours les disparus

Mais le vent résiste, les voix s'emportent

Il faudra encore tenir la nuit

Contre la nuit

```{=tex}
\asterisme
```
Désarchéologiser le futur

immobile dans le mobile

Célébrer

C'est-à-dire

Consumer la nostalgie

d'autres récits

Libérer

la clarté d'étoiles polaires

faisant nos ombres plus vastes

et le présent

à venir

```{=tex}
\asterisme
```
Pas sacré

Pas vie

Pas vu

Lassé

Claqué

L'amertume à la colle

Où s'agrègent des planètes

Décompositions d'autrefois

```{=tex}
\asterisme
```
Rires enterrés et revenant à la vie

Avec nous, ascendants

Formant des influences plus puissantes que toutes les constellations

Mordant

le poème interminable

Eux tous, je les ai été, je les ai été, et je les serai

Dans des libertés égales, des actions inconnues, parfois extraordinaires

Pour briser les mots et les malheurs

```{=tex}
\asterisme
```
Ce que c'est que de taire

```{=tex}
\asterisme
```
Malédictions sur les malédictions proférées en vain

Dire

C'est vouloir faire se lever des ombres combattantes

Malédictions et miracles

Vide sur des places vides

```{=tex}
\vspace{1\baselineskip}
```
Faire

Le ciel

```{=tex}
\asterisme
```
S'étendre et faire la clarté

À cette précision stellaire, s'atteindre et tournoyer

Danser dans cette lumière perpétuelle

Alors plus de *jamais*

Une lutte perpétuelle

```{=tex}
\asterisme
```
Avoir les mots --- je crois à quelque chose de ça

À ce que tu pourrais m'apprendre toi aussi qui lis ça

À tes nuits non totalitaires

À chacune d'entre vous

À l'espace entre chacune qui nous promet et nous lie

Cela faire notre commun combat

```{=tex}
\asterisme
```
Plantes d'étoiles

Terre d'étoiles

Noire, noire comme aucune nuit ne l'a été

```{=tex}
\asterisme
```
Souvenirs d'éternité

```{=tex}
\hspace*{6em}
```
--- Sur des îles

Il neige en noir

Et je tends la langue

```{=tex}
\asterisme
```
Trouvez le monde

Changez la nuit

Pensez donc le ciel

```{=tex}
\asterisme
```
En ce moment je suis un ordinateur

Je ne connais pas la nuit

```{=tex}
\vspace{1\baselineskip}
```
En ce moment je ne suis pas la nuit

Je suis la peur

```{=tex}
\vspace{1\baselineskip}
```
En ce moment j'hérite de jours brûlés

De signaux qui viennent de loin

## Début la nuit

Généalogie dévorée de l'intérieur

Toujours la même histoire

```{=tex}
\vspace{1\baselineskip}
```
Nuit blanche engendra liane

Liane engendra vague

Vague nuit

```{=tex}
\vspace{1\baselineskip}
```
*Et cetera omnia*

```{=tex}
\vspace{1\baselineskip}
```
Rien n'arrive tout devient

Nuit après nuit

Lierres et tentations de nuit

Lignes et tentacules de vie

```{=tex}
\vspace{1\baselineskip}
```
```{=tex}
\begin{addmargin}[6em]{0em}
Nuits braves qui ont tenu jusqu'au bout

Nuits faibles qui ont eu des bras nombreux

Nuits d'orage effacées par le vent

Nuits de lumière dans l'inconnu

Nuits faites pour nous

Sœurs de silence

Nous ne sommes pas seules

Mais dans la nuit
\end{addmargin}
```
# Envoi

Ce livre sera le livre du dehors, de l'obscurité en nous et hors de nous

De la magie, de l'impossible réalisé

Ce livre sera livre s'il s'excède vers toi et en liane vers le monde et l'outre-espace

```{=tex}
\vspace{1\baselineskip}
```
Ce livre te cherche, t'appelle, te conjure

De faire briller des contre-nuits toi aussi

```{=tex}
\vspace{1\baselineskip}
```
Alors ce livre sera la nuit promise

La prophétie-silence faite des nuits tues et soudain libérées

```{=tex}
\vspace{1\baselineskip}
```
Dans le ciel des étoiles

Dans la nuit la nuit

```{=tex}
\vspace{1\baselineskip}
\begin{nscenter}
\arb{ليل من الليل لليل}
\end{nscenter}
\vspace{1\baselineskip}
```
Celle-là qui nous est intime et nécessaire, celle-là qui se reflète dans les profondeurs noires du ciel étoilé et les caractères noirs des mots

```{=tex}
\begin{nscenter}
\asterisme
\hspace*{4em}*
*\hspace{14em}*
\hspace{6em}*
\hspace{3em}*
\hspace{-9em}*
*
\hspace{-5em}*
\end{nscenter}
\clearpage
\vspace{6\baselineskip}
\begin{nsright}
```
"... un livre, c'est la nuit."

--- Marguerite Duras

```{=tex}
\end{nsright}
\clearpage
```
```{=tex}
\begin{nscenter}

\vspace{6\baselineskip}
```

**EXCIPIT LIBER NOCTIUM**

```{=tex}
\end{nscenter}
```

[^1-contre-nuit_1]: René Daumal : "L'être humain est une superposition de cercles vicieux." ... et de ciels, dira-t-on\.

[^1-contre-nuit_2]: Livre non pas indifférent, mais vivant. C'est-à-dire *mutant* avec toi\.

[^1-contre-nuit_3]: Ma voix se mêlant au spectre de celle de Michaux, reprenant ses *épreuves et exorcismes* pour refaire cette *nuit remuante*, et le *pays de la magie*. Exorcisme pour faire sortir tout l'ailleurs de la nuit, la faire inappropriable, espace, cosmique, anonyme, totale, commune --- nous reliant et expropriant toute propriété\.

[^1-contre-nuit_4]: Là toutes, là elles. Les nuits de nos nuits. Brouillées par l'effervescence du ciel, l'éclair bouillonnant de l'injonction à faire de la nuit un temps actif, jouissif, impérieusement à ne pas perdre --- cauchemar d'une Capitale Songe 24/7 façonné par les vices du *narcocapitalisme* auquel devrait faire face notre anarcho-cosmo-politique\.

[^1-contre-nuit_5]: *Idem. Idem. Idem.* Nuits se rêvant libellules et n'étant que drones de surveillance des vertus intensificatrices\.

[^1-contre-nuit_6]: Ce livre est multiple. Chaque texte est un monde, "avec pour chaque mot rayonnant mille soleils possibles. Pour chaque phrase une constellation pour des civilisations à venir. Pour chaque verset une galaxie", dis-je ailleurs\.

[^1-contre-nuit_7]: Le savoir ne te sera pas d'un grand usage. Il t'aidera, sûrement, à découvrir la catastrophe. Mais il te restera à vivre et inventer les nouvelles valeurs pour élaborer une *contre-nuit* à la hauteur\.

[^1-contre-nuit_8]: Entends : à faire muter et proliférer en toi. À multiplier les possibilités de ce que l'expérience nocturne --- que tu connais, j'en suis sûr --- recouvre pour nous, face à la "pauvreté en expérience" qu'elle recouvre actuellement (cf. *infra*).

[^1-contre-nuit_9]: Passer de "Ouvert la nuit" (celle de Morand, homme pressé, philosophie d'une ouverture et d'une disponibilité, d'une exploitation de tout et de tous en continu) à "Ouverte la nuit" désignant la possibilité d'un rapport autre : un rapport à ce que la poésie (Hölderlin, Rilke) a désigné mystérieusement sous le terme d'"Ouvert" avec une majuscule qui désigne au contraire l'affleurement des apparences et l'abandon à un laisser-être, un accueil à ce qui se donne sans attente, dans une générosité de la présence, épiphanie sans arrière-monde, voile aimé dans ses plis et replis de brume autour de la Lune\.

[^1-contre-nuit_10]: Disons magie, disons expérience, comme on dit l'émerveillement et l'émotion\.

[^1-contre-nuit_11]: Moribond. La Bête doit faire un bond hors de la mort. Ce bond de la Bête : contre-nuit, refusant l'assimilation de la nuit et de la mort.\.

[^1-contre-nuit_12]: Privation du monde --- voilà une des dimensions importantes (ce n'est pas l'essentiel, jamais, car on parle ici de nos existences, nos existences dans la nuit et le monde). Le monde de la nuit s'est absenté. Cette absence en est donc venue à désigner la nuit et la nuit l'absence (absence de jour, absence de vie)\.

[^1-contre-nuit_13]: Ouvrez vos oreilles de chauves-souris : reliaison et expérience sont deux phalènes de ce texte. Relier les savoirs autour de la nuit, retrouver les expériences de la nuit\.

[^1-contre-nuit_14]: Des répétitions. Toujours. Des litanies. Ce livre de prières adressées aux nuits adelphes, résistantes\.

[^1-contre-nuit_15]: Tout ce qui nous manque --- des récits, des sommeils. Une façon de se dérober. Une façon de composer nuitamment les ferments de la sédition\.

[^1-contre-nuit_16]: *Via paradoxi*. Nuit paradoxale, nuit neutre de la contre-nuit Absolue et réelle. Et vivante et disparue\.

[^1-contre-nuit_17]: *Idem*\.

[^1-contre-nuit_18]: C'est cette identité qui est comme au principe de la contre-nuit. C'est cette ruine de nuit que l'on veut faire métamorphoser en un lierre infiniment pâle et tendu vers les étoiles. Végétalisant le plurivers\.

[^1-contre-nuit_19]: D'autres choses que des mots. De la parole en silence --- poésie. "Celui qui marche sur la tête il a le ciel en abîme sous lui", Paul Celan. C'est avec le soleil de la bombe d'Hiroshima, tout ce qui s'est désigné comme image stellaire, cosmique, qui s'est désastré. "Soleil cou coupé", écrivait Apollinaire à la fin de "Zone". Dans ce monde où les étoiles, le soleil, la lune n'ont plus d'images, de récits et sont considérés comme de purs objets matériels, se joue en même temps leur éloignement physique, sensuel et imaginaire de nos vies. Plus que jamais rendus à la matière, plus que jamais absents de nos pensées et de nos vies\.

[^1-contre-nuit_20]: Distraire, dérober, subtiliser --- voler, rêver, érotiser, rendre subtil. Sans mot d'ordre. Tenter de faire désirer la contre-nuit, de te faire à sa démesure, à sa beauté qui ne cesse de donner\.

[^1-contre-nuit_21]: Est-ce raisonnable ? On a donné tous les raisonnements contre tous les égarements qui ont conduit à la géhenne actuelle. Maintenant, il est grand temps d'allumer les contre-feux\.

[^1-contre-nuit_22]: Telle est-elle née pour moi, étranglée plus que je ne peux le dire, le confesser. À d'autres, donc\.

[^1-contre-nuit_23]: "Dans la nuit / Dans la nuit / Je me suis uni à la nuit / À la nuit sans limites / À la nuit (...)", Henri Michaux\.

[^1-contre-nuit_24]: *Autretemps. Autrechose*. On manque de vocabulaire pour l'échappée belle qui se prophétise ici dans le magma nocturne de ces phrases\.

[^1-contre-nuit_25]: C'est totalement insuffisant, je ne suis pas d'accord, bien sûr. Cela est nécessaire, pourtant.\.

[^1-contre-nuit_26]: *Pollens*. Disant toujours avec Novalis les fragments d'imaginaire, le brouillon des mots s'assimilant à l'ADN des pierres, des humains, des pensées\.

[^1-contre-nuit_27]: Constellation. Ce sera mon dernier mot : faire imaginer, relier d'un texte à l'autre, la forme de la contre-nuit. Même si, tu le verras, forme de l'informe, elle se donne comme *constellation négative*\.

[^1-contre-nuit_28]: Manière dont Édouard Glissant --- hommage aux archipels des Noirs dispersés par la traite --- appelait l'alternative à la "genèse", racine unique tuant tout à l'entour. La digenèse dit la multiplicité du Tout-monde, menaçant les généalogies univoques, ouvrant sans cesse les récits, fussent-ils des récits d'origine\.

[^1-contre-nuit_29]: À nouveau Blanchot : "*La nuit est ce que le jour ne veut pas dissiper, mais s'approprier : la nuit est aussi l'essentiel qu'il ne faut pas perdre, mais conserver, accueillir non plus comme limite, mais en elle-même ; dans le jour doit passer la nuit ; la nuit qui se fait jour rend la lumière plus riche et fait de la clarté, au lieu de la scintillation de la surface, le rayonnement venu de la profondeur.*"

[^1-contre-nuit_30]: Du ciel nocturne comme de la vie, de la politique et de la poétique. Ne séparant la politique d'une politique d'existence, prenant la nuit comme Tiers, comme moyen de faire apparaître en plein jour toutes les destructions : extinction des espèces nocturnes, pollutions atmosphériques, défigurations lumineuses, injonction à l'activité --- tout ce à quoi veut répondre la contre-nuit : lutte pour le sommeil, l'existence d'autres formes de vie, pour un air respirable, un climat plus clément\.

[^1-contre-nuit_31]: C'est au mot près, la promesse d'*Ars Industrialis* et de Bernard Stiegler dans l'ouvrage *Réenchanter le monde*\.

[^1-contre-nuit_32]: Quelque chose de la Grande Nichée du post-exotisme emmené par Antoine Volodine et consort, et des bien réelles *anelosimus eximius* dont Abrüpt m'a appris l'existence. Manière toujours de mêler l'imaginaire et le réel dans la lutte pour la transformation du monde. Biomimétisme sombre, mêlé à l'écologie et au marxisme radical\.

[^1-contre-nuit_33]: Je ne prendrais pas le temps de montrer que tout est fiction alors que la fiction morbide dans laquelle on vit conduit à la destruction de la vie sur Terre --- ce qu'aucune fiction ne pourra changer, ni que toute l'espèce humaine s'origine dans la fiction. Qu'elle est essentiellement espèce imaginante. Que la fiction, c'est la grotte primitive. Que c'est la grotte devenue terrier. C'est ce que l'on mine. Avec nos gueules noires de suie. Creusant nos galeries dans la nuit vide\.

[^1-contre-nuit_34]: De Marx à Latour, du "fétiche de la marchandise" aux "faitiches"\.

[^1-contre-nuit_35]: "... Les clartés, au lieu de s'annuler, se diffractent et se multiplient, autres constellations, autres assemblages de sens. Mais d'une manière ou d'une autre, ni considération, ni désidération : fin de la sidération en générale. *Praxis*." Jean-Luc Nancy\.

[^1-contre-nuit_36]: Il faut que je dise ici, dans ces souterrains de la pensée que sont les phrases, ces voies négatives et fécondes que Daumal a permis de penser avec son *Contre-ciel*. *Contre-ciel* qu'il n'entendait non pas en une pure négativité athéologique, mais, par le détour de l'Inde antique, de ses textes, de ses traditions, de ses voix, une négativité qui ne soit pas dialectique, un contre-ciel qui ne soit pas le simple "retrait des dieux", une poésie qui ne soit pas une esthétique mais une recherche, une démarche et dont il faut garder la volonté de faire partie de "l'histoire des cataclysmes" de son temps plutôt que d'une pensée esthétique : être au diapason des désastres présents, ouvrir, par le détour d'une autre culture, la possibilité d'une perspective nouvelle, la recherche d'une poésie existentielle, matérielle, spirituelle, intégrale, voilà quelques principes que l'on peut hériter de Daumal, et qui font de la "contre-nuit" une perspective née du *Contre-ciel*. Façon aussi d'allumer des contre-feux faits de pleine obscurité\.

[^1-contre-nuit_37]: Ainsi "les contre-chants de la contre-nuit" sont le centre de gravité attirant l'ensemble de ces textes et de ces dimensions multiples dans un vortex épique\.

[^1-contre-nuit_38]: André Breton. Ouvrons ici la parenthèse du surréalisme qui voulait mêler Marx et Rimbaud : "changer la vie" et "transformer le monde", double exigence existentielle et politique, individuelle et collective. Pourtant le surréalisme aura été pris dans les mâchoires de la publicité et de la littérarité, sa résistance digérée comme un sous-produit culturel, et comme toute contre-culture, assimilé à la monstruosité ambiante qui ne garde de la culture que des images et des icônes (Spectacle, Culturel, et tant de Simulacres --- Debord, Deguy, Baudrillard). N'importe. C'est à ces questions, ces dimensions complètes --- microcosme et macrocosme --- qu'il faut se rapporter. Faire autre chose que des œuvres d'art : des tempêtes, des grâces et des orages, des cruautés et des silences, des bonheurs et des promesses\.

[^1-contre-nuit_39]: Nuit redoublée à la puissance de la nuit, nuit dans la nuit, la *nuit obscure de l'âme* chez les mystiques, depuis le poème de Jean de la Croix, désigne bien non pas cette réalité physique de la nuit, mais l'état d'inconnaissance, de dénuement absolu et de désespoir que certains appellent Dieu. Nuit seconde qui trouve sa place ici, dans ces souterrains semblables à ces fondations que l'on trouve dans *Aminadab* de Blanchot --- nuit des grottes, des lieux clos. "Qu'y avait-il au-dehors ? Était-ce la nuit ?" Cette nuit *est* le dehors. Ce dehors est l'absolu, l'insituable, et ce par quoi tout arrive. Ce dehors est cette *autre nuit* : celle qui est l'"apparition du “tout a disparu”" --- mouvement de la contradiction, présence de l'absence, du neutre, du suspens et de l'attente. Telle est au plus près la question du *désastre* dont Blanchot fait, après-guerre, le fondement abyssal (les Thomas déjà auparavant étaient paroles d'abîme) de l'écriture. Comme si la civilisation occidentale devait en passer par l'épreuve de cette nuit impossible à soutenir, ce point d'obscurité insoutenable de la Shoah --- effondrement de la civilisation du Livre, ruine de toute Œuvre possible, mais encore, nécessité d'inventer la parole et la théorie de cette "écriture du désastre", de cette "absence d'œuvre", de cet "entretien infini". Théorie, c'est-à-dire le ciel. Même un ciel hanté par l'abîme de cet événement impossible à réaliser\.

[^1-contre-nuit_40]: Friedrich Nietzsche\.

[^1-contre-nuit_41]: ... *per Chaos hoc ingens vastique silentia regni*\.

[^1-contre-nuit_42]: Marquant ici tout ce qu'indique Neyrat dans son *Ange noir de l'histoire : Cosmos et technique de l'Afrofuturisme*, et encore dans *Le Cosmos de Walter Benjamin*\.

[^1-contre-nuit_43]: Pour expliquer que la nuit soit noire, Poe a l'intuition géniale que l'univers n'a pas toujours existé et que si nous ne sommes pas éblouis la nuit par toutes les étoiles l'explication en est que l'univers est fini (que les étoiles elles aussi ont une histoire) et que la lumière met du temps à nous parvenir (sa vitesse n'est pas instantanée)\.

[^1-contre-nuit_44]: **etc., etc.**

[^1-contre-nuit_45]: Auguste Blanqui, révolutionnaire anarchiste né en France, est une figure politique centrale des insurrections du XIX^e^ siècle de *leur* ère --- car il faudra d'autres figures, d'autres repères, et composer aussi d'autres manières de vivre dans le temps. Transféré après la Commune à Morlaix, au Fort du Taureau, il écrit pour se soustraire à un état de surveillance délirant les pages serrées de *L'éternité par les astres*. Révolution non pas politique mais astronomique. Comme si la politique et les astres n'avaient rien en commun --- et là est notre profonde divergence, notre étonnement. L'aveuglement d'un penseur et acteur à ce qui nous apparaît aujourd'hui comme seul fait *révolutionnaire* : que le cosmos intéresse au premier chef notre politique, et devrait être placé au centre (*centre dont la périphérie est partout et le centre nulle part*). L'univers dans notre moelle. Spéculation purement astronomique donc, où Blanqui s'imagine à partir du système de Laplace les mondes habités par d'autres humanités comme autant de répliques des grandeurs et des misères, et de nous-mêmes placés dans ces autres mondes dans toutes les positions possibles\.

[^1-contre-nuit_46]: Revoilà les mantras sur l'éducation populaire à notre désidération, les classes d'astronomie et d'astrologie du désastre, l'agitprop déployée en cyberlittérature\.

[^1-contre-nuit_47]: Il faudrait raconter comment nous, bactéries, travaillons, digérons notre pensée à partir de ces mots gigantesques ? Parler de Glissant reprenant de Segalen le Divers comme alternative à l'universel ? Parler, toujours selon Glissant, de la Relation et du *droit à l'opacité*, de l'Autre dans son inaccessibilité, vers lequel changer en échangeant *sans se perdre ni se dénaturer*. C'est-à-dire en même temps voulant changer et nous dénaturer, sûrement. Ayant fait de la philosophie des mélanges hors frontières. Des impossibles et des imprévus faisant la vie\.

[^1-contre-nuit_48]: Nietzsche nous a mis en garde contre cette fascination de la nuit comme l'émanation méphitique d'un ensemble de valeurs morbides et doloristes, comme l'a été le Romantisme assignant à la suite du christianisme à la nuit un pur imaginaire de fuite, d'imaginaire, de mal, de solitude inapaisée, de tentations et de négations. C'est une *autre nuit* qu'il désire, minuit profond éclatant d'étoiles, promesses de nos volontés et de nos créations, douleurs acceptées et non sanctifiées, nuits plurivoques et nuits créatrices, solitudes étoilées. Nuit intempestive aujourd'hui que cette nuit non pas idéelle, mais nuit sensible et réelle et non pas la nuit abrutie de ses représentations, nuit que Nietzsche désigne comme "la vengeance des ivrognes en tout genre, pour lesquels l'aube est le moment le plus lugubre de la journée" (*Aurore*)

[^1-contre-nuit_49]: Notre époque aura ainsi été si frénétique à s'étouffer (du grec : *πνίγω*) pour crier de la forme de l'horreur de son moment (anthropocène, capitalocène, plantationocène, pyrocène, androcène, etc...) jusqu'au bonheur de son étrangement (cosmocène, alienocène).\ 
    *Le temps d'écouter dans cette pâle insomnie la voix étouffée de la vie*, disait, prophétique comme le sont souvent les poètes, Jean Métellus.

[^1-contre-nuit_50]: Depuis le scientifique James Lovelock qui pour donner au système d'autorégulation climatique du système-Terre avait évoqué *L'hypothèse Gaïa* (1970), Bruno Latour a repris et développé notre situation emmêlée au monde et *Face à Gaïa*\.

[^1-contre-nuit_51]: "Tu te consoles / dans mon obscurité. / je me console / dans la tienne". "L'obscurité. / L'obscurité doit / devenir lisible / à sa manière / (aux résistants). Entendre / illisible aux salauds de l'espèce / & vitale à mon corps affolé." Cédric Demangeot\.
