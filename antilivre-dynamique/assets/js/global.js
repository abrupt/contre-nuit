import * as THREE from "./three/three.module.js";
// import { MapControls } from "./three/OrbitControls.js";
import { OrbitControls } from "./three/OrbitControls.js";
import { EffectComposer } from "./three/EffectComposer.js";
import { RenderPass } from "./three/RenderPass.js";
import { GlitchPass } from "./three/GlitchPass.js";
import { ShaderPass } from "./three/ShaderPass.js";
import { FilmPass } from "./three/FilmPass.js";
import { VignetteShader } from "../js/shaders/VignetteShader.js";

import { ImprovedNoise } from './three/ImprovedNoise.js';

import { ConvexGeometry } from './three/ConvexGeometry.js';
import * as BufferGeometryUtils from './three/BufferGeometryUtils.js';

// Multilangue URL
let pathURL;
if (window.location.href.indexOf("/en/") != -1) {
  pathURL = "../";
} else {
  pathURL = "./";
}
//// Menu
const menuBtn = document.querySelector(".btn--menu");
const randomBtn = document.querySelector(".btn--random");
const menu = document.querySelector(".menu");
let menuOpen = false;
const musiqueBtn = document.querySelector(".btn--sound");
const musiqueBtnOn = document.querySelector(".btn--sound .info--on");
const musiqueBtnOff = document.querySelector(".btn--sound .info--off");
const morceau = document.querySelector(".son");


const loadBtn = document.querySelector(".btn--load");

loadBtn.addEventListener("click", (e) => {
  e.preventDefault();
  loadBtn.blur();
  document.querySelector('.loading').classList.add("loading--done");
});


menuBtn.addEventListener("click", (e) => {
  e.preventDefault();
  menuBtn.blur();
  menuOpen = !menuOpen;
  if (menuOpen) {
    // if (!isTouchDevice()) controls.enabled = false;
  } else {
    // if (!isTouchDevice()) controls.enabled = true;
  }
  menu.classList.toggle("show--menu");
});

// Musique
// let musiqueJoue = "pause";
let vol = 0;
let interval = 30;
let fadeInAudio;
let fadeOutAudio;
function musique(track) {
  if (track.paused) {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    track.volume = vol;
    track.play();
    fadeInAudio = setInterval(function() {
      if (track.volume < 0.99) {
        track.volume += 0.01;
      } else {
        clearInterval(fadeInAudio);
      }
    }, interval);
  } else {
    track.pause();
  }
}
let silence = true;
musiqueBtn.addEventListener("click", (e) => {
  e.preventDefault();
  if (silence) {
    musique(morceau);
  } else {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    morceau.pause();
  }
  musiqueBtnOn.classList.toggle("none");
  musiqueBtnOff.classList.toggle("none");
  silence = !silence;
  // musiqueBtn.classList.toggle('playing');
  musiqueBtn.blur();
});

function randomNb(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomNbFrac(min, max) {
  // min and max included
  return Math.random() * (max - min) + min;
}

function isTouchDevice() {
  return (
    "ontouchstart" in window ||
    navigator.maxTouchPoints > 0 ||
    navigator.msMaxTouchPoints > 0
  );
}

//
// Three.js
//
let container;
let camera, controls, scene, renderer;
let textureEquirec;
let textureForm;
let starBox, stars, vertices;
let ambientLight, dirLight1, dirLight2, spotLight;
let dirGroup;
let group;
let parameters;
let materials = [];
let composer, composer2;
let clickable = true;
let movingCamera = false;
let movingCameraEnd = false;
let animClick = false;
// let loadingTexture1 = false;
// let loadingTexture2 = false;
// let loadingTexture3 = false;
// let loadingTexture4 = false;
// let loadingTexture5 = false;
// let loadingTexture6 = false;
// let loadingTexture7 = false;
// let loadingTextureGround = false;
let loadingAsteroids = false;
let everythingLoaded = false;
let loadingTextureNight = false;
let loadingTextureAsteroids = false;

const nbCercles = 6;
const textes = [...document.querySelectorAll(".texte")];
const texteContainer = document.querySelector(".textes");
const footnotes = document.querySelector('.footnotes');
const allNotes = document.querySelectorAll('.footnotes li');

const textesCercles = [];

for (let i = 0; i < nbCercles; i++) {
  const newCercle = [];
  textes.forEach((el) => {
    if (el.dataset.cercle - 1 == i) {
      newCercle.push(el);
    }
  })
  textesCercles.push(newCercle);
}

let blackHole;
let blackHoleGeo;
let blackHoleV3, blackHolePos, blackHoleNPos, blackHoleRadius;

let listSolids = [];

const mouse = new THREE.Vector2();
let clock = new THREE.Clock();
let noise = new ImprovedNoise()

init();
//render(); // remove when using next line for animation loop (requestAnimationFrame)
animate();

function init() {
  scene = new THREE.Scene();

  // Background image
  const textureLoader = new THREE.TextureLoader();
  textureEquirec = textureLoader.load(
    './etc/contre-nuit-fond.jpg',
    function(texture) {
      loadingTextureNight = true;
    }
  );
  textureEquirec.mapping = THREE.EquirectangularReflectionMapping;
  textureEquirec.encoding = THREE.sRGBEncoding;
  scene.background = textureEquirec;

  textureForm = textureLoader.load(
    './etc/contre-nuit-fond.jpg',
    function(texture) {
      loadingTextureAsteroids = true;
    }
  );
  textureForm.mapping = THREE.EquirectangularReflectionMapping;

  // Colors
  // scene.background = new THREE.Color(0x000000);
  // scene.fog = new THREE.Fog(0xd0a96d, 1, 1000);

  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    20000
  );
  camera.position.set(0, 100, 800);

  // lights
  ambientLight = new THREE.AmbientLight(0xffffff, 1);
  scene.add(ambientLight);

  // spotLight = new THREE.SpotLight(0xfb2ead);
  // spotLight.angle = Math.PI / 5;
  // spotLight.penumbra = 0.3;
  // spotLight.position.set(5, 10, 5);
  // spotLight.castShadow = true;
  // spotLight.shadow.camera.near = 8;
  // spotLight.shadow.camera.far = 2000;
  // spotLight.shadow.mapSize.width = 512;
  // spotLight.shadow.mapSize.height = 512;
  // spotLight.shadow.bias = -0.002;
  // spotLight.shadow.radius = 4;
  // scene.add(spotLight);

  dirLight1 = new THREE.DirectionalLight(0xffffff, 1);
  dirLight1.position.set(-100, 100, 100);
  dirLight2 = new THREE.DirectionalLight(0xffffff, 0.38);
  dirLight2.position.set(100, -100, -100);
  // dirLight1.castShadow = true;
  // dirLight1.shadow.camera.near = 1;
  // dirLight1.shadow.camera.far = 400;
  // dirLight1.shadow.camera.right = 200;
  // dirLight1.shadow.camera.left = -200;
  // dirLight1.shadow.camera.top = 150;
  // dirLight1.shadow.camera.bottom = -150;
  // dirLight1.shadow.mapSize.width = 1024;
  // dirLight1.shadow.mapSize.height = 1024;
  // dirLight1.shadow.radius = 4;
  // dirLight1.shadow.bias = -0.0009;

  dirGroup = new THREE.Group();
  dirGroup.add(dirLight1);
  dirGroup.add(dirLight2);
  scene.add(dirGroup);

  // const helper = new THREE.CameraHelper(dirLight1.shadow.camera);
  // scene.add(helper);
  // const axesHelper = new THREE.AxesHelper(5000);
  // axesHelper.setColors(0xff0000, 0x00ff00, 0x0000ff);
  // scene.add(axesHelper);
  // const helperLight = new THREE.DirectionalLightHelper(dirLight1, 5);
  // scene.add(helperLight);
  // const helperLight2 = new THREE.DirectionalLightHelper(dirLight2, 5);
  // scene.add(helperLight2);

  // Textures
  // const mapConcrete1 = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/concrete1.jpg",
  //   function(texture) {
  //     loadingTexture1 = true;
  //   }
  // );
  // mapConcrete1.wrapS = mapConcrete1.wrapT = THREE.RepeatWrapping;
  // mapConcrete1.anisotropy = 16;
  //
  // const mapConcrete2 = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/concrete2.jpg",
  //   function(texture) {
  //     loadingTexture2 = true;
  //   }
  // );
  // mapConcrete2.wrapS = mapConcrete2.wrapT = THREE.RepeatWrapping;
  // mapConcrete2.anisotropy = 16;
  //
  // const mapConcrete3 = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/concrete3.jpg",
  //   function(texture) {
  //     loadingTexture3 = true;
  //   }
  // );
  // mapConcrete3.wrapS = mapConcrete3.wrapT = THREE.RepeatWrapping;
  // mapConcrete3.anisotropy = 16;
  //
  // const mapConcrete4 = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/concrete4.jpg",
  //   function(texture) {
  //     loadingTexture4 = true;
  //   }
  // );
  // mapConcrete4.wrapS = mapConcrete4.wrapT = THREE.RepeatWrapping;
  // mapConcrete4.anisotropy = 16;
  //
  // const mapConcrete5 = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/concrete5.jpg",
  //   function(texture) {
  //     loadingTexture5 = true;
  //   }
  // );
  // mapConcrete5.wrapS = mapConcrete5.wrapT = THREE.RepeatWrapping;
  // mapConcrete5.anisotropy = 16;
  //
  // const mapConcrete6 = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/concrete6.jpg",
  //   function(texture) {
  //     loadingTexture6 = true;
  //   }
  // );
  // mapConcrete6.wrapS = mapConcrete6.wrapT = THREE.RepeatWrapping;
  // mapConcrete6.anisotropy = 16;
  //
  // const mapConcrete7 = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/concrete7.jpg",
  //   function(texture) {
  //     loadingTexture7 = true;
  //   }
  // );
  // mapConcrete7.wrapS = mapConcrete7.wrapT = THREE.RepeatWrapping;
  // mapConcrete7.anisotropy = 16;
  //
  // const mapSable = new THREE.TextureLoader().load(
  //   pathURL + "js/textures/sable.jpg",
  //   function(texture) {
  //     loadingTextureGround = true;
  //   }
  // );
  // mapSable.wrapS = mapSable.wrapT = THREE.RepeatWrapping;
  // mapSable.anisotropy = 16;
  // mapSable.repeat.set(60, 60);
  //
  // // ground
  // const planeGeometry = new THREE.PlaneGeometry(2000, 2000, 8, 8);
  // const planeMaterial = new THREE.MeshLambertMaterial({
  //   color: 0xd6b480,
  //   map: mapSable,
  //   // shininess: 0,
  //   // specular: 0x653f2c,
  // });
  //
  // const ground = new THREE.Mesh(planeGeometry, planeMaterial);
  // ground.rotation.x = -Math.PI / 2;
  // ground.position.set(0, 0, 0);
  // ground.scale.multiplyScalar(3);
  // // ground.castShadow = true;
  // // ground.receiveShadow = true;
  // // scene.add(ground);

  // Objects
  // Grid of objects
  const nbSolids = textes.length;

  // const sphereGeometry = new THREE.BoxGeometry(1, 1, 1);
  const listMaterial = [
    new THREE.MeshPhongMaterial({
      color: 0xeeeeee,
      shininess: 100,
      specular: 0xffffff,
      // wireframeLinewidth: 2,
      // envMap: textureEquirec,
      envMap: textureForm,
    }),
    new THREE.MeshPhongMaterial({
      color: 0x777777,
      shininess: 100,
      specular: 0x888888,
      // wireframeLinewidth: 2,
      // envMap: textureEquirec,
      envMap: textureForm,
      // opacity: 1,
      // transparent: true,
      // depthTest: true,
      // depthWrite: true,
      // shininess: 100,
      // envMap: textureEquirec,
      // reflectivity: 1,
    }),
    new THREE.MeshPhongMaterial({
      color: 0xdddddd,
      shininess: 100,
      specular: 0xcccccc,
      // wireframeLinewidth: 2,
      // envMap: textureEquirec,
      envMap: textureForm,
      // opacity: 1,
      // transparent: true,
      // depthTest: true,
      // depthWrite: true,
      // shininess: 100,
      // envMap: textureEquirec,
      // reflectivity: 1,
    }),
    new THREE.MeshPhongMaterial({
      color: 0x666666,
      shininess: 100,
      specular: 0xbbbbbb,
      // wireframeLinewidth: 2,
      // envMap: textureEquirec,
      envMap: textureForm,
      // opacity: 1,
      // transparent: true,
      // depthTest: true,
      // depthWrite: true,
      // shininess: 100,
      // envMap: textureEquirec,
      // reflectivity: 1,
    }),
    new THREE.MeshPhongMaterial({
      color: 0xd0d0d0,
      shininess: 100,
      specular: 0xaaaaaa,
      // wireframeLinewidth: 2,
      // envMap: textureEquirec,
      envMap: textureForm,
      // opacity: 1,
      // transparent: true,
      // depthTest: true,
      // depthWrite: true,
      // shininess: 100,
      // envMap: textureEquirec,
      // reflectivity: 1,
    }),
    new THREE.MeshPhongMaterial({
      color: 0x888888,
      shininess: 100,
      specular: 0xbbbbbb,
      // wireframeLinewidth: 2,
      // envMap: textureEquirec,
      envMap: textureForm,
      // opacity: 1,
      // transparent: true,
      // depthTest: true,
      // depthWrite: true,
      // shininess: 100,
      // reflectivity: 1,
    }),
    new THREE.MeshPhongMaterial({
      color: 0xbbbbbb,
      shininess: 100,
      specular: 0xeeeeee,
      // wireframeLinewidth: 2,
      // envMap: textureEquirec,
      envMap: textureForm,
      // opacity: 1,
      // transparent: true,
      // depthTest: true,
      // depthWrite: true,
      // shininess: 100,
      // envMap: textureEquirec,
      // reflectivity: 1,
    }),
  ];



  // const verticesOfCube = [
  //   -1, -1, -1, 1, -1, -1, 1, 1, -1, -1, 1, -1,
  //   -1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1,
  // ];
  //
  // const indicesOfFaces = [
  //   2, 1, 0, 0, 3, 2,
  //   0, 4, 7, 7, 3, 0,
  //   0, 1, 5, 5, 4, 0,
  //   1, 2, 6, 6, 5, 1,
  //   2, 3, 7, 7, 6, 2,
  //   4, 5, 6, 6, 7, 4
  // ];
  //


  let counterPerCircle = 0;
  let levelCircle = 1;
  let radiusStart = 200;
  const gapCircle = 200;
  let size = 10;
  let grid = [];
  const sizeCell = (size * 2) + 10;

  for (let i = 0; i < textesCercles.length; i++) {
    let newGrid = [];
    grid.push(newGrid);
  }

  for (let i = 0; i < textesCercles.length; i++) {
    const rows = 4;
    const columns = Math.ceil(textesCercles[i].length / rows);
    if (i > 0) {
      const previousColumns = Math.ceil(textesCercles[i - 1].length / rows);
      radiusStart = radiusStart + (sizeCell * previousColumns) + gapCircle;
    }
    for (let j = 0; j < rows; j++) {
      for (let k = 0; k < columns; k++) {
        const angle = randomNb(0, 360);
        grid[i].push({
          occupy: 0,
          // x: radiusStart + (sizeCell * k),
          x: Math.cos(angle) * (radiusStart + (sizeCell * k)),
          y: sizeCell - (j * sizeCell),
          // z: 0,
          z: Math.sin(angle) * (radiusStart + (sizeCell * k)),
        });
      }
    }
  }
  // console.log(grid);

  function generateAsteroid(size, circle) {

      let points = [];
      for (let i = 0; i < 42; i++) {
        let rx = Math.random() * (size + size) - size;
        let ry = (Math.random() * (size + size) - size) * 1.62;
        let rz = Math.random() * (size + size) - size;
        points.push(new THREE.Vector3(rx, ry, rz));
      }

      let geometry = new ConvexGeometry(points);
      let material = listMaterial[circle - 1];

      let mesh = new THREE.Mesh(geometry, material);
      mesh.castShadow = true;
      mesh.receiveShadow = true;

      // mesh.options = {
      //   roughScale: randomNbFrac(0.01, 0.05),
      //   fineScale: randomNbFrac(0.05, 0.1),
      //   roughHeight: randomNbFrac(25, 150),
      //   fineHeight: randomNbFrac(12, 50),
      //   uvOffset: [randomNbFrac(0.01,999), randomNbFrac(0.01,999)],
      //   timeInc: randomNbFrac(0.0008, 0.002)
      // };

      // stones.push(geomesh);
      // scene.add(geomesh);
    return mesh;

  }

  // const blackHoleGeo = new THREE.BoxGeometry(1, 1, 1);
  //
  // blackHole = generateAsteroid(10, 7);
  // scene.add(blackHole);

  blackHoleRadius = 2;
  blackHoleGeo = new THREE.IcosahedronGeometry(1, 1);
  blackHoleNPos = [];
  blackHoleV3 = new THREE.Vector3();
  blackHolePos = blackHoleGeo.attributes.position;
  for (let i = 0; i < blackHolePos.count; i++) {
    blackHoleV3.fromBufferAttribute(blackHolePos, i).normalize();
    blackHoleNPos.push(blackHoleV3.clone());
  }
  blackHoleGeo.userData.nPos = blackHoleNPos;
  // m = new THREE.MeshNormalMaterial({ wireframe: false });
  blackHole = new THREE.Mesh(blackHoleGeo, listMaterial[6]);
  blackHole.position.set(0, 0, 0);
  scene.add(blackHole);

  // generateAsteroid(size, 1);

  // for (let i = 0; i < nbSolids; i++) {
  //   console.log(Math.ceil(Math.sqrt(textesCercles[cercleNum - 1].length)));
  // }

  for (let i = 0; i < nbSolids; i++) {
    const cercleNum = textes[i].dataset.cercle;
    // const obj = new THREE.Mesh(sphereGeometry, listMaterial[cercleNum]);
    // const cell = grid[i];
    const nbCell = randomNb(0, (grid[cercleNum - 1].length - 1));
    const cell = grid[cercleNum - 1][nbCell];
    let posX = cell.x;
    let posY = cell.y;
    let posZ = cell.z;

    // console.log(textesCercles[cercleNum].length);

    if (cercleNum === levelCircle) {
      counterPerCircle += 1;
    } else {
      counterPerCircle = 1;
      levelCircle = cercleNum;
      // radiusStart = 40;
    }

    // console.log(`level: ${levelCircle}; counter: ${counterPerCircle}`);

    // const geometry = new THREE.SphereGeometry(size, 30, 30);
    const geometry = new THREE.BoxGeometry(size, size, size);
    const material = listMaterial[cercleNum - 1];
    // const mesh = new THREE.Mesh(geometry, material);
    const mesh = generateAsteroid(size, cercleNum);
    const obj = new THREE.Object3D();

    // let radius = (radiusStart * (cercleNum * 2)) + (gap + (size * counterPerCircle));
    // let radius = radiusStart;
    // radiusStart += size * 2;
    mesh.radius = posX;
    mesh.textNb = i;
    mesh.speed = randomNbFrac(0.0001, 0.001);
    mesh.speedSelf = randomNbFrac(0.001, 0.01);

    mesh.position.x = posX;
    mesh.position.y = posY;
    mesh.position.z = posZ;
    grid[cercleNum - 1].splice(grid[cercleNum - 1].indexOf(cell), 1);

    obj.add(mesh);
    scene.add(obj);
    listSolids.push(obj);

    // let theta = randomNb(0, 360);
    // let radius = randomNb(60, 80) * (cercleNum * 2);
    // obj.radius = radius;
    // obj.textNb = i;
    // let posX = Math.cos(theta) * radius;
    // let posY = 0;
    // let posZ = Math.sin(theta) * radius;
    // obj.position.set(posX, posY, posZ);
    // obj.updateMatrix();
    // obj.matrixAutoUpdate = false;
    // obj.castShadow = true;
    // obj.receiveShadow = true;

    // let radius = randomNb(60, 80) * (cercleNum * 2);
    // const asteroid = makeAsteroid(10,listMaterial[1],radius);
    // asteroid.radius = radius;
    // asteroid.textNb = i;
    // obj.updateMatrix();
    // obj.matrixAutoUpdate = false;
    // obj.castShadow = true;
    // obj.receiveShadow = true;

    // listSolids.push(asteroid);
    // scene.add(obj);

    if (i == nbSolids - 1) {
      loadingAsteroids = true;
    }

    // renderer.render(scene, camera);
  }
  // for (const solid of listSolids) {
  //   console.log(solid.radius);
  // }

  // for (let i = 0; i < 500; i++) {
  //   const mesh = new THREE.Mesh(geometry, material);
  //   mesh.scale.x = 20;
  //   mesh.scale.y = Math.random() * 80 + 10;
  //   mesh.scale.z = 20;
  //   mesh.updateMatrix();
  //   mesh.matrixAutoUpdate = false;
  //   scene.add(mesh);
  // }

  // const dirLight1 = new THREE.DirectionalLight(0xffffff);
  // dirLight1.position.set(1, 1, 1);
  // scene.add(dirLight1);

  // const dirLight2 = new THREE.DirectionalLight(0x002288);
  // dirLight2.position.set(-1, -1, -1);
  // scene.add(dirLight2);

  // const ambientLight = new THREE.AmbientLight(0x222222);
  // scene.add(ambientLight);

  //

  // Render
  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  // renderer.shadowMap.enabled = true;
  // renderer.shadowMap.type = THREE.VSMShadowMap;

  // controls
  controls = new OrbitControls(camera, renderer.domElement);
  controls.enableDamping = true;
  controls.dampingFactor = 0.05;
  controls.screenSpacePanning = false;
  // controls.panSpeed = 0.2;
  // controls.rotateSpeed = 0.8;
  // controls.zoomSpeed = 0.8;
  controls.minDistance = 0;
  controls.maxDistance = 10000;
  // controls.maxPolarAngle = Math.PI / 2.015;
  controls.target = new THREE.Vector3(0, 1, 0);
  controls.update();

  // Container
  container = document.getElementById("container");
  container.appendChild(renderer.domElement);

  // postprocessing
  const shaderVignette = VignetteShader;
  const effectVignette = new ShaderPass(shaderVignette);
  // const glitchPass = new GlitchPass();
  // // glitchPass.goWild = true;
  //
  effectVignette.uniforms["offset"].value = 0.9;
  effectVignette.uniforms["darkness"].value = 1.1;
  //
  const effectFilm = new FilmPass(0.4, 0.03, 600, true);

  // Glitch
  composer = new EffectComposer(renderer);
  composer.addPass(new RenderPass(scene, camera));
  //
  // composer = new EffectComposer(renderer);
  // composer.addPass(new RenderPass(scene, camera));
  //
  // composer.addPass(glitchPass);
  // composer.addPass(effectFilm);
  composer.addPass(effectVignette);
  //
  // // Permanent
  // composer2 = new EffectComposer(renderer);
  // composer2.addPass(new RenderPass(scene, camera));
  //
  // composer2.addPass(effectFilm);
  // composer2.addPass(effectVignette);

  window.addEventListener("resize", onWindowResize);
}

let timer;
controls.addEventListener("start", (el) => {
  movingCameraEnd = false;
  // timer = setTimeout(() => {
  //   moveControls = true;
  // }, 150);
});

controls.addEventListener("end", (el) => {
  clearTimeout(timer);
  movingCameraEnd = true;
  if (movingCamera) {
    movingCamera = false;
  }
});

controls.addEventListener("change", (el) => {
  if (movingCameraEnd) {
    clearTimeout(timer);
    movingCamera = false;
  } else {
    timer = setTimeout(() => {
      movingCamera = true;
    }, 150);
  }
});

container.addEventListener("click", onDocumentMouseClick, false);
container.addEventListener("mousemove", onDocumentMouseMove, false);
texteContainer.addEventListener(
  "mousemove",
  (event) => {
    event.stopPropagation();
    document.body.style.cursor = "";
  },
  false
);

let textesOpen = false;
let timerGlitch;
function onDocumentMouseClick(event) {
  const mouse = new THREE.Vector2();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  const intersectCenter = raycaster.intersectObject(blackHole, false);
  if ((intersects.length > 0 || intersectCenter.length > 0) && clickable) {
    clearTimeout(timerGlitch);
    // console.log(intersects);
    let texteChoisi;
    if (intersectCenter.length > 0) {
      texteChoisi = textes[randomNb(0, textes.length - 1)];
    } else {
      texteChoisi = textes[intersects[0].object.textNb];
    }
    const footnoteref = texteChoisi.querySelectorAll('.footnote-ref');
    document.body.style.cursor = "";
    if (textesOpen) {
      textes.forEach((el) => {
        el.classList.remove("show");
      });
      texteChoisi.classList.add("show");
      document.querySelector(".texte.show").scrollIntoView();
      if (footnoteref.length > 0) {
        footnotes.classList.add('show');
        const listNb = footnotes.querySelector('ol');
        listNb.start = footnoteref[0].innerHTML;
        allNotes.forEach((el) => {
          el.classList.add('hide');
        });
        footnoteref.forEach((el) => {
          const refNb = el.hash;
          const footnoteContent = footnotes.querySelector('[id^="' + refNb.substring(1) + '"]');
          footnoteContent.classList.remove('hide');
        });
      } else {
        footnotes.classList.remove('show');
      }
    } else {
      texteContainer.classList.add("show");
      texteChoisi.classList.add("show");
      document.querySelector(".texte.show").scrollIntoView();
      if (footnoteref.length > 0) {
        footnotes.classList.add('show');
        const listNb = footnotes.querySelector('ol');
        listNb.start = footnoteref[0].innerHTML;
        allNotes.forEach((el) => {
          el.classList.add('hide');
        });
        footnoteref.forEach((el) => {
          const refNb = el.hash;
          const footnoteContent = footnotes.querySelector('[id^="' + refNb.substring(1) + '"]');
          footnoteContent.classList.remove('hide');
        });
      }
      // if (!isTouchDevice()) controls.activeLook = false;
      textesOpen = !textesOpen;
    }
    animClick = true;
    timerGlitch = setTimeout(() => {
      animClick = false;
    }, 2000);
  }
}

const closeBtn = document.querySelector(".textes__close");
closeBtn.addEventListener("click", (e) => {
  e.preventDefault();
  document.querySelector(".texte.show").scrollIntoView();
  texteContainer.classList.remove("show");
  footnotes.classList.remove('show');
  allNotes.forEach((el) => {
    el.classList.remove('hide');
  });
  textes.forEach((el) => {
    el.classList.remove("show");
  });
  textesOpen = !textesOpen;
  // if (!isTouchDevice()) controls.activeLook = true;
  closeBtn.blur();
});

function onDocumentMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  const intersectCenter = raycaster.intersectObject(blackHole, false);
  if (intersects.length > 0 || intersectCenter.length > 0) {
    document.body.style.cursor = "pointer";
  } else {
    document.body.style.cursor = "";
  }
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

for (const solid of listSolids) {
  // console.log(solid);
  // solid.rotateY(solid.speed);
}
// console.log(blackHole);
function animate(time) {
  requestAnimationFrame(animate);
  controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true

  let t = clock.getElapsedTime() * 0.2;
   blackHoleGeo.userData.nPos.forEach((p, idx) => {
   	let ns = noise.noise(p.x * 40 + t, p.y * 40 + t, p.z * 40 + t);
     blackHoleV3.copy(p).multiplyScalar(blackHoleRadius).addScaledVector(p, ns);
     blackHolePos.setXYZ(idx, blackHoleV3.x * 40, blackHoleV3.y * 40, blackHoleV3.z * 40);
   })
   blackHoleGeo.computeVertexNormals();
   blackHolePos.needsUpdate = true;

  // let t = clock.getElapsedTime() * 0.25;
  //  console.log(t);
  //  g.userData.nPos.forEach((p, idx) => {
  //  	let ns = noise.noise(p.x + t, p.y + t, p.z + t);
  //    v3.copy(p).multiplyScalar(radius).addScaledVector(p, ns);
  //    pos.setXYZ(idx, v3.x, v3.y, v3.z);
  //  })
  //  g.computeVertexNormals();
  //  pos.needsUpdate = true;

  render();


  if (
    !everythingLoaded &&
    loadingTextureNight &&
    loadingTextureAsteroids &&
    loadingAsteroids
    // loadingTexture1 &&
    // loadingTexture2 &&
    // loadingTexture3 &&
    // loadingTexture4 &&
    // loadingTexture5 &&
    // loadingTexture6 &&
    // loadingTexture7 &&
    // loadingTextureGround &&
  ) {
    everythingLoaded = true;
    const finish = document.querySelector(".loading");
    finish.classList.add("loading--all-is-loaded");
  }

  if (movingCamera) {
    clickable = false;
  } else {
    clickable = true;
  }

  // const delta = clock.getDelta() * 0.001;
  // const timeOther = Date.now() * 0.0005;
  //

  blackHole.rotateX(0.00042);
  blackHole.rotateY(0.00042);
  blackHole.rotateZ(0.00042);
  for (const solid of listSolids) {
    solid.rotateY(solid.children[0].speed);
    solid.children[0].rotateY(solid.children[0].speedSelf);
    solid.children[0].rotateZ(solid.children[0].speedSelf);
    solid.children[0].rotateX(solid.children[0].speedSelf);
  }


  // for (let i = 0; i < vertices.velocities.length; i++) {
  //   vertices.velocities[i / 3 + (i % 3)] += vertices.accelerations[i];
  //   vertices.positions[i * 3 + 1] -= vertices.velocities[i];
  //
  //   if (vertices.positions[i * 3 + 1] < -200) {
  //     vertices.positions[i * 3 + 1] = 4000;
  //     vertices.velocities[i / 3 + (i % 3)] = 0;
  //   }
  // }

  // stars.rotation.y += 0.002;

  // starBox.setAttribute(
  //   "position",
  //   new THREE.BufferAttribute(new Float32Array(vertices.positions), 3)
  // );

  // const delta = clock.getDelta() / 20;
  //
  // dirGroup.rotation.y += 0.7 * delta;
  // dirLight.position.z = 17 + Math.sin(time * 0.001) * 5;


  // if (animClick) {
    // composer.render();
  // } else {
  //   composer2.render();
  // }
}

function render() {
  renderer.render(scene, camera);
}

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector(".menu"));
let simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
// Disable scroll simplebar for print
window.addEventListener("beforeprint", (event) => {
  simpleBarContent.unMount();
  simpleBarContentTextes.unMount();
});
window.addEventListener("afterprint", (event) => {
  simpleBarContent = new SimpleBar(document.querySelector(".menu"));
  simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
});

//
// Scripts hack
//
// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// We listen to the resize event
window.addEventListener("resize", () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});

////
//// Prevent bounce effect iOS
////
//// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
//// let contentBounce = document.querySelector('.container');
//// contentBounce.addEventListener('touchstart', function (event) {
////   this.allowUp = this.scrollTop > 0;
////   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
////   this.slideBeginY = event.pageY;
//// });

//// contentBounce.addEventListener('touchmove', function (event) {
////   let up = event.pageY > this.slideBeginY;
////   let down = event.pageY < this.slideBeginY;
////   this.slideBeginY = event.pageY;
////   if ((up && this.allowUp) || (down && this.allowDown)) {
////     event.stopPropagation();
////   } else {
////     event.preventDefault();
////   }
//// });

// //
// // Old iOS versions
// //
// let messageIOS = false;
//
// const platform = window.navigator.platform;
// const iosPlatforms = ['iPhone', 'iPad', 'iPod'];
//
// if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
//   const banner = document.createElement('div');
//   banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">This page will not work properly with iOS versions older than 13.<br>(Click on this banner to make it disappear.)</p></div>`;
//   banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
//   document.querySelector('body').appendChild(banner);
//   messageIOS = true;
// }
